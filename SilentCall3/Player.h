#pragma once
#include <vector>
#include "common.cpp"
#include "src/Item.h"
#include "src/items/ArmourItem.h"
#include "src/items/PotionItem.h"
#include "src/items/WeaponItem.h"
#include "SpriteAnimation.h"

class Player
{
public:
	Player();
	Player(int x, int y, SpriteAnimation* sa);
	~Player();

	double GetX();
	double GetY();
	void SetX(double x);
	void SetY(double y);
	void SetXYPosition(double x, double y);

	SpriteAnimation* GetSpriteAnimation();
	void SetSpriteAnimation(SpriteAnimation* sa);
	int GetAnimationPosition();

	double GetMovementSpeed();
	void SetMovementSpeed(double speed);
	double GetEffectiveMovementSpeed();
	int GetWidth();
	int GetHeight();

	bool GetIfFacingRight();
	void SetIfFacingRight(bool right);
	/*! \brief True if player's last movement included pushing up*/
	bool GetIfPointingUp();
	void SetIfPointingUp(bool is_pointing_up);
	bool GetIfPointingDown();
	void SetIfPointingDown(bool is_pointing_down);
	bool GetIfPointingLeft();
	void SetIfPointingLeft(bool is_pointing_left);
	bool GetIfPointingRight();
	void SetIfPointingRight(bool is_pointing_right);
	/*! \brief The angle player is pointing*/
	double GetPointingAngle();
	bool GetIfMoving();
	void SetIfMoving(bool moving);

	// Step forward sprite animation etc
	void Update();

	// Functions for attacking with primary weapon
	bool IsReadyToPrimaryAttack();
	void PrimaryAttack(SpriteAnimation* sprite_animation);
	/*! \brief If the player is currently attacking directly (i.e. not via projectile) */
	bool IsDirectlyPrimaryAttacking();
	int GetPrimaryAttackDamage();

	// Functions for attacking with primary weapon
	bool IsReadyToSecondaryAttack();
	void SecondaryAttack(SpriteAnimation* sprite_animation);
	/*! \brief If the player is currently attacking directly (i.e. not via projectile) */
	bool IsDirectlySecondaryAttacking();
	int GetSecondaryAttackDamage();

	double GetShieldDistanceFromPlayer();

	// Functions for defending with primary weapon slot
	bool IsReadyToPrimaryDefend();
	bool IsPrimaryDefending();
	/*! \brief Start to defend with primary weapon slot */
	void PrimaryDefend();
	/*! \brief Stop defending with primary weapon slot */
	void StopPrimaryDefend();
	/*! \brief Defend blow with primary weapon slot 
	 * @param  damage amount of damage 
	 * @return true   if this blow broke defense */
	bool PrimaryDefendBlock(int damage);
	double GetPrimaryDefendingAngle();

	// Functions for defending with secondary weapon slot
	bool IsReadyToSecondaryDefend();
	bool IsSecondaryDefending();
	/*! \brief Start to defend with secondary weapon slot */
	void SecondaryDefend();
	/*! \brief Stop defending with secondary weapon slot */
	void StopSecondaryDefend();
	/*! \brief Defend blow with secondary weapon slot
	* @param  damage amount of damage
	* @return true   if this blow broke defense */
	bool SecondaryDefendBlock(int damage);
	double GetSecondaryDefendingAngle();

	bool IsReadyToDrinkPotion();
	void DrinkHealthPotion();

	double GetXP();
	void SetXP(double xp);
	/*! \brief Give the player XP
	*
	* @param  double xp to add to the player
	* @return true if the increase caused at least one level up
	*/
	bool AddXP(double xp);
	double GetXPForNextLevel();
	int GetLevel();

	double GetGold();
	void SetGold(double gold);
	void AddGold(double gold);

	double GetHealth();
	double GetMaxHealth();

	/*! \brief Set player's health explicitly
	*
	* @param  double health the new health value
	* @return true if this update caused death
	*/
	bool SetHealth(double health);

	void SetMaxHealth(double max_health);

	/*! \brief Apply damage to the player (will be adjusted for equipped armour and other defenses)
	*
	* @param  double damage to apply to the player (before accounting for their armour)
	* @return true if this update caused death
	*/
	bool ApplyDamage(double damage);

	/*! \brief Apply damage to the player (Does not account for armour - that should be pre-calculated)
	*
	* @param  double damage to apply to the player
	* @return true if this update caused death
	*/
	bool ApplyRawDamage(double damage);

	bool IsAlive();

	bool IsInInvincibilityFrames();

	std::vector<std::shared_ptr<Item>> GetItems();
	std::vector<std::shared_ptr<Item>> GetItems(ItemType item_type);
	void SetItems(std::vector<std::shared_ptr<Item>> items);
	void AddItem(std::shared_ptr<Item>);
	void RemoveItem(std::shared_ptr<Item>);
	bool HasQuestItem(QuestItem quest_item_id);
	void RemoveQuestItem(QuestItem quest_item_id);
	int GetNumberOfHealthPotions();

	/*! \brief Equip an item to a slot. Put the existing item back in inventory.
	*
	* @param slot the slot to equip to
	* @param item the item to equip
	*/
	void EquipItemInSlot(EquipSlot slot, std::shared_ptr<Item> item);

	std::shared_ptr<ArmourItem> GetEquippedArmour();
	void SetEquippedArmour(std::shared_ptr<ArmourItem> armour);
	int GetArmourLevel();

	std::shared_ptr<WeaponItem> GetPrimaryEquippedWeapon();
	void SetPrimaryEquippedWeapon(std::shared_ptr<WeaponItem> weapon);
	std::shared_ptr<WeaponItem> GetSecondaryEquippedWeapon();
	void SetSecondaryEquippedWeapon(std::shared_ptr<WeaponItem> weapon);

	SpriteAnimation* GetPrimaryWeaponAnimation();
	int GetPrimaryCurrentWeaponAnimationFrame();
	SpriteAnimation* GetSecondaryWeaponAnimation();
	int GetSecondaryCurrentWeaponAnimationFrame();

	double SellingPriceFor(std::shared_ptr<Item> item);
	double BuyingPriceFor(std::shared_ptr<Item> item);

private:
	double x;
	double y;
	// Track last position 
	double previous_x = 0;
	double previous_y = 0;
	SpriteAnimation* sprite_animation;
	int animation_position = 0;
	double movement_speed = 1.5;
	int width = 14;
	int height = 28;
	bool facing_right = true;
	bool moving = false;
	bool pointing_up = false;
	bool pointing_down = false;
	bool pointing_left = false;
	bool pointing_right = false;

	// Attack data
	int primary_attack_cooldown_duration = 10;
	int primary_attack_cooldown_current = 0;
	bool directly_primary_attacking = false;
	int secondary_attack_cooldown_duration = 10;
	int secondary_attack_cooldown_current = 0;
	bool directly_secondary_attacking = false;

	// Defend data
	double shield_distance_from_player = 16;
	int primary_defend_cooldown_duration = 20;
	int primary_defend_cooldown_current = 0;
	bool directly_primary_defending = false;
	double primary_defending_angle = 0;
	int secondary_defend_cooldown_duration = 20;
	int secondary_defend_cooldown_current = 0;
	bool directly_secondary_defending = false;
	double secondary_defending_angle = 0;

	// Item usage data
	int potion_cooldown_duration = 60;
	int potion_cooldown_current = 0;
	int kPotionHealingAmount = 25;

	double max_health = 100;
	double current_health = 100;
	bool alive = true;
	int total_invincibility_frames = 10;
	int current_invincibility_frames = 0;

	double gold = 0;
	double xp = 0;
	int level = 1;

	double xp_needed_for_level(double level);
	/*! \brief Level up the player by one level */
	void LevelUp();

	std::vector<std::shared_ptr<Item>> items;

	// Effectiveness at trading
	double mercantile_ability = 0.3;

	std::shared_ptr<ArmourItem> equipped_armour = nullptr;
	std::shared_ptr<WeaponItem> equipped_primary_weapon = nullptr;
	std::shared_ptr<WeaponItem> equipped_secondary_weapon = nullptr;

	SpriteAnimation* primary_weapon_animation;
	int primary_weapon_animation_current_frame = 0;
	SpriteAnimation* secondary_weapon_animation;
	int secondary_weapon_animation_current_frame = 0;
};

