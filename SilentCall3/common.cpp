#pragma once
#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>

static const std::string kAssetDir = "..\\..\\assets";
static const std::string kLogFilename = "Silent_Call_3.log";

enum GameState { gamestate_title_screen, gamestate_village, gamestate_playing };
enum DungeonMapType{ village_maptype, forest1, forest2, forest3, graveyard, caves };

enum TileType { empty, edge_top, solid, edge_right, edge_left, edge_bottom, 
	            corner_bottom_left, corner_bottom_right, corner_top_left, corner_top_right,
                jutting_corner_bottom_left, jutting_corner_bottom_right, jutting_corner_top_left, jutting_corner_top_right,
	            low_edge_top, low_solid, low_edge_right, low_edge_left, low_edge_bottom,
	            low_corner_bottom_left, low_corner_bottom_right, low_corner_top_left, low_corner_top_right,
	            low_jutting_corner_bottom_left, low_jutting_corner_bottom_right, low_jutting_corner_top_left, low_jutting_corner_top_right};

static const int MAP_TILE_SIZE = 32;

enum TownMenuOptionId { town_ventureforth, town_quests, town_system, town_merchant, town_banker,
						quests_availablequests, quests_forfeit, quests_turnin, quests_feats, quests_back, 
						system_options, system_save, system_saveandquit, system_back};

enum QuestState { quest_ready, quest_in_progress, quest_complete, quest_failed };
enum QuestType { hunt, assassinate, explore, find, MAX };
enum QuestCompletionCriteriaType { defeat_an_enemy, defeat_all_enemies, reach_a_location, find_an_item };
enum QuestLineType { no_questline, village_questline, merchant_questline, banker_questline };

enum EnemyGeneratorType { yurt, tree, stump, bonepile, grave, hole, rock_mound_small, rock_mound };

enum EnemyType { goblin, crow, spider, shroom, mad_skeleton, bad_skeleton, zombie, slime };

enum EnemyMovementState { idle, wandering, alarmed, targeting };

enum ClutterGroup { clutter_group_basic_wares, clutter_group_funerary };
enum ClutterType { small_vase, big_vase, barrel, small_crate, medium_crate, small_funerary_vase, big_funerary_vase, coffin };
static const double CHANCE_CLUTTER_SPOT_POPULATED = 0.2;
static const int CLUTTER_SPREAD_RADIUS = 32;

enum DropTableType { no_drops, only_gold, gold_potion, gold_potion_weapon_armour,
	                 sometimes_unique_armour, sometimes_unique_weapon,
                     rarely_unique_armour, rarely_unique_weapon};
// Types of drop items i.e. classes of items that might appear on the ground
enum DropItemType { drop_item_nothing, drop_item_weapon_random, drop_item_armour_random, drop_item_accessory,
	                drop_item_gold, drop_item_potion, drop_item_armour_unique, drop_item_weapon_unique, drop_item_quest };

// Actual types of items
enum ItemType { item_type_weapon, item_type_armour, item_type_accessory, item_type_potion, item_type_quest };

static ItemType string_to_item_type(std::string item_type_string)
{
	if (item_type_string == "0")
	{
		return item_type_weapon;
	}
	if (item_type_string == "1")
	{
		return item_type_armour;
	}
	if (item_type_string == "2")
	{
		return item_type_accessory;
	}
	if (item_type_string == "3")
	{
		return item_type_potion;
	}
	if (item_type_string == "4")
	{
		return item_type_quest;
	}
	BOOST_LOG_TRIVIAL(warning) << "Unhandled item type string '" << item_type_string << "' - defaulting to weapon.";
	return item_type_weapon;
}

//Types of weapons
enum WeaponType { weapon_type_sword, weapon_type_staff, weapon_type_axe, weapon_type_spear, weapon_type_club, weapon_type_bow, weapon_type_shield, weapon_type_MAX };

static WeaponType string_to_weapon_type(std::string weapon_type_string)
{
	if (weapon_type_string == "0")
	{
		return weapon_type_sword;
	}
	if (weapon_type_string == "1")
	{
		return weapon_type_staff;
	}
	if (weapon_type_string == "2")
	{
		return weapon_type_axe;
	}
	if (weapon_type_string == "3")
	{
		return weapon_type_spear;
	}
	if (weapon_type_string == "4")
	{
		return weapon_type_club;
	}
	if (weapon_type_string == "5")
	{
		return weapon_type_bow;
	}
	if (weapon_type_string == "6")
	{
		return weapon_type_shield;
	}
	BOOST_LOG_TRIVIAL(warning) << "Unhandled weapon type string '" << weapon_type_string << "' - defaulting to sword.";
	return weapon_type_sword;
}

// ID unique quest items
enum QuestItem {quest_item_none, quest_item_merchant_guild_amulet, quest_item_banker_clue };

static QuestItem string_to_quest_item(std::string quest_item_string)
{
	if (quest_item_string == "1")
	{
		return quest_item_merchant_guild_amulet;
	}
	if (quest_item_string == "2")
	{
		return quest_item_banker_clue;
	}
	BOOST_LOG_TRIVIAL(warning) << "Unhandled quest item string '" << quest_item_string << "' - defaulting to none.";
	return quest_item_none;
}

enum PotionType { potion_type_healing };

static PotionType string_to_potion_type(std::string potion_type_string) {
	if (potion_type_string == "0")
	{
		return potion_type_healing;
	}
	BOOST_LOG_TRIVIAL(warning) << "Unhandled potion type string '" << potion_type_string << "' - defaulting to healing potion.";
	return potion_type_healing;
}

// All the dialog types
enum UIDialogType { uidlg_generic, uidlg_number_input, uidlg_message,
	                uidlg_available_quests, uidlg_quest_completed, 
	                uidlg_merchant_welcome, uidlg_merchant_invest, uidlg_merchant_buy, uidlg_merchant_sell,
	                uidlg_banker_welcome, uidlg_banker_deposit, uidlg_banker_withdraw,
	                uidlg_pausegame, uidlg_character_inventory, uidlg_character_equip, uidlg_equip_change };

enum EquipSlot { equip_slot_primary_weapon, equip_slot_secondary_weapon, equip_slot_armour };

/*! \brief Set a SDL_Texture pointer to a texture loaded from PNG file
*  \todo  Improve error reporting from this function
*
* @param *ren Pointer to SDL renderer against which to create the texture
* @param path File path to the PNG file to open
*/
static SDL_Texture* LoadTextureFromPng(SDL_Renderer *ren, std::string path)
{
	SDL_Texture *result;
	SDL_Surface *png = IMG_Load(path.c_str());
	result = SDL_CreateTextureFromSurface(ren, png);
	SDL_FreeSurface(png);
	return result;
}

/*! \brief Convert an entity's radian angle to a rendering angle
*
* Convert radians from the math system to degrees on the screen.
* Because the math system points toward the right of the screen and rotates anti-clockwise on the screen,
* it's necessary to multiply the radians by -1 (to have it rotate the other direction)
*
* @param  radians The angle in radians that this entity is facing
* @return         The angle in degrees to render the sprite for this entity at
*/
static double to_render_angle(double radians)
{
	return 0.0 - ((radians * 180.0) / M_PI);
}

static double distance_between(double x1, double y1, double x2, double y2)
{
	return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

/*! \brief detect collision given the width, height and centre x,y of two objects
*
* @param x1 x ordinate of centre of first object
* @param y1 y ordinate of centre of first object
* @param w1 width of first object
* @param h1 height of first object
* @param x2 x ordinate of centre of second object
* @param y2 y ordinate of centre of second object
* @param w2 width of second object
* @param h2 height of second object
* @return   true if the two bounding rects overlap
*/
static bool CheckCollisionFromCentre(double x1, double y1, int w1, int h1,
									 double x2, double y2, int w2, int h2)
{
	double left1 = x1 - (w1 / 2);
	double left2 = x2 - (w2 / 2);
	double right1 = x1 + (w1 / 2);
	double right2 = x2 + (w2 / 2);
	double top1 = y1 - (h1 / 2);
	double top2 = y2 - (h2 / 2);
	double bottom1 = y1 + (h1 / 2);
	double bottom2 = y2 + (h2 / 2);

	BOOST_LOG_TRIVIAL(trace) << "Checking collision between left,right,top,bottom (" << left1 << "," << right1 << "," << top1 << "," << bottom1 << ")"
		<< " and (" << left2 << "," << right2 << "," << top2 << "," << bottom2 << ")";
	if (right1 >= left2)
	{
		BOOST_LOG_TRIVIAL(trace) << "Collision check r1 >= l2 ...";
		if (left1 <= right2)
		{
			BOOST_LOG_TRIVIAL(trace) << "... l1 <= r2 ...";
			if (top1 <= bottom2)
			{
				BOOST_LOG_TRIVIAL(trace) << "... t1 <= b2 ...";
				if (bottom1 >= top2)
				{
					BOOST_LOG_TRIVIAL(trace) << "Collision detected between left,right,top,bottom (" << left1 << "," << right1 << "," << top1 << "," << bottom1 << ")"
						                     << " and (" << left2 << "," << right2 << "," << top2 << "," << bottom2 << ")";
					return true;
				}
			}
		}
	}
	return false;
}

/*! \brief Create an SDL_Texture for a given string in a given TTF font
*
* @param *ren         Pointer to SDL renderer against which to create the texture
* @param font         Pointer to the TTF_Font to render the string in
* @param texture_text The string to render
* @param text_color   Color to render the font
* @param wrap_length  Number of pixels to wrap at
*/
static SDL_Texture* CreateTextureForString(SDL_Renderer *ren, TTF_Font& font, std::string texture_text, SDL_Color text_color, int wrap_length)
{

	SDL_Texture* result = nullptr;

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped(&font, texture_text.c_str(), text_color, wrap_length);
	if (textSurface == NULL)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError();
	}
	else
	{
		//Create texture from surface pixels
		result = SDL_CreateTextureFromSurface(ren, textSurface);
		if (result == NULL)
		{
			BOOST_LOG_TRIVIAL(fatal) << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError();
		}

		//Dispose of the surface
		SDL_FreeSurface(textSurface);
	}

	return result;
}

/*! \brief For a non-integer number; return a string with only integer digits
*
*/
static std::string StringForWholePart(double number)
{
	std::string s = std::to_string(number);
	size_t dotIndex = s.find(".");
	return s.substr(0, dotIndex);
}

static double RandomDoubleZeroToOne()
{
	return rand() / (RAND_MAX + 1.);
}