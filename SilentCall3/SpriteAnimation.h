#pragma once
#include <SDL.h>
#include <vector>
/*! \file SpriteAnimation.h
* A class representing a sprite animation by a series of positions on a single sprite sheet*/
class SpriteAnimation
{
public:

	struct AnimationFrame
	{
		int x;
		int y;
	};

	SpriteAnimation();
	/*! \brief Create a new SpriteAnimation
	 *
	 * @param sheet    Pointer to an SDL_Texture that is the "sheet" of animation frames to pick from 
	 * @param w        The width of this sprite
	 * @param h        The height of this sprite
	 * @param duration Number of frames of the game each frame of this animation should persist
	 * @param frames   An std::vector of AnimationFrame structs; one per frame of animation
	 */
	SpriteAnimation(SDL_Texture* sheet, int w, int h, int duration, std::vector<AnimationFrame>& aFrames);
	~SpriteAnimation();

	SDL_Texture* GetSpriteSheet();

	/*! \brief Get the rectangle of the sprite sheet for the current frame of animation
	 *
	 * @param  step Which frame of animation to get the rectangle for
	 * @return An SDL_Rect with the x,y,w,h on the sprite sheet for this frame of animation 
	 */
	SDL_Rect GetAnimationFrame(int step);

	/*! \brief Get the length of this animation
	*
	* @return The number of frames in this animation
	*/
	int GetAnimationLength();

private:
	SDL_Texture* spritesheet;
	int sprite_width;
	int sprite_height;
	int frame_duration;
	std::vector<AnimationFrame> frames;
};

