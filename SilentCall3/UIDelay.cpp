#include "UIDelay.h"

UIDelay::UIDelay(int aDuration)
{
	duration = aDuration;
}

UIDelay::~UIDelay()
{
}

void UIDelay::Cooldown()
{
	if (ticks > 0)
	{
		ticks -= 1;
	}
}

void UIDelay::Activate()
{
	ticks = duration;
}

bool UIDelay::Ready()
{
	return ticks == 0;
}

int UIDelay::GetTicksRemaining()
{
	return ticks;
}

int UIDelay::GetDuration()
{
	return duration;
}
