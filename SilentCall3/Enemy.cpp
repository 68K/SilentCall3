#include "Enemy.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
	delete(&point_of_interest);
}

Enemy::Enemy(EnemyType type, int x, int y)
	: Actor()
{
	this->type = type;
	this->SetX(x);
	this->SetY(y);
}

EnemyType Enemy::GetType()
{
	return type;
}

void Enemy::SetType(EnemyType type)
{
	this->type = type;
}

int Enemy::GetBestiaryId()
{
	return bestiary_id;
}

void Enemy::SetBestiaryId(int id)
{
	this->bestiary_id = id;
}

double Enemy::GetMovementSpeed()
{
	return movement_speed;
}

void Enemy::SetMovementSpeed(double speed)
{
	movement_speed = speed;
}

bool Enemy::GetIfMoving()
{
	return moving;
}

void Enemy::SetIfMoving(bool moving)
{
	this->moving = moving;
}

EnemyMovementState Enemy::GetMovementState()
{
	return movement_state;
}

void Enemy::SetMovementState(EnemyMovementState state)
{
	this->movement_state = state;
}

std::pair<double, double> Enemy::GetPointOfInterest()
{
	return point_of_interest;
}

void Enemy::SetPointOfInterest(double x, double y)
{
	point_of_interest.first = x;
	point_of_interest.second = y;
}

double Enemy::GetAlertRadius()
{
	return alert_radius;
}

void Enemy::SetAlertRadius(double radius)
{
	this->alert_radius = radius;
}

void Enemy::Alarm()
{
	this->SetMovementState(alarmed);
	alarm_current_frames = alarm_duration_frames;
}

bool Enemy::StillAlarmed()
{
	return alarm_current_frames > 0;
}

bool Enemy::CanBeAlarmed()
{
	return alarm_duration_frames > 0;
}

void Enemy::SetAlarmDuration(int duration)
{
	this->alarm_duration_frames = duration;
}

double Enemy::GetMeleeDamage()
{
	return this->melee_damage;
}

void Enemy::SetMeleeDamage(double damage)
{
	this->melee_damage = damage;
}

double Enemy::GetProjectileDamage()
{
	return this->projectile_damage;
}

void Enemy::SetProjectileDamage(double damage)
{
	this->projectile_damage = damage;
}

bool Enemy::IsStunned()
{
	return is_stunned;
}

void Enemy::Stun(int duration)
{
	is_stunned = true;
	stun_duration = duration;
}

int Enemy::GetLevel()
{
	return this->level;
}

void Enemy::SetLevel(int level)
{
	this->level = level;
}

int Enemy::GetGold()
{
	return gold;
}

void Enemy::SetGold(int gold)
{
	this->gold = gold;
}

int Enemy::GetXP()
{
	return xp;
}

void Enemy::SetXP(int xp)
{
	this->xp = xp;
}

DropTableType Enemy::GetDropTableType()
{
	return drop_table_type;
}

void Enemy::SetDropTableType(DropTableType type)
{
	this->drop_table_type = type;
}

void Enemy::Update()
{
	this->ActorUpdate();
	if (is_stunned)
	{
		stun_duration--;
		if (stun_duration <= 0)
		{
			is_stunned = false;
			stun_duration = 0;
		}
	}
	if (alarm_current_frames > 0)
	{
		alarm_current_frames--;
	}
}