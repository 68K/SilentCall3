#include "common.cpp"

#pragma once
/*! \file PlayerInput.h 
 * A class representing the player's input on devices connected to the PC.
 * Abstracts device state and allows the game to query what actions the player has indicated
 * Non-static so that input data can be bundled each frame and SDL doesn't need to open devices for each individual check */
class PlayerInput
{
public:
	PlayerInput();
	~PlayerInput();

	/*! \brief Enumerate and connect to player input devices
	 * \todo Currently assumes one gamepad. Need to support player picking up one of several gamepads or choosing keyboard*/
	void InitialiseInputDevices();

	/*! \brief Update the state of input devices*/
	void UpdatePlayerInput();

	/* \brief If the player has indicated quitting the game
	 * @return true if player has pressed input that indicates quitting, false otherwise*/
	bool ExitPressed();

	/* \brief If the player has indicated moving left on the screen
	* @return true if player has pressed input that indicates moving left on the screen, false otherwise*/
	bool LeftPressed();

	/* \brief If the player has indicated moving right on the screen
	* @return true if player has pressed input that indicates moving right on the screen, false otherwise*/
	bool RightPressed();

	/* \brief If the player has indicated moving up on the screen
	* @return true if player has pressed input that indicates moving up on the screen, false otherwise*/
	bool UpPressed();

	/* \brief If the player has started indicating moving up on the screen
	* @return true if player has tapped input that indicates moving up on the screen, false otherwise*/
	bool UpTapped();

	/* \brief If the player has indicated moving down on the screen
	* @return true if player has pressed input that indicates moving down on the screen, false otherwise*/
	bool DownPressed();

	/* \brief If the player has started indicating moving down on the screen
	* @return true if player has tapped input that indicates moving down on the screen, false otherwise*/
	bool DownTapped();

	/* \brief If the player is indicating menu confirmation
	* @return true if player is pressing input that indicates menu confirmation, false otherwise*/
	bool ConfirmPressed();

	/* \brief If the player has started indicating menu confirmation
	* @return true if player started pressing input that indicates menu confirmation, false otherwise*/
	bool ConfirmTapped();

	/* \brief If the player is currently pressing Start e.g. on the title menu or when paused
	* @return true if player is currently input that indicates wanting to start, false otherwise*/
	bool StartPressed();

	/* \brief If the player has tapped Start e.g. on the title menu or when paused
	* @return true if player has tapped input that indicates wanting to start, false otherwise*/
	bool StartTapped();

	/* \brief If the player has indicated pressing Cancel e.g. on a title menu
	* @return true if player has pressed input that indicates wanting to back up, false otherwise*/
	bool CancelPressed();

	/* \brief If the player has indicated attacking with their primary weapon
	* @return true if player has pressed input that indicates wanting to attack with their primary weapon, false otherwise*/
	bool PrimaryAttackPressed();

	/* \brief If the player has indicated attacking with their secondary weapon
	* @return true if player has pressed input that indicates wanting to attack with their secondary weapon, false otherwise*/
	bool SecondaryAttackPressed();

	/* \brief If the player has indicated using a health potion
	* @return true if player has pressed input that indicates wanting to use a health potion, false otherwise*/
	bool HealthPotionPressed();

private:
	bool last_start_pressed = false;
	bool last_up_pressed = false;
	bool last_down_pressed = false;
	bool last_confirm_pressed = false;
};