#pragma once
#include "common.cpp"
#include "Enemy.h"
#include "SpriteAnimation.h"

class EnemyGenerator: public Actor
{
public:
	EnemyGenerator();
	EnemyGenerator(EnemyGeneratorType type, int x, int y);
	~EnemyGenerator();

	EnemyGeneratorType GetEnemyGeneratorType();

	double GetSpawnRadius();

	// Step forward sprite animation etc
	void Update();

	// Check if ready to spawn an enemy
	bool GetIfReadyToGenerateEnemy();

	// Generate an enemy - return an enemy type to spawn and reset spawn count
	EnemyType GenerateEnemy();

private:
	EnemyGeneratorType type = yurt;

	int frames_between_spawns = 100;
	int frames_since_last_spawn = 0;
	double spawn_radius = 260;
};

