#pragma once
#include <string>
#include <vector>
#include "common.cpp"

/*! \file TmxMap.h
* A class representing a map in the .tmx format 
* Assumes there is a 'Solid walls' and 'Visible tiles' layer*/
class TmxMap
{
public:
	TmxMap();
	TmxMap(std::string filename);
	~TmxMap();

	std::string GetTilemapFilename();
	int GetWidth();
	int GetHeight();
	int GetTilesetColumnCount();
	int GetTileSize();
	unsigned* GetSolidWalls();
	unsigned* GetVisibleTiles();
	unsigned* GetFogTiles();
	void SetFogTileOpacity(int x, int y, unsigned opacity);
	std::pair<int, int>* PickRandomStartPoint();
	std::pair<int, int>* PickRandomExitPoint();
	bool HasExitTo(DungeonMapType destination);
	std::pair<int, int>* GetExitPointForDestination(DungeonMapType destination);
	std::vector<std::pair<int, int>*> GetEnemySpawnPoints();
	std::vector<std::pair<int, int>*> GetClutterPoints();
	/*! \brief Get the type of solid wall at X,Y
	*
	* @param x X ordinate in pixels that the desired tile is at
	* @param y Y ordinate in pixels that the desired tile is at
	* @return Solid wall TileType that x,y lands at
	*/
	virtual TileType GetSolidWallTileTypeAt(int x, int y);

private:
	void ReadInBase64TileData(std::string raw_data, unsigned* destination, int width, int height);
	int width;
	int height;
	unsigned* solid_walls;
	unsigned* visible_tiles;
	unsigned* fog_tiles;

	std::vector<std::pair<int, int>*> player_spawn_points;
	std::vector<std::pair<int, int>*> player_exit_points;
	// Map of where exits lead, first being the map it leads to, second being the index of player_exit_points that leads to that map
	std::map<int, int> player_exit_destinations;
	std::vector<std::pair<int, int>*> enemy_spawn_points;
	std::vector<std::pair<int, int>*> clutter_points;

	int tile_size;
	int tileset_columns;
	std::string tileset_image_source;
};