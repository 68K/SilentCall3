#pragma once
#include <SDL.h>
#include "SpriteAnimation.h"

/*! \file Building.h
 * A class representing a building in the village */
class Building
{
public:
	Building();
	Building(SDL_Rect location, SpriteAnimation* sprite_anim);
	~Building();

	SDL_Rect GetTileLocationRect();
	void SetTileLocationRect(SDL_Rect location);

	SpriteAnimation* GetSpriteAnimation();

	int GetCurrentAnimationFrameIndex();
	void StepForwardAnimation();

private:
	SDL_Rect tile_location_rect;
	SpriteAnimation* sprite_animation;
	int current_animation_frame = 0;
};

