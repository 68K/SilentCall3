#include "PresentationMap.h"

PresentationMap::PresentationMap()
{
	// Map file names to map types
	DungeonMapFileNames[village_maptype] = kAssetDir + "\\maps\\village.tmx";
	DungeonMapFileNames[forest1] = kAssetDir + "\\maps\\Forest.tmx";
	DungeonMapFileNames[forest2] = kAssetDir + "\\maps\\Forest2.tmx";
	DungeonMapFileNames[graveyard] = kAssetDir + "\\maps\\Graveyard.tmx";
}

PresentationMap::~PresentationMap()
{
	if (tmx_map != nullptr)
	{
		delete(tmx_map);
	}
	SDL_DestroyTexture(texture_walls);
}

void PresentationMap::LoadTmxMap(DungeonMapType map_type)
{
	BOOST_LOG_TRIVIAL(debug) << "Loading presentation map of map type '" << map_type << "'";
	this->map_type = map_type;

	tmx_map = new TmxMap(DungeonMapFileNames[map_type]);
	w = tmx_map->GetWidth();
	h = tmx_map->GetHeight();
	tileset_columns = tmx_map->GetTilesetColumnCount();
	tile_size = tmx_map->GetTileSize();
}

void PresentationMap::LoadTextures(SDL_Renderer *ren)
{
	std::string wall_texture_filename = tmx_map->GetTilemapFilename();
	if (wall_texture_filename.empty())
	{
		BOOST_LOG_TRIVIAL(error) << "Tile map does not have a filename for its tileset";
		throw "Tile map does not have a filename for its tileset";
	}

	texture_walls = LoadTextureFromPng(ren, kAssetDir + "\\" + wall_texture_filename);

	if (texture_walls == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load " << wall_texture_filename;
		throw "Failed to load " + wall_texture_filename;
	}
}

void PresentationMap::Render(SDL_Renderer *ren, int centre_pixel_x, int centre_pixel_y)
{
	int render_width = 0;
	int render_height = 0;
	SDL_GetRendererOutputSize(ren, &render_width, &render_height);
	int full_tiles_to_render_x = render_width / tile_size;
	int full_tiles_to_render_y = render_height / tile_size;
	// The nominal centre point, if tile 0,0 was at pixel 0,0
	int nominal_centre_x = (render_width / 2);
	int nominal_centre_y = (render_height / 2);
	// The leftmost full tile to render
	int min_tile_x = (int)fmax(0, (centre_pixel_x / tile_size) - (full_tiles_to_render_x / 2));
	// The rightmost full tile to render
	int max_tile_x = (int)fmin(w, (centre_pixel_x / tile_size) + (full_tiles_to_render_x / 2) + 1);
	// The topmost full tile to render
	int min_tile_y = (int)fmax(0, (centre_pixel_y / tile_size) - (full_tiles_to_render_y / 2));
	// The bottommost full tile to render
	int max_tile_y = (int)fmin(h, (centre_pixel_y / tile_size) + (full_tiles_to_render_y / 2) + 1);
	//BOOST_LOG_TRIVIAL(debug) << "Drawing tiles " << min_tile_x << "," << min_tile_y << " to " << max_tile_x << "," << max_tile_y;

	for (int i = min_tile_x; i < max_tile_x; i++)
	{
		for (int j = min_tile_y; j < max_tile_y; j++)
		{
			SDL_Rect rect_to_draw;
			int tile_to_draw = tmx_map->GetVisibleTiles()[(j * w) + i];
			rect_to_draw.x = (tile_to_draw % tileset_columns) * tile_size;
			rect_to_draw.y = (tile_to_draw / tileset_columns) * tile_size;
			rect_to_draw.w = tile_size;
			rect_to_draw.h = tile_size;

			SDL_Rect dest_rect = SDL_Rect{ i * MAP_TILE_SIZE + (nominal_centre_x - centre_pixel_x), j * MAP_TILE_SIZE + (nominal_centre_y - centre_pixel_y), MAP_TILE_SIZE, MAP_TILE_SIZE };
			SDL_RenderCopy(ren, texture_walls, &rect_to_draw, &dest_rect);
		}
	}
}

std::vector<Npc*> PresentationMap::GetNpcs()
{
	BOOST_LOG_TRIVIAL(trace) << "PresentationMap::GetNpcs returning std::vector<Npc*> of length " << npcs.size();
	return npcs;
}

void PresentationMap::AddNpc(Npc* npc)
{
	this->npcs.push_back(npc);
}

void PresentationMap::ClearNpcs()
{
	BOOST_LOG_TRIVIAL(debug) << "Clearing NPCs from presentation map";
	npcs.clear();
}

int PresentationMap::GetWidthInPixels()
{
	return w * tile_size;
}

int PresentationMap::GetHeightInPixels()
{
	return h * tile_size;
}