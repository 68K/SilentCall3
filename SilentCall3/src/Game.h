#pragma once
#include <vector>
#include "../common.cpp"
#include "Bestiary.h"
#include "../Building.h"
#include "../DungeonMap.h"
#include "Furniture.h"
#include "items/ItemGenerator.h"
#include "../Player.h"
#include "../PlayerInput.h"
#include "PresentationMap.h"
#include "Projectile.h"
#include "../Quest.h"
#include "quests/QuestLine.h"
#include "quests/QuestSpec.h"
#include "SpriteAnimationFrames.h"
#include "../TownMenu.h"
#include "../UIDelay.h"
#include "ui/UIDialog.h"
#include "ui/UIIcon.h"

/*! \file Game.h
* A class representing the game as a class, encapsulates all the data and functions for the game */
class Game
{
public:
	Game();
	~Game();

	void SetConfigPath(std::string config_path);
	void LoadConfig();

	void SetSavePath(std::string save_path);
	std::string GetSavePath();
	void LoadSaveFile(std::string save_filename);
	bool WriteSaveFile(std::string save_filename);

	bool Quit();

	void SetTownMenuMainOptions();
	
	Quest* GenerateQuest();

	Quest* QuestFromQuestSpec(QuestSpec& spec);

	/*! \brief If current quest is completed then update quest lines
	*
	*/
	void UpdateQuestLines();

	/*! \brief Fill quest list with available quest line quests then fill remainder with random quests
	*
	*/
	void FillQuestList();

	/*! \brief Prepare gameplay for a new quest e.g. load the map
	*
	* @param Quest* quest Pointer to the quest object to prepare for
	*/
	void PrepareQuest(Quest& quest);

	/*! \brief Yield quest reward(s) and unset the current quest
	*
	*/
	void TurnInQuest();

	std::shared_ptr<DroppedItem> QuestItemFromQuestItemId(QuestItem quest_item_id, double x, double y, SpriteAnimation* sprite_animation);

	/*! \brief During a quest, transition to another map
	*
	* @param DungeonMapType map the map to transition to
	*/
	void TransitionToMap(DungeonMapType map);

	/*! \brief Transition from playing back to the title screen
	*
	*/
	void TransitionPlayingToTitle();

	bool AnyDialogOpen();

	/*! \brief Initialise the game. Only actions that are executed once when the game boots belong here*/
	void Initialise();

	/*! \brief Initialise a new game. Executed every time a new game is started. */
	void NewGame();

	void InitialiseInput();

	bool SetDisplay();

	bool LoadAssets();

	void DefineSprites();

	void Update();

	void Render();

private:

	bool quit = false;

	// A renderer
	SDL_Renderer *ren = nullptr;
	// A window
	SDL_Window *win = nullptr;
	// A static sized texture to render the game to. The actual user display has this stretched onto it
	SDL_Texture *static_sized_texture = nullptr;
	// A basic white texture that the game can use to draw simple shapes 
	SDL_Texture *basic_128_texture = nullptr;
	// A circular white texture loaded from file
	SDL_Texture* circular_texture = nullptr;

	// Path to save game files under
	std::string config_file_path = ".";
	std::string config_filename = "SilentCall3.cfg";
	std::string save_file_path = ".";
	std::string save_game_filename = "SaveGame.dat";

	int window_width = 1600;	// Width of window, if displaying in a window
	int window_height = 900;	// Height of window, if displaying in a window
	int display_width = 1600;	// Width of display (actual value to be detected at start of main program)
	int display_height = 900;	// Height of display (actual value to be detected at start of main program)
	int render_width = 1600;	// Width to render game at (to be stretched to window/display)
	int render_height = 900;	// Height to render game at (to be stretched to window/display)
	SDL_Rect display_rect;		// Rect on the full display to render the game to, to preserve aspect
	bool fullscreen = false;
	const char* window_title = "Silent Call 3";

	int map_display_centre_pixel_x = 0; // X ordinate on the dungeon map (in pixels, not tiles) to centre view on
	int map_display_centre_pixel_y = 0; // Y ordinate on the dungeon map (in pixels, not tiles) to centre view on

	GameState current_game_state = gamestate_title_screen;

	bool paused = false;

	Bestiary* bestiary = nullptr;
	
	// Village map details
	PresentationMap* village_map;
	int kVillageMapWidth = 32;
	int kVillageMapHeight = 22;
	int kVillageMapTopMargin = 64;
	int kVillageMapLeftMargin = 64;
	int kVillageMapTileSize = 32;
	std::vector<Building*> buildings;
	int kVillageMenuLeftMargin = render_width - 300;
	int kVillageMenuTopMargin = kVillageMapTopMargin + 64;
	int kMaxNumberOfQuestsInVillage = 8;
	std::vector<Quest*> available_quests_in_village;
	std::vector<UIDialog*> open_dialogs; // Vector of open UI dialogs
	TownMenu* current_town_menu = nullptr;
	int current_town_menu_selection = 0;
	// Vector to store all UIDelays so they can be cooled down in one go
	std::vector<UIDelay*> ui_delays;

	// Merchant details
	/*! \brief Add merchant to village (if not already added)
	*/
	void AddMerchantToVillage();
	/*! \brief Prepare the merchant inventory
	*/
	void FillMerchantInventory();
	bool is_merchant_in_village = false;
	int merchant_level = 1;
	int CostForNextMerchantLevel();
	std::vector<std::shared_ptr<Item>> merchant_items;
	int kMerchantBuildingLeft = 8;
	int kMerchantBuildingTop = 6;
	int kMerchantItemsMax = 8;

	//Banker details
	void AddBankerToVillage();
	bool is_banker_in_village = false;
	int kBankerBuildingLeft = 10;
	int kBankerBuildingTop = 8;
	int bank_balance = 0;
	double bank_interest_rate = 1.02;
	int bank_service_charge = 50;

	ItemGenerator* item_generator;
	std::map<int, std::string> item_description_index;

	// Dungeon details
	DungeonMap* current_dungeon_map = nullptr;

	Quest* current_quest = nullptr;
	bool quest_prepared = false;
	std::map<QuestLineType, QuestLine*> quest_lines;
	std::map<QuestLineType, int> questline_progress;

	/*! \brief Prepare the dungeon map, add enemies, furniture etc
	*/
	void PrepareDungeonMap();

	/*! \brief Everything that needs done when an enemy dies.
	*          Give player XP, drop loot in the level etc
	*/
	void KillEnemy(Enemy* enemy);

	/*! \brief Scale XP for an enemy according to player level
	*/
	double ScaleXP(double enemy_xp, int enemy_level);

	/*! \brief Everything that happens when a piece of clutter is destroyed 
	*          Set its health to 0, possible drop loot etc */
	void DestroyClutterItem(std::shared_ptr<ClutterItem> clutter_item);
	std::unique_ptr<DropTable> clutter_drop_table;

	// Player details
	Player* player;

	// Process a Game Over
	void GameOver();

	// \brief Process player directly attacking
	void PlayerDirectAttack(int damage);
	void StartPlayerAttack(WeaponItem weapon, bool is_primary_attack);

	SpriteAnimation* SpriteAnimationForEnemyState(EnemyType type, EnemyMovementState state);

	void DrawPlayerWeapon(bool is_primary_weapon);
	void DrawPlayerShield(bool is_primary_shield);

	PlayerInput* player_input;

	// Projectiles
	std::vector<std::unique_ptr<Projectile>> projectiles;

	// Fonts
	TTF_Font *ttffont_anglican_60 = nullptr;
	TTF_Font *ttffont_ui_36 = nullptr;
	TTF_Font *ttffont_ui_20 = nullptr;

	// UI String textures
	SDL_Texture* string_texture_ui_titlescreen_title = nullptr;

	// Define rects for UI elements
	SDL_Rect ui_element_rect_button = SDL_Rect{ 0, 0, 192, 92 };
	SDL_Rect ui_element_rect_player_health_bar = SDL_Rect{ 20, 20, 900, 16 };

	// UI Icons
	// TODO: These should be in a single collection
	UIIcon* ui_icon_health_potion;
	const int kNumberOfHealthPotionsShown = 10;
	UIIcon* ui_icon_down_arrow;
	UIIcon* ui_icon_up_arrow;
	UIIcon* ui_icon_effect_stun;

	// Sprite Sheets
	SDL_Texture* spritesheet_characters;
	SDL_Texture* spritesheet_furniture;
	SDL_Texture* spritesheet_buildings;
	SDL_Texture* spritesheet_ui;
	SDL_Texture* spritesheet_enemy_generators;
	SDL_Texture* spritesheet_effects;
	SDL_Texture* spritesheet_items;

	// Sprite Animations
	SpriteAnimationFrames* sprite_animation_frames;
	SpriteAnimation* sprite_animation_player_facing_right;
	SpriteAnimation* sprite_animation_player_moving_right;
	SpriteAnimation* sprite_animation_npc_male1_waving;
	SpriteAnimation* sprite_animation_npc_male1_merchant;
	SpriteAnimation* sprite_animation_npc_banker_facing_right;
	SpriteAnimation* sprite_animation_npc_banker_injured;
	SpriteAnimation* sprite_animation_enemy_goblin_facing_right;
	SpriteAnimation* sprite_animation_enemy_goblin_moving_right;
	SpriteAnimation* sprite_animation_enemy_goblin_alarmed_facing_right;
	SpriteAnimation* sprite_animation_enemy_crow_facing_right;
	SpriteAnimation* sprite_animation_enemy_crow_moving_right;
	SpriteAnimation* sprite_animation_enemy_spider_facing_right;
	SpriteAnimation* sprite_animation_enemy_spider_moving_right;
	SpriteAnimation* sprite_animation_enemy_shroom_facing_right;
	SpriteAnimation* sprite_animation_enemy_shroom_moving_right;
	SpriteAnimation* sprite_animation_enemy_madskeleton_facing_right;
	SpriteAnimation* sprite_animation_enemy_madskeleton_moving_right;
	SpriteAnimation* sprite_animation_enemy_badskeleton_facing_right;
	SpriteAnimation* sprite_animation_enemy_badskeleton_moving_right;
	SpriteAnimation* sprite_animation_enemy_zombie_facing_right;
	SpriteAnimation* sprite_animation_enemy_zombie_moving_right;
	SpriteAnimation* sprite_animation_enemy_slime_facing_right;
	SpriteAnimation* sprite_animation_enemy_slime_moving_right;

	SpriteAnimation* sprite_animation_furniture_map_exit;
	SpriteAnimation* sprite_animation_clutter_vase_small;
	SpriteAnimation* sprite_animation_clutter_vase_big;
	SpriteAnimation* sprite_animation_clutter_barrel;
	SpriteAnimation* sprite_animation_clutter_crate_small;
	SpriteAnimation* sprite_animation_clutter_crate_medium;
	SpriteAnimation* sprite_animation_clutter_funerary_vase_small;
	SpriteAnimation* sprite_animation_clutter_funerary_vase_big;
	SpriteAnimation* sprite_animation_clutter_coffin;
	SpriteAnimation* sprite_animation_player_camp;
	SpriteAnimation* sprite_animation_building_merchant_hut;
	SpriteAnimation* sprite_animation_building_banker_stall;
	SpriteAnimation* sprite_animation_enemy_generator_yurt;
	SpriteAnimation* sprite_animation_enemy_generator_tree;
	SpriteAnimation* sprite_animation_enemy_generator_stump;
	SpriteAnimation* sprite_animation_enemy_generator_bonepile;
	SpriteAnimation* sprite_animation_enemy_generator_grave;
	SpriteAnimation* sprite_animation_enemy_generator_hole;
	SpriteAnimation* sprite_animation_enemy_generator_rockmound_small;
	SpriteAnimation* sprite_animation_enemy_generator_rockmound;
	SpriteAnimation* sprite_animation_projectile_player_blade;
	SpriteAnimation* sprite_animation_projectile_player_shield;
	SpriteAnimation* sprite_animation_weapon_staff;
	SpriteAnimation* sprite_animation_weapon_axe;
	SpriteAnimation* sprite_animation_projectile_spear;
	SpriteAnimation* sprite_animation_projectile_club;
	SpriteAnimation* sprite_animation_weapon_bow;
	SpriteAnimation* sprite_animation_effect_stun;
	SpriteAnimation* sprite_animation_projectile_arrow;
	SpriteAnimation* sprite_animation_item_weapon;
	SpriteAnimation* sprite_animation_item_equipment;
	SpriteAnimation* sprite_animation_item_gold;
	SpriteAnimation* sprite_animation_item_health_potion;
	SpriteAnimation* sprite_animation_item_sparkle;
	SpriteAnimation* sprite_animation_ui_arrow_down;
	SpriteAnimation* sprite_animation_ui_arrow_up;

	// Map of enemy type and state to animation
	std::map<std::pair<EnemyType, EnemyMovementState>, SpriteAnimation*> sprite_animation_for_enemy_state;

	// UI Delays
	UIDelay* ui_delay_town_menu_select;
};

