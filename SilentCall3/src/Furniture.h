#include "Actor.h"
#include "../common.cpp"
#include "../SpriteAnimation.h"

#pragma once

/*! \file Furniture.h
* A class representing a piece of furniture in a dungeon map */
class Furniture: public Actor
{
public:
	Furniture();
	Furniture(double x, double y, int w, int h, SpriteAnimation* sa);
	~Furniture();

	// Can an entity on the map move through this?
	bool IsPassable();
	void SetIsPassable(bool passable);

	// Can this furniture be moved around the map?
	bool IsMovable();
	void SetIsMovable(bool movable);

	// Can this furniture be destroyed?
	bool IsDestructible();
	void SetIsDestructible(bool destructible);

	// Step forward sprite animation etc
	void Update();

private:
	bool passable = false;
	bool movable = false;
	bool destructible = false;
};

