#include "../common.cpp"
#include "../Enemy.h"
#include "BestiaryEntry.h"
#include "DropTable.h"

#pragma once
class Bestiary
{
public:
	Bestiary();
	~Bestiary();

	void FillDropTables();
	void FillEntries();
	Enemy* GenerateEnemy(EnemyGeneratorType generator, int max_level, double x, double y);

	DropTable* GetDropTableForBestiaryId(int id);

private:
	std::vector<BestiaryEntry*> entries;
	std::map<int, DropTable*> drop_tables;
	/*! \brief Generate an enemy from a bestiary entry
	*
	* @param entry     pointer to a BestiaryEntry
	* @param max_level maximum level. Returned enemy will be randomly between its base level and this max level.
	* @param x         x ordinate to set the enemy at
	* @param y         y ordinate to set the enemy at
	* @return pointer to an Enemy
	*/
	Enemy* GenerateEnemyFromBestiaryEntry(BestiaryEntry* entry, int max_level, double x, double y);
};