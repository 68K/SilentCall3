#include "Game.h"
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/log/core.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <fstream>
#include "items/PotionItem.h"

namespace logging = boost::log;

Game::Game()
{
}

Game::~Game()
{
	BOOST_LOG_TRIVIAL(info) << "Begin deleting Game";
	BOOST_LOG_TRIVIAL(debug) << "About to delete current_town_menu";
	delete(current_town_menu);
	if (current_dungeon_map != nullptr)
	{
		BOOST_LOG_TRIVIAL(debug) << "About to delete current_dungeon_map";
		delete(current_dungeon_map);
	}
	if (basic_128_texture != nullptr)
	{
		SDL_DestroyTexture(basic_128_texture);
	}
	if (static_sized_texture != nullptr)
	{
		SDL_DestroyTexture(static_sized_texture);
	}
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	BOOST_LOG_TRIVIAL(info) << "Finished deleting Game";
}

void Game::SetConfigPath(std::string config_path)
{
	this->config_file_path = config_path;
}

void Game::LoadConfig()
{
	std::string config_file = config_file_path + "\\" + config_filename;
	std::ifstream f (config_file.c_str(), std::ifstream::in);
	if (f.good())
	{
		BOOST_LOG_TRIVIAL(info) << "Loading config from file '" << config_file << "'";

		using boost::property_tree::ptree;
		ptree pt;
		try
		{
			read_ini(config_file, pt);
			// Try to read loglevel config
			boost::optional< ptree& > optional_loglevel = pt.get_child_optional("loglevel");
			if (optional_loglevel)
			{
				std::string loglevel = pt.get_child("loglevel").data();
				if (loglevel == "info")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::info
					);
				}
				else if (loglevel == "debug")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::debug
					);
				}
				else if (loglevel == "trace")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::trace
					);
				}
				else if (loglevel == "warning")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::warning
					);
				}
				else if (loglevel == "error")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::error
					);
				}
				else if (loglevel == "fatal")
				{
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::fatal
					);
				}
				else
				{
					BOOST_LOG_TRIVIAL(warning) << "Config file has unknown value for loglevel, '" << loglevel << "', defaulting to info";
					logging::core::get()->set_filter
					(
						logging::trivial::severity >= logging::trivial::info
					);
				}
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << "Config file does not specify loglevel, defaulting to info";
				logging::core::get()->set_filter
				(
					logging::trivial::severity >= logging::trivial::info
				);
			}
		}
		catch (std::exception const&  ex)
		{
			BOOST_LOG_TRIVIAL(error) << "Something went wrong loading config: " << ex.what();
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << "Config file '" << config_filename << "' not found. Using default config";
		logging::core::get()->set_filter
		(
		    logging::trivial::severity >= logging::trivial::info
		);
	}
}

void Game::LoadSaveFile(std::string save_filename)
{
	std::ifstream f(save_filename.c_str(), std::ifstream::in);
	if (f.good())
	{
		BOOST_LOG_TRIVIAL(info) << "Loading save from file '" << save_filename << "'";
		using boost::property_tree::ptree;
		ptree root;
		std::ifstream f(save_filename.c_str(), std::ifstream::in);
		boost::property_tree::read_json(f, root);
		try
		{
			questline_progress[village_questline] = root.get<int>("quests.progress.village_questline");
			questline_progress[merchant_questline] = root.get<int>("quests.progress.merchant_questline");
			questline_progress[banker_questline] = root.get<int>("quests.progress.banker_questline");
			player->SetGold(root.get<double>("player.current_gold"));
			player->AddXP(root.get<double>("player.xp"));
			boost::optional<ptree&> child = root.get_child_optional("player.equipped_armour");
			if (child)
			{
				std::shared_ptr<ArmourItem> loaded_armour = std::make_shared<ArmourItem>();
				loaded_armour->SetName(root.get<std::string>("player.equipped_armour.name"));
				loaded_armour->SetDescriptionId(root.get<int>("player.equipped_armour.description_id"));
				loaded_armour->SetArmourLevel(root.get<int>("player.equipped_armour.armour_level"));
				loaded_armour->SetValue(root.get<int>("player.equipped_armour.armour_value"));
				BOOST_LOG_TRIVIAL(debug) << "LoadSaveFile: Equipping saved armour " << loaded_armour->GetName();
				player->SetEquippedArmour(loaded_armour);
			}
			child = root.get_child_optional("player.equipped_primary_weapon");
			if (child)
			{
				std::shared_ptr<WeaponItem> loaded_primary_weapon = std::make_shared<WeaponItem>(string_to_weapon_type(root.get<std::string>("player.equipped_primary_weapon.weapon_type")));
				loaded_primary_weapon->SetName(root.get<std::string>("player.equipped_primary_weapon.name"));
				loaded_primary_weapon->SetDescriptionId(root.get<int>("player.equipped_primary_weapon.description_id"));
				loaded_primary_weapon->SetAttackLevel(root.get<int>("player.equipped_primary_weapon.attack_level"));
				loaded_primary_weapon->SetValue(root.get<int>("player.equipped_primary_weapon.value"));
				player->SetPrimaryEquippedWeapon(loaded_primary_weapon);
			}
			child = root.get_child_optional("player.equipped_secondary_weapon");
			if (child)
			{
				std::shared_ptr<WeaponItem> loaded_secondary_weapon = std::make_shared<WeaponItem>(string_to_weapon_type(root.get<std::string>("player.equipped_secondary_weapon.weapon_type")));
				loaded_secondary_weapon->SetName(root.get<std::string>("player.equipped_secondary_weapon.name"));
				loaded_secondary_weapon->SetDescriptionId(root.get<int>("player.equipped_secondary_weapon.description_id"));
				loaded_secondary_weapon->SetAttackLevel(root.get<int>("player.equipped_secondary_weapon.attack_level"));
				loaded_secondary_weapon->SetValue(root.get<int>("player.equipped_secondary_weapon.value"));
				player->SetSecondaryEquippedWeapon(loaded_secondary_weapon);
			}
			child = root.get_child_optional("player_carried_items");
			if (child)
			{
				BOOST_FOREACH(const ptree::value_type &v, root.get_child("player_carried_items"))
				{
					BOOST_LOG_TRIVIAL(debug) << "LoadSaveFile: Loading item " << v.second.get<std::string>("name");
					ItemType loaded_item_type = string_to_item_type(v.second.get<std::string>("type"));
					switch (loaded_item_type)
					{
					case item_type_accessory:
						BOOST_LOG_TRIVIAL(warning) << "Loading an accessory from save file but they aren't implemented yet!";
						break;
					case item_type_armour:
					{
						std::shared_ptr<ArmourItem> loaded_armour = std::make_shared<ArmourItem>();
						loaded_armour->SetName(v.second.get<std::string>("name"));
						loaded_armour->SetDescriptionId(v.second.get<int>("description_id"));
						loaded_armour->SetArmourLevel(v.second.get<int>("armour_value"));
						loaded_armour->SetValue(v.second.get<int>("value"));
						player->AddItem(loaded_armour);
						break;
					}
					case item_type_weapon:
					{
						WeaponType loaded_weapon_type = string_to_weapon_type(v.second.get<std::string>("weapon_type"));
						std::shared_ptr<WeaponItem> loaded_weapon = std::make_shared<WeaponItem>(loaded_weapon_type);
						loaded_weapon->SetName(v.second.get<std::string>("name"));
						loaded_weapon->SetAttackLevel(v.second.get<int>("weapon_attack_level"));
						loaded_weapon->SetDescriptionId(v.second.get<int>("description_id"));
						loaded_weapon->SetValue(v.second.get<int>("value"));
						player->AddItem(loaded_weapon);
						break;
					}
					case item_type_potion:
					{
						PotionType loaded_potion_type = string_to_potion_type(v.second.get<std::string>("potion_type"));
						std::shared_ptr<PotionItem> loaded_potion = std::make_shared<PotionItem>(loaded_potion_type);
						player->AddItem(loaded_potion);
						break;
					}
					case item_type_quest:
					{
						BOOST_LOG_TRIVIAL(warning) << "Loading a quest item from save file but they aren't implemented yet!";
						break;
					}
					default:
						BOOST_LOG_TRIVIAL(warning) << "Defaulted on type of item to load from save file: " << v.second.get<std::string>("type");
				    }
				}
			}
			bool save_file_merchant_in_village = root.get<bool>("village.is_merchant_in_village");
			if (save_file_merchant_in_village)
			{
				BOOST_LOG_TRIVIAL(debug) << "LoadSaveFile: Adding merchant building to village";
				AddMerchantToVillage();
			}
			this->merchant_level = root.get<int>("village.merchant_level");
			bool save_file_banker_in_village = root.get<bool>("village.is_banker_in_village");
			if (save_file_banker_in_village)
			{
				BOOST_LOG_TRIVIAL(debug) << "LoadSaveFile: Adding banker building to village";
				AddBankerToVillage();
			}
			this->bank_balance = root.get<int>("village.bank_balance");
		}
		catch (boost::property_tree::ptree_error e)
		{
			BOOST_LOG_TRIVIAL(error) << "Error when loading '" << save_filename << "': '" << e.what() << "'";
		}

		// Reset quest list
		FillQuestList();
		// Reset town menu
		SetTownMenuMainOptions();
	}
	else
	{
		BOOST_LOG_TRIVIAL(info) << "Unable to open save file '" << save_filename << "'";
	}
}

bool Game::WriteSaveFile(std::string save_filename)
{
	BOOST_LOG_TRIVIAL(info) << "Writing save file to " << save_filename;
	using boost::property_tree::ptree;
	ptree root;
	root.put("quests.progress.village_questline", questline_progress[village_questline]);
	root.put("quests.progress.merchant_questline", questline_progress[merchant_questline]);
	root.put("quests.progress.banker_questline", questline_progress[banker_questline]);
	root.put("player.current_gold", player->GetGold());
	root.put("player.xp", player->GetXP());
	if (player->GetEquippedArmour())
	{
		root.put("player.equipped_armour.name", player->GetEquippedArmour()->GetName());
		root.put("player.equipped_armour.description_id", player->GetEquippedArmour()->GetDescriptionId());
		root.put("player.equipped_armour.armour_level", player->GetEquippedArmour()->GetArmourLevel());
		root.put("player.equipped_armour.armour_value", player->GetEquippedArmour()->GetValue());
	}
	if (player->GetPrimaryEquippedWeapon())
	{
		root.put("player.equipped_primary_weapon.name", player->GetPrimaryEquippedWeapon()->GetName());
		root.put("player.equipped_primary_weapon.description_id", player->GetPrimaryEquippedWeapon()->GetDescriptionId());
		root.put("player.equipped_primary_weapon.attack_level", player->GetPrimaryEquippedWeapon()->GetAttackLevel());
		root.put("player.equipped_primary_weapon.value", player->GetPrimaryEquippedWeapon()->GetValue());
		root.put("player.equipped_primary_weapon.weapon_type", player->GetPrimaryEquippedWeapon()->GetWeaponType());
	}
	if (player->GetSecondaryEquippedWeapon())
	{
		root.put("player.equipped_secondary_weapon.name", player->GetSecondaryEquippedWeapon()->GetName());
		root.put("player.equipped_secondary_weapon.description_id", player->GetSecondaryEquippedWeapon()->GetDescriptionId());
		root.put("player.equipped_secondary_weapon.attack_level", player->GetSecondaryEquippedWeapon()->GetAttackLevel());
		root.put("player.equipped_secondary_weapon.value", player->GetSecondaryEquippedWeapon()->GetValue());
		root.put("player.equipped_secondary_weapon.weapon_type", player->GetSecondaryEquippedWeapon()->GetWeaponType());
	}
	if (player->GetItems().size() > 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "Saving " << player->GetItems().size() << " carried items.";
		ptree items;
		std::vector<std::shared_ptr<Item>> player_items = player->GetItems();
		for (std::vector<std::shared_ptr<Item>>::iterator it = player_items.begin(); it < player_items.end(); it++)
		{
			BOOST_LOG_TRIVIAL(debug) << "Saving item to file: " << (*it)->GetName();
			ptree item;
			item.put("name", (*it)->GetName());
			item.put("type", (*it)->GetItemType());
			item.put("description_id", (*it)->GetDescriptionId());
			item.put("value", (*it)->GetValue());
			ArmourItem* armour = nullptr;
			PotionItem* potion = nullptr;
			WeaponItem* weapon = nullptr;
			switch ((*it)->GetItemType())
			{
			case item_type_armour:
				BOOST_LOG_TRIVIAL(debug) << "...it's armour!";
				armour = static_cast<ArmourItem*>((*it).get());
				item.put("armour_value", armour->GetArmourLevel());
				break;
			case item_type_potion:
				BOOST_LOG_TRIVIAL(debug) << "...it's a potion!";
				potion = static_cast<PotionItem*>((*it).get());
				item.put("potion_type", potion->GetPotionType());
				break;
			case item_type_quest:
				item.put("quest_item", (*it)->GetQuestItemId());
				break;
			case item_type_weapon:
				BOOST_LOG_TRIVIAL(debug) << "It's a weapon!";
				weapon = static_cast<WeaponItem*>((*it).get());
				item.put("weapon_type", weapon->GetWeaponType());
				item.put("weapon_attack_level", weapon->GetAttackLevel());
				break;
		    }
			items.push_back(std::make_pair("", item));
		}
		root.add_child("player_carried_items", items);
	}
	root.put("village.is_merchant_in_village", this->is_merchant_in_village);
	root.put("village.merchant_level", this->merchant_level);
	root.put("village.is_banker_in_village", this->is_banker_in_village);
	root.put("village.bank_balance", this->bank_balance);

	std::ofstream f(save_filename.c_str(), std::ifstream::out);
	boost::property_tree::write_json(f, root, false);
	f.close();
	return true;
}

void Game::SetSavePath(std::string save_path)
{
	save_file_path = save_path;
	boost::filesystem::path dir(save_file_path);
	if (boost::filesystem::create_directory(dir))
	{
		BOOST_LOG_TRIVIAL(info) << "Created save file directory '" << save_file_path << "'";
	}
}

std::string Game::GetSavePath()
{
	return save_file_path;
}

bool Game::Quit()
{
	return quit;
}

void Game::SetTownMenuMainOptions()
{
	BOOST_LOG_TRIVIAL(debug) << "Updating town options.";
	if (current_town_menu)
	{
		delete(current_town_menu);
	}
	current_town_menu = new TownMenu("Welcome", CreateTextureForString(ren, *ttffont_ui_36, "Welcome", SDL_Color{ 255, 255, 0, 255 }, render_width));
	current_town_menu->AddOption(TownMenuOption(town_ventureforth, "Venture Forth", CreateTextureForString(ren, *ttffont_ui_36, "Venture Forth", SDL_Color{ 0, 0, 0, 255 }, render_width), false));
	current_town_menu->AddOption(TownMenuOption(town_quests, "Quests", CreateTextureForString(ren, *ttffont_ui_36, "Quests", SDL_Color{ 0, 0, 0, 255 }, render_width), true));
	if (is_merchant_in_village)
	{
		BOOST_LOG_TRIVIAL(debug) << "Adding merchant option to town menu";
		current_town_menu->AddOption(TownMenuOption(town_merchant, "Merchant", CreateTextureForString(ren, *ttffont_ui_36, "Merchant", SDL_Color{ 0, 0, 0, 255 }, render_width), true));
	}
	else
	{
		BOOST_LOG_TRIVIAL(debug) << "Not adding merchant option to town menu";
	}
	if (is_banker_in_village)
	{
		BOOST_LOG_TRIVIAL(debug) << "Adding banker option to town menu";
		current_town_menu->AddOption(TownMenuOption(town_banker, "Banker", CreateTextureForString(ren, *ttffont_ui_36, "Banker", SDL_Color{ 0, 0, 0, 255 }, render_width), true));
	}
	else
	{
		BOOST_LOG_TRIVIAL(debug) << "Not adding banker option to town menu";
	}
	current_town_menu->AddOption(TownMenuOption(town_system, "System", CreateTextureForString(ren, *ttffont_ui_36, "System", SDL_Color{ 0, 0, 0, 255 }, render_width), true));
}

Quest * Game::GenerateQuest()
{
	QuestType random_quest_type = static_cast<QuestType>(rand() % QuestType::MAX);
	switch (random_quest_type)
	{
	case(QuestType::assassinate) :
	{
		return new Quest("Assassinate a thing", "It needs to die", forest1, player->GetLevel(), defeat_an_enemy, 0, 0, quest_item_none, forest1, nullptr,
						 no_questline, -1);
	}
	case(QuestType::explore) :
	{
		return new Quest("Scout somewhere", "Find your way through the forest", forest1, player->GetLevel(), reach_a_location, 0, 0, quest_item_none, forest1, nullptr,
						 no_questline, -1);
	}
	case(QuestType::find) :
	{
		return new Quest("Find a thing", "A thing was lost. There's a sizable reward if you can find it!", forest1, player->GetLevel(), find_an_item, 0, 0, quest_item_none, forest1, nullptr,
						 no_questline, -1);
	}
	case(QuestType::hunt) :
	{
		return new Quest("Kill everything", "Currently the greatest threat to the village is N things. Go and remove them as a threat.", forest1, player->GetLevel(), defeat_all_enemies, 0, 0, quest_item_none, forest1, nullptr,
						 no_questline, -1);
	}
	default:
	{
		BOOST_LOG_TRIVIAL(error) << "Defaulted when randomly picking a quest type!";
		return nullptr;
	}
	}
}

Quest* Game::QuestFromQuestSpec(QuestSpec& spec)
{
	return new Quest(spec.GetTitle(), spec.GetDescription(), spec.GetMapType(), spec.GetBaseLevel(), spec.GetCompletionCriteriaType(),
		spec.GetCompletionTargetX(), spec.GetCompletionTargetY(), spec.GetQuestItemId(), spec.GetCompletionTargetMap(), nullptr,
		spec.GetQuestLineType(), spec.GetId());
}

void Game::UpdateQuestLines()
{
	if (current_quest == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Attempt to update quest lines when current quest is null!";
	}
	else {
		if (current_quest->GetQuestLineType() == no_questline)
		{
			BOOST_LOG_TRIVIAL(debug) << "Attempt to update quest lines when current quest does not belong to a quest line.";
		}
		else
		{
			BOOST_LOG_TRIVIAL(debug) << "Setting quest line's progress to the max of " << questline_progress[current_quest->GetQuestLineType()] << "," << current_quest->GetQuestLineId() + 1;
			questline_progress[current_quest->GetQuestLineType()] = std::max(questline_progress[current_quest->GetQuestLineType()], current_quest->GetQuestLineId() + 1);
		}
	}
	
}

void Game::FillQuestList()
{
	BOOST_LOG_TRIVIAL(debug) << "Filling quest list...";
	available_quests_in_village.clear();
	// First add quest line quests
	// TODO: limit if too many quest line quests are available
	for (std::map<QuestLineType, int>::iterator it = questline_progress.begin(); it != questline_progress.end(); it++)
	{
		BOOST_LOG_TRIVIAL(debug) << "Checking questline progress for questline " << it->first << "...";
		QuestSpec* available_quest = quest_lines[it->first]->GetNextAvailableQuest(questline_progress);
		if (available_quest)
		{
			BOOST_LOG_TRIVIAL(debug) << "...new quest available, adding to quest list.";
			available_quests_in_village.push_back(QuestFromQuestSpec(*available_quest));
		} 
		else
		{
			BOOST_LOG_TRIVIAL(debug) << "...no next quest available from this questline.";
		}
	}

	//Then fill remainder with random quests
	if ((int)available_quests_in_village.size() < kMaxNumberOfQuestsInVillage)
	{
		for (int i = available_quests_in_village.size(); i < kMaxNumberOfQuestsInVillage; i++)
		{
			available_quests_in_village.push_back(GenerateQuest());
		}
		for (std::vector<Quest*>::iterator it = available_quests_in_village.begin(); it < available_quests_in_village.end(); it++)
		{
			std::string title = (*it)->GetTitle();
			BOOST_LOG_TRIVIAL(debug) << "Quest added to quest list: " << title;
		}
	}
}

void Game::PrepareQuest(Quest& quest)
{
	current_dungeon_map = new DungeonMap();
	current_dungeon_map->LoadTmxMap(quest.GetMapType(), quest);
	current_dungeon_map->LoadTextures(ren);

	PrepareDungeonMap();

	current_quest->SetState(quest_in_progress);

	quest_prepared = true;
}

void Game::TurnInQuest()
{
	if (current_quest == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Attempt to turn in quest when current_quest is null";
	}
	else
	{
		if (current_quest->GetState() != quest_complete)
		{
			BOOST_LOG_TRIVIAL(error) << "Attempt to turn in quest when current_quest is not completed (actual state is '" << current_quest->GetState() << "'";
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << "Turning in quest '" << current_quest->GetTitle() << "'";
			delete(current_dungeon_map);
			current_dungeon_map = nullptr;
			UpdateQuestLines();
			FillQuestList();
			
			// Specific updates
			// Add merchant to village
			if (current_quest->GetQuestLineType() == merchant_questline && current_quest->GetQuestLineId() == 2)
			{
				AddMerchantToVillage();
			}
			// Add banker to village
			if (current_quest->GetQuestLineType() == banker_questline && current_quest->GetQuestLineId() == 1)
			{
				AddBankerToVillage();
			}

			delete(current_quest);
			current_quest = nullptr;
			quest_prepared = false;

			// Update bank balance
			bank_balance = bank_balance * bank_interest_rate;
			bank_balance = std::max(0, bank_balance - bank_service_charge);

			WriteSaveFile(this->GetSavePath() + "\\SaveGame.dat");

			// Reset town menu
			SetTownMenuMainOptions();
		}
	}
}

std::shared_ptr<DroppedItem> Game::QuestItemFromQuestItemId(QuestItem quest_item_id, double x, double y, SpriteAnimation* sprite_animation)
{
	std::shared_ptr<DroppedItem> result = std::make_shared<DroppedItem>(drop_item_quest, 0, x, y, 32, 32, sprite_animation);
	std::string name = "NO NAME";
	int description_id = 1;
	switch (quest_item_id)
	{
	case quest_item_none:
		BOOST_LOG_TRIVIAL(error) << "Attempt to create a quest item when the quest item ID is quest_item_none";
		return nullptr;
	case quest_item_merchant_guild_amulet:
		name = "Merchant Guild Amulet";
		description_id = 3;
		break;
	case quest_item_banker_clue:
		name = "Bloody scrap";
		description_id = 4;
		break;
	default:
		BOOST_LOG_TRIVIAL(error) << "Defaulted on quest item ID when creating a quest item";
		return nullptr;
	}
	std::shared_ptr<Item> item = std::make_shared<Item>(item_type_quest);
	item->SetQuestItemId(quest_item_id);
	item->SetName(name);
	item->SetDescriptionId(description_id);
	result->SetItem(item);

	return result;
}

void Game::TransitionToMap(DungeonMapType map)
{
	BOOST_LOG_TRIVIAL(debug) << "Transitioning to map type " << map;
	if (!current_quest)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to transition map; current_quest is null!";
		return;
	}
	DungeonMap* next_dungeon_map = new DungeonMap();
	next_dungeon_map->LoadTmxMap(map, *current_quest);
	next_dungeon_map->LoadTextures(ren);
	DungeonMap* last_dungeon_map = current_dungeon_map;
	current_dungeon_map = next_dungeon_map;

	PrepareDungeonMap();

	BOOST_LOG_TRIVIAL(debug) << "New map ready; deleting old map";
	projectiles.clear();
	delete(last_dungeon_map);
}

void Game::TransitionPlayingToTitle()
{
	BOOST_LOG_TRIVIAL(info) << "Game is returning to the title screen";
	current_game_state = gamestate_title_screen;
	if (current_dungeon_map)
	{
		BOOST_LOG_TRIVIAL(debug) << "Game is about to delete the dungeon map";
		delete(current_dungeon_map);
		current_dungeon_map = nullptr;
	}
	if (village_map)
	{
		BOOST_LOG_TRIVIAL(debug) << "Game is about to delete the village map";
		delete(village_map);
		village_map = nullptr;
	}
	BOOST_LOG_TRIVIAL(debug) << "Game is about to clear the quest list";
	available_quests_in_village.clear();

	BOOST_LOG_TRIVIAL(debug) << "Game is about to delete the open dialog list";
	open_dialogs.clear();

	current_town_menu_selection = 0;

	bool is_merchant_in_village = false;
	int merchant_level = 1;
	merchant_items.clear();

	if (current_quest)
	{
		BOOST_LOG_TRIVIAL(debug) << "Game is about to delete the currently active quest";
		delete(current_quest);
		current_quest = nullptr;
	}
	quest_prepared = false;

	BOOST_LOG_TRIVIAL(debug) << "Game is about to delete any projectiles";
	projectiles.clear();
}

bool Game::AnyDialogOpen()
{
	return !open_dialogs.empty();
}

void Game::KillEnemy(Enemy* enemy)
{
	BOOST_LOG_TRIVIAL(debug) << "Killed an enemy!";
	player->AddXP(ScaleXP((enemy)->GetXP(), (enemy)->GetLevel()));
	DropTable* drop_table = bestiary->GetDropTableForBestiaryId(enemy->GetBestiaryId());
	double drop_chance = RandomDoubleZeroToOne();
	DropItemType drop_type = drop_table->GetDropType(drop_chance);
	std::shared_ptr<DroppedItem> drop = nullptr;
	switch (drop_type)
	{
		case(drop_item_nothing) :
			BOOST_LOG_TRIVIAL(debug) << "Nothing dropped... (drop chance " << drop_chance << ")";
			break;
		case(drop_item_armour_random) :
		{
			BOOST_LOG_TRIVIAL(debug) << "Dropping random armour";
			int armour_level = player->GetLevel();
			if (current_quest)
			{
				armour_level = current_quest->GetBaseLevel();
			}
			std::shared_ptr<ArmourItem> armour = item_generator->GenerateRandomArmour(armour_level);
			drop = std::make_shared<DroppedItem>(drop_item_armour_random, armour_level, enemy->GetX(), enemy->GetY(), 32, 32, sprite_animation_item_equipment);
			drop->SetItem((std::shared_ptr<Item>)armour);
			current_dungeon_map->AddDroppedItem(drop);
			break;
		}
		case(drop_item_weapon_random) :
		{
			int weapon_level = player->GetLevel();
			if (current_quest)
			{
				weapon_level = current_quest->GetBaseLevel();
			}
			std::shared_ptr<WeaponItem> weapon = item_generator->GenerateRandomWeapon(weapon_level);
			drop = std::make_shared<DroppedItem>(drop_item_weapon_random, weapon_level, enemy->GetX(), enemy->GetY(), 32, 32, sprite_animation_item_weapon);
			drop->SetItem((std::shared_ptr<Item>)weapon);
			current_dungeon_map->AddDroppedItem(drop);
			break;
		}
		case(drop_item_gold) :
			BOOST_LOG_TRIVIAL(debug) << "Dropping " << enemy->GetGold() << " gold.";
			drop = std::make_shared<DroppedItem>(drop_item_gold, enemy->GetGold(), enemy->GetX(), enemy->GetY(), 32, 32, sprite_animation_item_gold);
			drop->SetSpriteAnimation(sprite_animation_item_gold); // TODO: Why?
			current_dungeon_map->AddDroppedItem(drop);
			break;
		case(drop_item_potion) :
			BOOST_LOG_TRIVIAL(debug) << "Dropping a potion";
			drop = std::make_shared<DroppedItem>(drop_item_potion, 1, enemy->GetX(), enemy->GetY(), 32, 32, sprite_animation_item_health_potion);
			drop->SetItem(std::make_shared<PotionItem>(potion_type_healing));
			current_dungeon_map->AddDroppedItem(drop);
			break;
		default:
			BOOST_LOG_TRIVIAL(error) << "Defaulted when trying to randomly pick a type of dropped item! Unhandled drop item type: " << drop_type;
			break;
	}
}

double Game::ScaleXP(double enemy_xp, int enemy_level)
{
	int level_diff = enemy_level - player->GetLevel();
	return std::max(1.0,floor(enemy_xp * (pow(1.1, level_diff))));
}

void Game::DestroyClutterItem(std::shared_ptr<ClutterItem> clutter_item)
{
	clutter_item->SetHealth(0);
	double drop_chance = RandomDoubleZeroToOne();
	DropItemType drop_type = clutter_drop_table->GetDropType(drop_chance);
	std::shared_ptr<DroppedItem> drop = nullptr;
	switch (drop_type)
	{
	case(drop_item_nothing) :
		BOOST_LOG_TRIVIAL(trace) << "Nothing dropped when smashing clutter... (drop chance " << drop_chance << ")";
		break;
	case(drop_item_armour_random) :
	{
		BOOST_LOG_TRIVIAL(debug) << "Dropping random armour";
		int armour_level = player->GetLevel();
		if (current_quest)
		{
			armour_level = current_quest->GetBaseLevel();
		}
		std::shared_ptr<ArmourItem> armour = item_generator->GenerateRandomArmour(armour_level);
		drop = std::make_shared<DroppedItem>(drop_item_armour_random, armour_level, clutter_item->GetX(), clutter_item->GetY(), 32, 32, sprite_animation_item_equipment);
		drop->SetItem((std::shared_ptr<Item>)armour);
		current_dungeon_map->AddDroppedItem(drop);
		break;
	}
	case(drop_item_weapon_random) :
	{
		int weapon_level = player->GetLevel();
		if (current_quest)
		{
			weapon_level = current_quest->GetBaseLevel();
		}
		std::shared_ptr<WeaponItem> weapon = item_generator->GenerateRandomWeapon(weapon_level);
		drop = std::make_shared<DroppedItem>(drop_item_weapon_random, weapon_level, clutter_item->GetX(), clutter_item->GetY(), 32, 32, sprite_animation_item_weapon);
		drop->SetItem((std::shared_ptr<Item>)weapon);
		current_dungeon_map->AddDroppedItem(drop);
		break;
	}
	case(drop_item_gold) :
	{
		int gold_amount = 5;
		if (current_quest)
		{
			gold_amount = 1 + (rand() % current_quest->GetBaseLevel());
		}
		BOOST_LOG_TRIVIAL(debug) << "Dropping " << gold_amount << " gold.";
		drop = std::make_shared<DroppedItem>(drop_item_gold, gold_amount, clutter_item->GetX(), clutter_item->GetY(), 32, 32, sprite_animation_item_gold);
		drop->SetSpriteAnimation(sprite_animation_item_gold); // TODO: Why?
		current_dungeon_map->AddDroppedItem(drop);
		break;
	}
	case(drop_item_potion) :
		BOOST_LOG_TRIVIAL(debug) << "Dropping a potion";
		drop = std::make_shared<DroppedItem>(drop_item_potion, 1, clutter_item->GetX(), clutter_item->GetY(), 32, 32, sprite_animation_item_health_potion);
		drop->SetItem(std::make_shared<PotionItem>(potion_type_healing));
		current_dungeon_map->AddDroppedItem(drop);
		break;
	default:
		BOOST_LOG_TRIVIAL(error) << "Defaulted when trying to randomly pick a type of dropped item! Unhandled drop item type: " << drop_type;
		break;
	}
}

void Game::PlayerDirectAttack(int damage)
{
	// TODO: Axe should be roughly where the axe is actually swinging
	int collision_centre_x = player->GetX();
	int collision_centre_y = player->GetY();
	int collision_width = 64;
	int collision_height = 64;
	std::vector<Enemy*> enemies = current_dungeon_map->GetEnemies();
	for (std::vector<Enemy*>::iterator enemy = enemies.begin(); enemy < enemies.end(); enemy++)
	{
		if ((*enemy)->IsAlive() && !(*enemy)->IsInInvincibilityFrames())
		{
			if (CheckCollisionFromCentre(collision_centre_x, collision_centre_y, collision_width, collision_height,
				(*enemy)->GetX() - ((*enemy)->GetWidth() / 2), (*enemy)->GetY() - ((*enemy)->GetHeight() / 2), (*enemy)->GetWidth(), (*enemy)->GetHeight()))
			{
				if ((*enemy)->ApplyDamage(damage))
				{
					KillEnemy(*enemy);
				}
			}
		}
	}

	std::vector<std::shared_ptr<ClutterItem>> clutter = current_dungeon_map->GetClutter();
	for (std::vector<std::shared_ptr<ClutterItem>>::iterator clutter_item = clutter.begin(); clutter_item < clutter.end(); clutter_item++)
	{
		if ((*clutter_item)->IsAlive())
		{
			if (CheckCollisionFromCentre(collision_centre_x, collision_centre_y, collision_width, collision_height,
				(*clutter_item)->GetX() - ((*clutter_item)->GetWidth() / 2), (*clutter_item)->GetY() - ((*clutter_item)->GetHeight() / 2), (*clutter_item)->GetWidth(), (*clutter_item)->GetHeight()))
			{
				DestroyClutterItem(*clutter_item);
			}
		}
	}

	auto generators = current_dungeon_map->GetEnemyGenerators();
	for (std::vector<EnemyGenerator*>::iterator generator = generators.begin(); generator < generators.end(); generator++)
	{
		if ((*generator)->IsAlive() && !(*generator)->IsInInvincibilityFrames())
		{
			if (CheckCollisionFromCentre(collision_centre_x, collision_centre_y, collision_width, collision_height,
				(*generator)->GetX(), (*generator)->GetY(), (*generator)->GetWidth(), (*generator)->GetHeight()))
			{
				(*generator)->ApplyDamage(damage);
			}
		}
	}
}

void Game::StartPlayerAttack(WeaponItem weapon, bool is_primary_attack)
{
	if (weapon.GetWeaponType() == weapon_type_shield)
	{
		BOOST_LOG_TRIVIAL(warning) << "Attempt to attack using shield - this should not happen.";
		return;
	}
	SpriteAnimation* attack_animation = nullptr;
	switch (weapon.GetWeaponType())
	{
	case weapon_type_axe:
		attack_animation = sprite_animation_weapon_axe;
		break;
	case weapon_type_bow:
		attack_animation = sprite_animation_weapon_bow;
		break;
	case weapon_type_staff:
		attack_animation = sprite_animation_weapon_staff;
		break;
	}
	if (is_primary_attack)
	{
		player->PrimaryAttack(attack_animation);
	}
	else
	{
		player->SecondaryAttack(attack_animation);
	}
	if (weapon.FiresProjectiles())
	{
		Projectile* new_projectile = new Projectile(player->GetX(), player->GetY(), 8, 24, sprite_animation_projectile_player_blade, true);
		double distance_from_player = 12;

		switch (weapon.GetWeaponType())
		{
		case weapon_type_bow:
			new_projectile->SetSpriteAnimation(sprite_animation_projectile_arrow);
			new_projectile->SetLifeTime(100);
			new_projectile->SetSpeed(10);
			break;
		case weapon_type_club:
			new_projectile->SetSpriteAnimation(sprite_animation_projectile_club);
			new_projectile->SetLifeTime(10);
			break;
		case weapon_type_spear:
			new_projectile->SetSpriteAnimation(sprite_animation_projectile_spear);
			new_projectile->SetLifeTime(20);
			new_projectile->SetWidth(64);
			new_projectile->SetHeight(64);

			break;
		case weapon_type_sword:
			new_projectile->SetSpriteAnimation(sprite_animation_projectile_player_blade);
			distance_from_player = 24;
			break;
		}
		// Spear fires in 4 directions; other weapons in 8
		new_projectile->SetIfFacingRight(player->GetIfFacingRight());
		new_projectile->SetAngle(player->GetPointingAngle());
		new_projectile->SetX(new_projectile->GetX() + distance_from_player * cos(new_projectile->GetAngle()));
		new_projectile->SetY(new_projectile->GetY() - distance_from_player * sin(new_projectile->GetAngle()));

		if (is_primary_attack)
		{
			new_projectile->SetDamage(player->GetPrimaryAttackDamage());
		}
		else
		{
			new_projectile->SetDamage(player->GetSecondaryAttackDamage());
		}
		projectiles.push_back((std::unique_ptr<Projectile>)new_projectile);
	}
}

void Game::Initialise()
{

	// Initialise bestiary
	bestiary = new Bestiary();
	bestiary->FillEntries();
	bestiary->FillDropTables();

	item_generator = new ItemGenerator();
	item_description_index.insert(std::pair<int,std::string>(0, "NO ITEM DESCRIPTION"));
	item_description_index.insert(std::pair<int, std::string>(1, "I AM ERROR"));
	item_description_index.insert(std::pair<int, std::string>(2, "Recover some health"));
	item_description_index.insert(std::pair<int, std::string>(3, "Permit to trade goods"));
	item_description_index.insert(std::pair<int, std::string>(4, "Seems like expensive silk. Soaked in blood..."));
	item_description_index.insert(std::pair<int, std::string>(5, "A trusty sword"));
	item_description_index.insert(std::pair<int, std::string>(6, "A fine bow"));
	item_description_index.insert(std::pair<int, std::string>(7, "A sturdy club"));
	item_description_index.insert(std::pair<int, std::string>(8, "A sharpened axe"));
	item_description_index.insert(std::pair<int, std::string>(9, "A solid staff"));
	item_description_index.insert(std::pair<int, std::string>(10, "A sturdy shield"));
	item_description_index.insert(std::pair<int, std::string>(11, "A lengthy spear"));
	item_description_index.insert(std::pair<int, std::string>(12, "Standard armour"));

	clutter_drop_table = std::make_unique<DropTable>();
	clutter_drop_table->AddDropChance(0.80, drop_item_nothing);
	clutter_drop_table->AddDropChance(0.15, drop_item_gold);
	clutter_drop_table->AddDropChance(0.05, drop_item_potion);

	// Menu Options
	SetTownMenuMainOptions();

	// UI inputs
	ui_delay_town_menu_select = new UIDelay(20);
	ui_delays.push_back(ui_delay_town_menu_select);

	// UI Icons
	ui_icon_health_potion = new UIIcon();
	ui_icon_health_potion->SetSpriteAnimation(sprite_animation_item_health_potion);
	ui_icon_health_potion->SetX(20);
	ui_icon_health_potion->SetY(40);
	ui_icon_health_potion->SetWidth(16);
	ui_icon_health_potion->SetHeight(16);

	ui_icon_down_arrow = new UIIcon();
	ui_icon_down_arrow->SetSpriteAnimation(sprite_animation_ui_arrow_down);
	ui_icon_down_arrow->SetWidth(32);
	ui_icon_down_arrow->SetHeight(32);

	ui_icon_up_arrow = new UIIcon();
	ui_icon_up_arrow->SetSpriteAnimation(sprite_animation_ui_arrow_up);
	ui_icon_up_arrow->SetWidth(32);
	ui_icon_up_arrow->SetHeight(32);

	ui_icon_effect_stun = new UIIcon();
	ui_icon_effect_stun->SetSpriteAnimation(sprite_animation_effect_stun);
	ui_icon_effect_stun->SetWidth(32);
	ui_icon_effect_stun->SetHeight(32);

	// Prepare quest lines
	quest_lines[village_questline] = new QuestLine(village_questline);
	quest_lines[merchant_questline] = new QuestLine(merchant_questline);
	quest_lines[banker_questline] = new QuestLine(banker_questline);
	quest_lines[village_questline]->AddQuestSpec(new QuestSpec(village_questline, 0, "Explore the Village Woods", new std::pair<QuestLineType, int>(village_questline, -1),
		"This area seems peaceful. You rub the warmth of the fire into your tired muscles\n"
		"and think on the events of the past days. It's an odd feeling to have everyone\n"
		"you've ever known die and yet, you carry on. But it makes your decision easy.\n"
		"Into those woods, into that darkness, from here you can only do right...",
		forest1, 1, reach_a_location, forest2, -1, -1, quest_item_none));
	quest_lines[merchant_questline]->AddQuestSpec(new QuestSpec(merchant_questline, 0, "Rescue the stranger in Ekispedya Glade", new std::pair<QuestLineType, int>(village_questline, 0),
		"Pushing forward into the woods, you eventually come to Ekispedya, an ancient glade of evergreens and igneous rock. "
		"As you pause to take in the natural beauty, your ears fill with everything around. A gentle dull tone of wind in leaves. "
		"Common birds. And something else. Cries for help!",
		forest2, 2, reach_a_location, forest2, 500, 500, quest_item_none));
	quest_lines[merchant_questline]->AddQuestSpec(new QuestSpec(merchant_questline, 1, "Find Churchyard", new std::pair<QuestLineType, int>(merchant_questline, 0),
		"Safe at the camp, you are surprised how soon the feeling of relief changes to one of anxiety. Fergal was in that glade for a reason - "
		"somewhere just south lay the resting place of his father. He returned to Gem after receiving a letter from his uncle... he was "
		"fleeing the war, and the life of a refugee lay ahead. The uncle left Fergal's birthright, a merchant guild amulet, at his "
		"father's grave so he would find it, even if the letter never arrived. Fergal asks you to scout ahead and find Churchyard",
		forest2, 3, reach_a_location, graveyard, -1, -1, quest_item_none));
	quest_lines[merchant_questline]->AddQuestSpec(new QuestSpec(merchant_questline, 2, "Death May Die", new std::pair<QuestLineType, int>(village_questline, 1),
		"Churchyard was crawling with the undead. Leading Fergal there would be his death. As you approach him you think how to let him down "
		"gently. But he's completely upbeat about the situation. Because you can get the amulet for him. You protest, but he swears that a "
		"merchant guild amulet is worth a king's ransom. That has to be an exaggeration... right?",
		graveyard, 4, find_an_item, graveyard, 2864, 2800, quest_item_merchant_guild_amulet));
	quest_lines[village_questline]->AddQuestSpec(new QuestSpec(village_questline, 1, "Seanadhmaid", new std::pair<QuestLineType, int>(merchant_questline, 0),
		"Standing at the edge of Ekispedya Glade, the land gently lowers then plateaus ahead. The trees down there have grown for millenia, "
		"and they form a perfectly unbroken canopy for miles around. If you want revenge, without walking straight into Gold Rick's armies, "
		"this is the way forward.",
		forest2, 4, reach_a_location, forest3, -1, -1, quest_item_none));
	quest_lines[village_questline]->AddQuestSpec(new QuestSpec(village_questline, 2, "The heart of the forest", new std::pair<QuestLineType, int>(village_questline, 1),
		"The forest you knew as a child as \"The Old Ones\" is vast. Press further east to pick up the trail of your attackers.",
		forest3, 7, find_an_item, forest3, 2624, 2464, quest_item_banker_clue));
	quest_lines[banker_questline]->AddQuestSpec(new QuestSpec(banker_questline, 0, "What happened here?", new std::pair<QuestLineType, int>(village_questline, 2),
		"You found a scrap of black cloth - smooth as velvet and jet black. So black there's no sign of the blood on it until you pick it up. "
		"Scattered on the ground beneath it are a few coins and a scrap of parchment with numbers scribed on it. The blood has not congealed. "
		"You still have time to find the victim of this attack.",
		forest3, 9, reach_a_location, caves, -1 ,-1, quest_item_none));
	quest_lines[banker_questline]->AddQuestSpec(new QuestSpec(banker_questline, 1, "Into Darkness", new std::pair<QuestLineType, int>(banker_questline, 0),
		"The trail from the site of the attack led you to a cave. A slight breeze flowing past you from inside the cave tells you that the "
		"cave system ahead is large with at least one other entrance. Caverns like this are certain to be filled with many "
		"vicious dark-dwelling creatures, and your quarry is bleeding. Are you sure you want to go in?",
		caves, 11, reach_a_location, caves, 10208, 4576, quest_item_none));

	questline_progress[village_questline] = 0;
	questline_progress[merchant_questline] = 0;
	questline_progress[banker_questline] = 0;

}

void Game::NewGame()
{
	// Initialise player
	if (player)
	{
		delete(player);
	}
	player = new Player(0, 0, sprite_animation_player_facing_right);
	player->SetEquippedArmour(item_generator->GenerateRandomArmour(player->GetLevel()));
	player->SetPrimaryEquippedWeapon(item_generator->GenerateRandomWeapon(player->GetLevel()));
	//player->SetPrimaryEquippedWeapon(item_generator->GenerateRandomWeapon(player->GetLevel(), weapon_type_bow));
	player->SetSecondaryEquippedWeapon(item_generator->GenerateRandomWeapon(player->GetLevel()));

	// Initialise village map
	village_map = new PresentationMap();
	village_map->LoadTmxMap(village_maptype);
	village_map->LoadTextures(ren);
	SDL_Rect camp_location = SDL_Rect{ kVillageMapWidth / 2 - 1, kVillageMapHeight / 2 - 1, 3, 3 };
	buildings.push_back(new Building(camp_location, sprite_animation_player_camp));

	// Initial list of quests
	FillQuestList();

	// Initial merchant items
	FillMerchantInventory();
}

void Game::InitialiseInput()
{
	// Initialise input devices
	player_input = new PlayerInput();
	player_input->InitialiseInputDevices();
}

bool Game::SetDisplay()
{
	// Determine display dimensions if playing in full screeen
	SDL_DisplayMode current;
	if (SDL_GetCurrentDisplayMode(0, &current) == 0)
	{
		display_width = current.w;
		display_height = current.h;
	}
	else
	{
		BOOST_LOG_TRIVIAL(fatal) << "SDL_GetCurrentDisplayMode Error: " << SDL_GetError();
		SDL_Quit();
		return 1;
	}

	if (fullscreen)
	{
		win = SDL_CreateWindow(window_title, 100, 100, display_width, display_height, SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN | SDL_WINDOW_BORDERLESS);
	}
	else
	{
		win = SDL_CreateWindow(window_title, (display_width - window_width) / 2, (display_height - window_height) / 2, window_width, window_height, SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS);
	}
	if (win == nullptr) {
		BOOST_LOG_TRIVIAL(fatal) << "SDL_CreateWindow Error: " << SDL_GetError();
		SDL_Quit();
		return 1;
	}

	// If fullscreen...
	if (fullscreen)
	{
		// ...hide mouse
		SDL_ShowCursor(SDL_DISABLE);

		// ...update the rect to display to
		double game_render_ratio = (double)render_width / render_height;
		double display_ratio = (double)display_width / display_height;
		if (display_ratio > game_render_ratio)
		{
			BOOST_LOG_TRIVIAL(debug) << "Display is wider ratio than rendering";
			int width = (int)((double)display_width * ((double)render_width / (double)display_width));
			display_rect.x = (display_width - width) / 2;
			display_rect.y = 0;
			display_rect.h = display_height;
			display_rect.w = width;
		}
		else
		{
			BOOST_LOG_TRIVIAL(debug) << "Display is narrower ratio than rendering";
			display_rect.x = 0;
			display_rect.w = display_width;
			display_rect.y = 0;
			display_rect.h = display_height;
		}
	}
	else
	{
		// If windowed...
		BOOST_LOG_TRIVIAL(debug) << "Windowed - display to entire window (" << window_width << "," << window_height << ")";
		display_rect.x = 0;
		display_rect.y = 0;
		display_rect.h = window_height;
		display_rect.w = window_width;
	}

	// Initialise the renderer
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		SDL_DestroyWindow(win);
		BOOST_LOG_TRIVIAL(fatal) << "SDL_CreateRenderer Error: " << SDL_GetError();
		SDL_Quit();
		return 1;
	}

	// Create a texture of set size to render to, then scale to user's resolution
	BOOST_LOG_TRIVIAL(debug) << "Fitting rendering to " << render_width << "," << render_height;
	static_sized_texture = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, render_width, render_height);

	// Initialise a basic texture for UI
	SDL_Surface *basic_surface;
	basic_surface = SDL_CreateRGBSurface(0, 128, 128, 32, 0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF);
	if (basic_surface == NULL) {
		BOOST_LOG_TRIVIAL(fatal) << "CreateRGBSurface failed: %s\n", SDL_GetError();
		return 1;
	}
	SDL_FillRect(basic_surface, NULL, SDL_MapRGB(basic_surface->format, 255, 255, 255));
	basic_128_texture = SDL_CreateTextureFromSurface(ren, basic_surface);
	SDL_FreeSurface(basic_surface);

	return true;
}

bool Game::LoadAssets()
{
	BOOST_LOG_TRIVIAL(debug) << "About to load ttf\\AnglicanText.ttf";
	ttffont_anglican_60 = TTF_OpenFont("ttf\\AnglicanText.ttf", 60);
	if (ttffont_anglican_60 == NULL)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to load ttf\\AnglicanText.ttf";
		SDL_Quit();
		return false;
	}

	BOOST_LOG_TRIVIAL(debug) << "About to load ttf\\Valiant.ttf";
	ttffont_ui_36 = TTF_OpenFont("ttf\\Valiant.ttf", 36);
	if (ttffont_ui_36 == NULL)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to load ttf\\Valiant.ttf";
		SDL_Quit();
		return false;
	}

	BOOST_LOG_TRIVIAL(debug) << "About to load ttf\\Clearly Gothic Bold.ttf";
	ttffont_ui_20 = TTF_OpenFont("ttf\\Clearly Gothic Bold.ttf", 20);
	if (ttffont_ui_20 == NULL)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to load ttf\\Clearly Gothic Bold.ttf";
		SDL_Quit();
		return false;
	}

	// UI String textures
	string_texture_ui_titlescreen_title = CreateTextureForString(ren, *ttffont_anglican_60, "Silent Call 3", SDL_Color{ 255, 255, 0, 255 }, render_width);

	// Load images to textures

	// Player characters
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\characters.png";
	spritesheet_characters = LoadTextureFromPng(ren, kAssetDir + "\\characters.png");
	if (spritesheet_characters == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load characters.png!";
		return false;
	}

	// Dungeon furniture
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\furniture.png";
	spritesheet_furniture = LoadTextureFromPng(ren, kAssetDir + "\\furniture.png");
	if (spritesheet_furniture == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load furniture.png!";
		return false;
	}

	// Village Buildings
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\buildings.png";
	spritesheet_buildings = LoadTextureFromPng(ren, kAssetDir + "\\buildings.png");
	if (spritesheet_buildings == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load buildings.png!";
		return false;
	}

	// UI Elements
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\ui_elements.png";
	spritesheet_ui = LoadTextureFromPng(ren, kAssetDir + "\\ui_elements.png");
	if (spritesheet_buildings == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load ui_elements.png!";
		return false;
	}

	// Enemy generators
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\enemy_generators.png";
	spritesheet_enemy_generators = LoadTextureFromPng(ren, kAssetDir + "\\enemy_generators.png");
	if (spritesheet_enemy_generators == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load enemy_generators.png!";
		return false;
	}

	// Enemy generators
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\effects.png";
	spritesheet_effects = LoadTextureFromPng(ren, kAssetDir + "\\effects.png");
	if (spritesheet_effects == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load effects.png!";
		return false;
	}

	// Items
	BOOST_LOG_TRIVIAL(debug) << "About to load " << kAssetDir + "\\items.png";
	spritesheet_items = LoadTextureFromPng(ren, kAssetDir + "\\items.png");
	if (spritesheet_items == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load items.png!";
		return false;
	}

	circular_texture = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 64, 64);
	SDL_SetRenderTarget(ren, circular_texture);
	SDL_Rect circle_rect = SDL_Rect{ 0, 160, 64, 64 };
	SDL_RenderCopy(ren, spritesheet_ui, &circle_rect, nullptr);

	return true;
}

void Game::DefineSprites()
{
	if (sprite_animation_frames)
	{
		delete(sprite_animation_frames);
	}
	sprite_animation_frames = new SpriteAnimationFrames;
	sprite_animation_frames->Initialise();

	// Player characters
	sprite_animation_player_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->player_facing_right);
	sprite_animation_player_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->player_moving_right);

	// NPCs
	sprite_animation_npc_male1_waving = new SpriteAnimation(spritesheet_characters, 32, 32, 3, sprite_animation_frames->npc_male1_waving);
	sprite_animation_npc_male1_merchant = new SpriteAnimation(spritesheet_characters, 32, 32, 8, sprite_animation_frames->npc_male1_merchant);
	sprite_animation_npc_banker_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->npc_banker_facing_right);
	sprite_animation_npc_banker_injured = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->npc_banker_injured);

	// Enemies
	sprite_animation_enemy_goblin_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_goblin_facing_right);
	sprite_animation_enemy_goblin_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_goblin_moving_right);
	sprite_animation_enemy_goblin_alarmed_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 8, sprite_animation_frames->enemy_goblin_alarmed_facing_right);

	sprite_animation_enemy_crow_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 8, sprite_animation_frames->enemy_crow_facing_right);
	sprite_animation_enemy_crow_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_crow_facing_right);

	sprite_animation_enemy_spider_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 1, sprite_animation_frames->enemy_spider_facing_right);
	sprite_animation_enemy_spider_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 20, sprite_animation_frames->enemy_spider_moving_right);

	sprite_animation_enemy_shroom_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 50, sprite_animation_frames->enemy_shroom_facing_right);
	sprite_animation_enemy_shroom_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_shroom_moving_right);

	sprite_animation_enemy_madskeleton_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_madskeleton_facing_right);
	sprite_animation_enemy_madskeleton_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 30, sprite_animation_frames->enemy_madskeleton_moving_right);

	sprite_animation_enemy_badskeleton_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 16, sprite_animation_frames->enemy_badskeleton_facing_right);
	sprite_animation_enemy_badskeleton_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 30, sprite_animation_frames->enemy_badskeleton_moving_right);

	sprite_animation_enemy_zombie_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 60, sprite_animation_frames->enemy_zombie_facing_right);
	sprite_animation_enemy_zombie_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 60, sprite_animation_frames->enemy_zombie_moving_right);

	sprite_animation_enemy_slime_facing_right = new SpriteAnimation(spritesheet_characters, 32, 32, 40, sprite_animation_frames->enemy_slime_facing_right);
	sprite_animation_enemy_slime_moving_right = new SpriteAnimation(spritesheet_characters, 32, 32, 40, sprite_animation_frames->enemy_slime_moving_right);

	// Furniture
	sprite_animation_furniture_map_exit = new SpriteAnimation(spritesheet_furniture, 32, 32, 2, sprite_animation_frames->furniture_map_exit);

	// Clutter
	sprite_animation_clutter_barrel = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_barrel);
	sprite_animation_clutter_vase_big = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_big_vase);
	sprite_animation_clutter_vase_small = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_small_vase);
	sprite_animation_clutter_crate_small = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_small_crate);
	sprite_animation_clutter_crate_medium = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_medium_crate);
	sprite_animation_clutter_funerary_vase_small = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_small_funerary_vase);
	sprite_animation_clutter_funerary_vase_big = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_big_funerary_vase);
	sprite_animation_clutter_coffin = new SpriteAnimation(spritesheet_furniture, 32, 32, 1, sprite_animation_frames->clutter_coffin);

	// Buildings
	sprite_animation_player_camp = new SpriteAnimation(spritesheet_buildings, 96, 96, 8, sprite_animation_frames->building_camp);
	sprite_animation_building_merchant_hut = new SpriteAnimation(spritesheet_buildings, 64, 64, 20, sprite_animation_frames->building_merchant_hut);
	sprite_animation_building_banker_stall = new SpriteAnimation(spritesheet_buildings, 64, 64, 1, sprite_animation_frames->building_banker_stall);

	// Enemy generators
	sprite_animation_enemy_generator_yurt = new SpriteAnimation(spritesheet_enemy_generators, 64, 64, 4, sprite_animation_frames->enemy_generator_yurt);
	sprite_animation_enemy_generator_tree = new SpriteAnimation(spritesheet_enemy_generators, 64, 64, 1, sprite_animation_frames->enemy_generator_tree);
	sprite_animation_enemy_generator_stump = new SpriteAnimation(spritesheet_enemy_generators, 32, 32, 1, sprite_animation_frames->enemy_generator_stump);
	sprite_animation_enemy_generator_bonepile = new SpriteAnimation(spritesheet_enemy_generators, 32, 32, 1, sprite_animation_frames->enemy_generator_bonepile);
	sprite_animation_enemy_generator_grave = new SpriteAnimation(spritesheet_enemy_generators, 32, 32, 4, sprite_animation_frames->enemy_generator_grave);
	sprite_animation_enemy_generator_hole = new SpriteAnimation(spritesheet_enemy_generators, 32, 32, 1, sprite_animation_frames->enemy_generator_hole);
	sprite_animation_enemy_generator_rockmound_small = new SpriteAnimation(spritesheet_enemy_generators, 32, 32, 1, sprite_animation_frames->enemy_generator_rockmound_small);
	sprite_animation_enemy_generator_rockmound = new SpriteAnimation(spritesheet_enemy_generators, 64, 64, 1, sprite_animation_frames->enemy_generator_rockmound);

    // Projectiles
	sprite_animation_projectile_player_blade = new SpriteAnimation(spritesheet_effects, 32, 32, 1, sprite_animation_frames->projectile_player_blade);
	sprite_animation_projectile_player_shield = new SpriteAnimation(spritesheet_effects, 32, 32, 1, sprite_animation_frames->projectile_player_shield);
	sprite_animation_projectile_spear = new SpriteAnimation(spritesheet_effects, 64, 64, 2, sprite_animation_frames->projectile_spear);
	sprite_animation_projectile_club = new SpriteAnimation(spritesheet_effects, 32, 32, 2, sprite_animation_frames->projectile_club);
	sprite_animation_projectile_arrow = new SpriteAnimation(spritesheet_effects, 32, 32, 1, sprite_animation_frames->projectile_arrow);

	// Weapons
	sprite_animation_weapon_staff = new SpriteAnimation(spritesheet_effects, 64, 64, 2, sprite_animation_frames->weapon_staff);
	sprite_animation_weapon_axe = new SpriteAnimation(spritesheet_effects, 64, 64, 2, sprite_animation_frames->weapon_axe);
	sprite_animation_weapon_bow = new SpriteAnimation(spritesheet_effects, 32, 32, 2, sprite_animation_frames->weapon_bow);

	// Effects
	sprite_animation_effect_stun = new SpriteAnimation(spritesheet_effects, 32, 32, 5, sprite_animation_frames->effect_stun);

	// Items
	sprite_animation_item_weapon = new SpriteAnimation(spritesheet_items, 32, 32, 2, sprite_animation_frames->item_weapon);
	sprite_animation_item_equipment = new SpriteAnimation(spritesheet_items, 32, 32, 2, sprite_animation_frames->item_equipment);
	sprite_animation_item_gold = new SpriteAnimation(spritesheet_items, 32, 32, 4, sprite_animation_frames->item_gold);
	sprite_animation_item_health_potion = new SpriteAnimation(spritesheet_items, 32, 32, 4, sprite_animation_frames->item_health_potion);
	sprite_animation_item_sparkle = new SpriteAnimation(spritesheet_items, 32, 32, 10, sprite_animation_frames->item_sparkle);

	// UI elements
	sprite_animation_ui_arrow_down = new SpriteAnimation(spritesheet_ui, 32, 32, 1, sprite_animation_frames->ui_arrow_down);
	sprite_animation_ui_arrow_up = new SpriteAnimation(spritesheet_ui, 32, 32, 1, sprite_animation_frames->ui_arrow_up);

	sprite_animation_for_enemy_state[std::make_pair(goblin, idle)] = sprite_animation_enemy_goblin_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(goblin, wandering)] = sprite_animation_enemy_goblin_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(goblin, targeting)] = sprite_animation_enemy_goblin_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(goblin, alarmed)] = sprite_animation_enemy_goblin_alarmed_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(crow, idle)] = sprite_animation_enemy_crow_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(crow, wandering)] = sprite_animation_enemy_crow_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(crow, targeting)] = sprite_animation_enemy_crow_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(crow, alarmed)] = sprite_animation_enemy_crow_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(spider, idle)] = sprite_animation_enemy_spider_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(spider, wandering)] = sprite_animation_enemy_spider_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(spider, targeting)] = sprite_animation_enemy_spider_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(spider, alarmed)] = sprite_animation_enemy_spider_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(shroom, idle)] = sprite_animation_enemy_shroom_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(shroom, wandering)] = sprite_animation_enemy_shroom_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(shroom, targeting)] = sprite_animation_enemy_shroom_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(shroom, alarmed)] = sprite_animation_enemy_shroom_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(mad_skeleton, idle)] = sprite_animation_enemy_madskeleton_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(mad_skeleton, wandering)] = sprite_animation_enemy_madskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(mad_skeleton, targeting)] = sprite_animation_enemy_madskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(mad_skeleton, alarmed)] = sprite_animation_enemy_madskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(bad_skeleton, idle)] = sprite_animation_enemy_badskeleton_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(bad_skeleton, wandering)] = sprite_animation_enemy_badskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(bad_skeleton, targeting)] = sprite_animation_enemy_badskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(bad_skeleton, alarmed)] = sprite_animation_enemy_badskeleton_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(zombie, idle)] = sprite_animation_enemy_zombie_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(zombie, wandering)] = sprite_animation_enemy_zombie_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(zombie, targeting)] = sprite_animation_enemy_zombie_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(zombie, alarmed)] = sprite_animation_enemy_zombie_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(slime, idle)] = sprite_animation_enemy_slime_facing_right;
	sprite_animation_for_enemy_state[std::make_pair(slime, wandering)] = sprite_animation_enemy_slime_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(slime, targeting)] = sprite_animation_enemy_slime_moving_right;
	sprite_animation_for_enemy_state[std::make_pair(slime, alarmed)] = sprite_animation_enemy_slime_moving_right;
}

SpriteAnimation* Game::SpriteAnimationForEnemyState(EnemyType type, EnemyMovementState state)
{
	if (sprite_animation_for_enemy_state[std::make_pair(type, state)])
	{
		return sprite_animation_for_enemy_state[std::make_pair(type, state)];
	}
	else
	{
		BOOST_LOG_TRIVIAL(error) << "No sprite animation for enemy type '" << type << "' and state '" << state << "' state. Defaulting to idle.";
		if (sprite_animation_for_enemy_state[std::make_pair(type, idle)])
		{
			return sprite_animation_for_enemy_state[std::make_pair(type, idle)];
		}
		else
		{
			BOOST_LOG_TRIVIAL(error) << "No sprite animation for enemy type '" << type << "' and state '" << idle << "' state. Defaulting to goblin idle.";
			return sprite_animation_for_enemy_state[std::make_pair(goblin, idle)];
		}
	}
}

void Game::DrawPlayerWeapon(bool is_primary_weapon)
{
	SpriteAnimation* weapon_animation = nullptr;
	std::shared_ptr<WeaponItem> weapon = nullptr;
	int current_animation_frame = 0;
	if(is_primary_weapon)
	{ 
		weapon_animation = player->GetPrimaryWeaponAnimation();
		weapon = player->GetPrimaryEquippedWeapon();
		current_animation_frame = player->GetPrimaryCurrentWeaponAnimationFrame();
	}
	else
	{
		weapon_animation = player->GetSecondaryWeaponAnimation();
		weapon = player->GetSecondaryEquippedWeapon();
		current_animation_frame = player->GetSecondaryCurrentWeaponAnimationFrame();
	}
	if (weapon && weapon_animation)
	{
		SDL_Rect rect_player_weapon;
		// TODO: Always centred on player?
		SDL_Rect current_weapon_animation_frame = weapon_animation->GetAnimationFrame(current_animation_frame);
		rect_player_weapon.x = (render_width / 2) - (current_weapon_animation_frame.w / 2);
		rect_player_weapon.y = (render_height / 2) - (current_weapon_animation_frame.h / 2);
		rect_player_weapon.w = current_weapon_animation_frame.w;
		rect_player_weapon.h = current_weapon_animation_frame.h;
		double angle = 0;
		if (weapon->GetWeaponType() == weapon_type_axe)
		{
			// TODO: A better way to do this?
			angle = (double)360 * (double)((double)current_animation_frame / (double)weapon_animation->GetAnimationLength());
		}
		SDL_RendererFlip weapon_flip = SDL_FLIP_NONE;
		if (!player->GetIfFacingRight())
		{
			// TODO: Always flipped same as player?
			weapon_flip = SDL_FLIP_HORIZONTAL;
			if (weapon->GetWeaponType() == weapon_type_axe)
			{
				// TODO: A better way to do this?
				angle = (double)-360 * (double)((double)current_animation_frame / (double)weapon_animation->GetAnimationLength());
			}
		}

		SDL_RenderCopyEx(ren, weapon_animation->GetSpriteSheet(),
			&weapon_animation->GetAnimationFrame(current_animation_frame), &rect_player_weapon,
			angle, NULL, weapon_flip);
	}
}

void Game::DrawPlayerShield(bool is_primary_shield)
{
	//BOOST_LOG_TRIVIAL(debug) << "Rendering shield";
	double angle = 0;
	double distance = player->GetShieldDistanceFromPlayer();
	if (is_primary_shield)
	{
		angle = player->GetPrimaryDefendingAngle();
	}
	else {
		angle = player->GetSecondaryDefendingAngle();
	}
	SDL_Rect shield_render_rect = { (render_width / 2) - (MAP_TILE_SIZE / 2) + (distance * cos(angle)), (render_height / 2) - (MAP_TILE_SIZE / 2) - (distance * sin(angle)), 32, 32 };
	// TODO: Do not assume shield is a single sprite frame from the effects sprite sheet
	SDL_RenderCopyEx(ren, spritesheet_effects, &sprite_animation_projectile_player_shield->GetAnimationFrame(0), &shield_render_rect, to_render_angle(angle), NULL, SDL_FLIP_NONE);
}

void Game::Update()
{
	BOOST_LOG_TRIVIAL(trace) << "New loop of Game::Update";

	SDL_PollEvent(NULL);

	player_input->UpdatePlayerInput();

	quit = player_input->ExitPressed();

	// Cool down UI delays
	for (std::vector<UIDelay*>::iterator it = ui_delays.begin(); it < ui_delays.end(); it++)
	{
		(*it)->Cooldown();
	}

	// Update UI elements
	ui_icon_health_potion->Update();
	ui_icon_down_arrow->Update();
	ui_icon_up_arrow->Update();
	ui_icon_effect_stun->Update();

	switch (current_game_state)
	{
		case(gamestate_title_screen) :
		{
			// Process input
			if (player_input->StartTapped())
			{
				BOOST_LOG_TRIVIAL(info) << "Game is transitioning from title screen to village";
				current_game_state = gamestate_village;
				NewGame();
				LoadSaveFile(GetSavePath() + "\\" + save_game_filename);
				if (player->GetItems().size() == 0)
				{
					// For testing, give the player some items
					for (int i = 0; i < 4; i++)
					{
						player->AddItem(item_generator->GenerateRandomArmour(player->GetLevel() + 4));
						player->AddItem(item_generator->GenerateRandomWeapon(player->GetLevel() + 4));
						player->AddItem(item_generator->GenerateRandomWeapon(player->GetLevel() + 4));
					}
				}
				ui_delay_town_menu_select->Activate();
			}
			break;
		}
		case(gamestate_playing) :
		{
			if (player->IsAlive())
			{
				if (paused)
				{
					if (open_dialogs.size() > 0)
					{
						UIDialog top_most_dialog = *(open_dialogs.back());
						UIDialogType type = top_most_dialog.GetDialogType();
						UIDialog* some_dialog = nullptr; // Declaring this here as it can't be declared in the switch
						switch (type)
						{
						case uidlg_pausegame:
						{
							GamePauseDialog* dialog = (GamePauseDialog*)open_dialogs.back();
							if (player_input->StartTapped())
							{
								paused = false;
								open_dialogs.pop_back();
								delete(dialog);
							}
							if (player_input->UpTapped())
							{
								dialog->InputUp();
							}
							if (player_input->DownTapped())
							{
								dialog->InputDown();
							}
							if (player_input->ConfirmTapped())
							{
								UIDialogType sub_dialog_type = dialog->InputConfirm();
								switch (sub_dialog_type)
								{
								case uidlg_character_inventory:
								{
									open_dialogs.push_back(new PlayerInventoryDialog(*ttffont_ui_20, player->GetItems()));
									break;
								}
								case uidlg_character_equip:
								{
									open_dialogs.push_back(new PlayerEquipDialog(*ttffont_ui_20, player));
									break;
								}
								default:
								{
									BOOST_LOG_TRIVIAL(warning) << "Don't know what pause menu selection with dialog type " << sub_dialog_type << " is! Doing nothing.";
								}
								}
							}
							break;
						}
						case uidlg_character_inventory:
						{
							PlayerInventoryDialog* dialog = (PlayerInventoryDialog*)open_dialogs.back();
							if (player_input->CancelPressed() || player_input->StartTapped())
							{
								open_dialogs.pop_back();
								delete(dialog);
							}
							break;
						}
						case uidlg_character_equip:
						{
							PlayerEquipDialog* dialog = (PlayerEquipDialog*)open_dialogs.back();
							if (player_input->CancelPressed() || player_input->StartTapped())
							{
								open_dialogs.pop_back();
								delete(dialog);
							}
							else {
								if (player_input->DownTapped())
								{
									dialog->InputDown();
								}
								if (player_input->UpTapped())
								{
									dialog->InputUp();
								}
								if (player_input->ConfirmTapped())
								{
									EquipSlot equip_slot = dialog->InputConfirm();
									ItemType item_type;
									switch (equip_slot)
									{
									case equip_slot_armour:
										item_type = item_type_armour;
										break;
									case equip_slot_primary_weapon:
										item_type = item_type_weapon;
										break;
									case equip_slot_secondary_weapon:
										item_type = item_type_weapon;
										break;
									}
									std::vector<std::shared_ptr<Item>> applicable_items = player->GetItems(item_type);
									open_dialogs.push_back(new EquipChangeList(*ttffont_ui_20, *player, equip_slot, applicable_items));
								}
							}
							break;
						}
						case uidlg_equip_change:
						{
							EquipChangeList* dialog = (EquipChangeList*)open_dialogs.back();
							if (player_input->CancelPressed() || player_input->StartTapped())
							{
								open_dialogs.pop_back();
								delete(dialog);
							}
							else {
								if (player_input->DownTapped())
								{
									dialog->InputDown();
								}
								if (player_input->UpTapped())
								{
									dialog->InputUp();
								}
								if (player_input->ConfirmTapped())
								{
									player->EquipItemInSlot(dialog->GetEquipSlot(), dialog->InputConfirm());
									open_dialogs.pop_back();
									delete(dialog);
								}
							}
							break;
						}
						default:
						{
							BOOST_LOG_TRIVIAL(warning) << "Don't know what dialog is open! Removing it.";
							some_dialog = open_dialogs.back();
							open_dialogs.pop_back();
							delete(some_dialog);
						}
						}
					}
					else
					{
						if (player_input->StartTapped())
						{
							BOOST_LOG_TRIVIAL(warning) << "Somehow paused with no dialog open. Player has tapped start, so just unpausing";
							paused = false;
						}
					}
				}
				else // Not paused
				{
					// Update the player before processing input so that previous input can be recorded
					player->Update();
					// Process input
					bool moved_this_frame = false;

					if (player_input->StartTapped())
					{
						paused = true;
						open_dialogs.push_back(new GamePauseDialog(*ttffont_ui_20));
					}

					if (player_input->RightPressed())
					{
						std::pair<double, double> new_pos = current_dungeon_map->ResultingCoordsFromAttemptedMove(player->GetX(), player->GetY(),
							player->GetX() + player->GetEffectiveMovementSpeed(), player->GetY(),
							player->GetWidth(), player->GetHeight());
						player->SetXYPosition(new_pos.first, new_pos.second);
						moved_this_frame = true;
						player->SetIfFacingRight(true);
						if (player->GetIfMoving() == false)
						{
							player->SetIfMoving(true);
							player->SetSpriteAnimation(sprite_animation_player_moving_right);
						}
					}
					if (player_input->LeftPressed())
					{
						std::pair<double, double> new_pos = current_dungeon_map->ResultingCoordsFromAttemptedMove(player->GetX(), player->GetY(),
							player->GetX() - player->GetEffectiveMovementSpeed(), player->GetY(),
							player->GetWidth(), player->GetHeight());
						player->SetXYPosition(new_pos.first, new_pos.second);
						moved_this_frame = true;
						player->SetIfFacingRight(false);
						if (player->GetIfMoving() == false)
						{
							player->SetIfMoving(true);
							player->SetSpriteAnimation(sprite_animation_player_moving_right);
						}
					}
					if (player_input->DownPressed())
					{
						std::pair<double, double> new_pos = current_dungeon_map->ResultingCoordsFromAttemptedMove(player->GetX(), player->GetY(),
							player->GetX(), player->GetY() + player->GetEffectiveMovementSpeed(),
							player->GetWidth(), player->GetHeight());
						player->SetXYPosition(new_pos.first, new_pos.second);
						moved_this_frame = true;
						if (player->GetIfMoving() == false)
						{
							player->SetIfMoving(true);
							player->SetSpriteAnimation(sprite_animation_player_moving_right);
						}
					}
					if (player_input->UpPressed())
					{
						std::pair<double, double> new_pos = current_dungeon_map->ResultingCoordsFromAttemptedMove(player->GetX(), player->GetY(),
							player->GetX(), player->GetY() - player->GetEffectiveMovementSpeed(),
							player->GetWidth(), player->GetHeight());
						player->SetXYPosition(new_pos.first, new_pos.second);
						moved_this_frame = true;
						if (player->GetIfMoving() == false)
						{
							player->SetIfMoving(true);
							player->SetSpriteAnimation(sprite_animation_player_moving_right);
						}
					}

					if (!moved_this_frame)
					{
						player->SetIfMoving(false);
						player->SetSpriteAnimation(sprite_animation_player_facing_right);
					}

					if (player_input->PrimaryAttackPressed())
					{
						if (player->GetPrimaryEquippedWeapon() && player->GetPrimaryEquippedWeapon()->GetWeaponType() == weapon_type_shield)
						{
							if (player->IsReadyToPrimaryDefend())
							{
								player->PrimaryDefend();
							}
						}
						else
						{
							if (player->IsReadyToPrimaryAttack())
							{
								StartPlayerAttack(*player->GetPrimaryEquippedWeapon(), true);
							}
						}
					}
					else // Primary attack button released 
					{
						if (player->IsPrimaryDefending())
						{
							player->StopPrimaryDefend();
						}
					}

					if (player_input->SecondaryAttackPressed())
					{
						if (player->GetSecondaryEquippedWeapon() && player->GetSecondaryEquippedWeapon()->GetWeaponType() == weapon_type_shield)
						{
							if (player->IsReadyToSecondaryDefend())
							{
								player->SecondaryDefend();
							}
						}
						else
						{
							if (player->IsReadyToSecondaryAttack())
							{
								StartPlayerAttack(*player->GetSecondaryEquippedWeapon(), false);
							}
						}
					}
					else
					{
						if (player->IsSecondaryDefending())
						{
							player->StopSecondaryDefend();
						}
					}

					if (player_input->HealthPotionPressed())
					{
						if (player->GetNumberOfHealthPotions() > 0 && player->IsReadyToDrinkPotion())
						{
							player->DrinkHealthPotion();
						}
					}

					// Update map fog of war
					current_dungeon_map->UpdateFogOfWar(*player);

					// Check player collisions
					if (distance_between((int)player->GetX(), (int)player->GetY(), current_dungeon_map->GetExitX(), current_dungeon_map->GetExitY()) < 16)
					{
						BOOST_LOG_TRIVIAL(debug) << "Player has collided with exit";
						if (current_dungeon_map->GetExitDestinationMap() == village_maptype)
						{
							BOOST_LOG_TRIVIAL(debug) << "Player stepped on exit back to village";
							current_game_state = gamestate_village;
							ui_delay_town_menu_select->Activate();
							if (current_quest->GetState() == quest_complete)
							{
								open_dialogs.push_back(new QuestCompleteDialog(*ttffont_ui_20, *current_quest));
							}
						}
						else
						{
							BOOST_LOG_TRIVIAL(debug) << "Player stepped on exit to next map";
							TransitionToMap(current_dungeon_map->GetExitDestinationMap());
						}
					}

					// Update everything
					std::vector<EnemyGenerator*> enemy_generators = current_dungeon_map->GetEnemyGenerators();
					for (std::vector<EnemyGenerator*>::iterator it = enemy_generators.begin(); it < enemy_generators.end(); it++)
					{
						(*it)->Update();
						if (distance_between(player->GetX(), player->GetY(), (*it)->GetX(), (*it)->GetY()) < (*it)->GetSpawnRadius() && (*it)->GetIfReadyToGenerateEnemy())
						{
							EnemyType enemy_type_to_generate = (*it)->GenerateEnemy();

							Enemy* enemy = bestiary->GenerateEnemy((*it)->GetEnemyGeneratorType(), current_quest->GetBaseLevel(), (*it)->GetX(), (*it)->GetY() + current_dungeon_map->GetTileSize() / 2);
							enemy->SetSpriteAnimation(SpriteAnimationForEnemyState(enemy->GetType(), idle));

							current_dungeon_map->AddEnemy(enemy);
						}
					}

					std::vector<Enemy*>& enemies = current_dungeon_map->GetEnemies();
					for (std::vector<Enemy*>::iterator it = enemies.begin(); it < enemies.end(); it++)
					{
						Enemy* enemy = *it;

						if (!enemy->IsAlive()) continue;
						enemy->Update();
						if (enemy->IsStunned()) continue;

						// Update movement
						int random_x = rand() % current_dungeon_map->GetWidthInPixels();
						int random_y = rand() % current_dungeon_map->GetHeightInPixels();
						// Not using switch because variable definition is needed
						if (enemy->GetMovementState() == idle)
						{
							// If idle for whatever reason, start wandering
							BOOST_LOG_TRIVIAL(debug) << "Idle enemy starting wandering";
							enemy->SetMovementState(wandering);
							enemy->SetPointOfInterest(random_x, random_y);
							enemy->SetIfFacingRight(random_x > enemy->GetX());
							enemy->SetSpriteAnimation(SpriteAnimationForEnemyState(enemy->GetType(), wandering));
						}
						else if (enemy->GetMovementState() == wandering)
						{
							double current_x = enemy->GetX();
							double current_y = enemy->GetY();
							if (distance_between(current_x, current_y, player->GetX(), player->GetY()) < enemy->GetAlertRadius())
							{
								if (enemy->CanBeAlarmed() && (rand() % 5 < 1))
								{
									enemy->Alarm();
									enemy->SetSpriteAnimation(SpriteAnimationForEnemyState(enemy->GetType(), alarmed));
									enemy->SetIfFacingRight(player->GetX() > enemy->GetX());
									// Set point of interest at current player location, to head there after ending alarm
									enemy->SetPointOfInterest(player->GetX(), player->GetY());
								}
								else
								{
									BOOST_LOG_TRIVIAL(debug) << "Wandering enemy will now target the player";
									enemy->SetMovementState(targeting);
									enemy->SetPointOfInterest(player->GetX(), player->GetY());
									enemy->SetIfFacingRight(player->GetX() > enemy->GetX());
								}
							}
							else
							{
								double angle_to_target = atan2(enemy->GetPointOfInterest().second - enemy->GetY(), enemy->GetPointOfInterest().first - enemy->GetX());
								double desired_x = enemy->GetX() + (enemy->GetMovementSpeed() * cos(angle_to_target));
								double desired_y = enemy->GetY() + (enemy->GetMovementSpeed() * sin(angle_to_target));
								std::pair<double, double> actual_coords = current_dungeon_map->ResultingCoordsFromAttemptedMove(enemy->GetX(), enemy->GetY(),
									desired_x, desired_y,
									enemy->GetWidth(), enemy->GetHeight());
								//BOOST_LOG_TRIVIAL(debug) << "moving enemy from (" << enemy->GetX() << "," << enemy->GetY() << ") to (" << actual_coords->first << "," << actual_coords->second << ")";
								enemy->SetX(actual_coords.first);
								enemy->SetY(actual_coords.second);
								// If blocked, then wander off somewhere else
								if (abs(current_x - actual_coords.first) < 0.1 && abs(current_y - actual_coords.second) < 0.1)
								{
									BOOST_LOG_TRIVIAL(trace) << "Wandering enemy couldn't move closer to target location, changing to a new random target location";
									enemy->SetPointOfInterest(random_x, random_y);
									enemy->SetIfFacingRight(random_x > enemy->GetX());
								}
							}
						}
						else if (enemy->GetMovementState() == alarmed)
						{
							if (!enemy->StillAlarmed())
							{
								BOOST_LOG_TRIVIAL(debug) << "Alarmed enemy will now target the player";
								enemy->SetMovementState(targeting);
								enemy->SetIfFacingRight(player->GetX() > enemy->GetX());
								enemy->SetSpriteAnimation(SpriteAnimationForEnemyState(enemy->GetType(), targeting));
							}
						}
						else if (enemy->GetMovementState() == targeting)
						{
							// If close to player, prioritise that over whatever the current point of interest was
							if (distance_between(enemy->GetX(), enemy->GetY(), player->GetX(), player->GetY()) < enemy->GetAlertRadius())
							{
								if (enemy->GetPointOfInterest().first != player->GetX() && enemy->GetPointOfInterest().second != player->GetY())
								{
									BOOST_LOG_TRIVIAL(trace) << "Enemy close to player, changing target from " << enemy->GetPointOfInterest().first << "," << enemy->GetPointOfInterest().second << " to " << player->GetX() << "," << player->GetY();
									enemy->SetPointOfInterest(player->GetX(), player->GetY());
									enemy->SetIfFacingRight(player->GetX() > enemy->GetX());
								}
							}
							double angle_to_target = atan2(enemy->GetPointOfInterest().second - enemy->GetY(), enemy->GetPointOfInterest().first - enemy->GetX());
							double desired_x = enemy->GetX() + (enemy->GetMovementSpeed() * cos(angle_to_target));
							double desired_y = enemy->GetY() + (enemy->GetMovementSpeed() * sin(angle_to_target));
							std::pair<double, double> actual_coords = current_dungeon_map->ResultingCoordsFromAttemptedMove(enemy->GetX(), enemy->GetY(), desired_x, desired_y, enemy->GetWidth(), enemy->GetHeight());

							//BOOST_LOG_TRIVIAL(debug) << "Enemy moving towards player from " << enemy->GetX() << "," << enemy->GetY() << " to " << actual_coords->first << "," << actual_coords->second << " heading towards " << enemy->GetPointOfInterest()->first << "," << enemy->GetPointOfInterest()->second << " on an angle of " << angle_to_target;
							enemy->SetX(actual_coords.first);
							enemy->SetY(actual_coords.second);
							// If close to target, go off wandering
							if (abs(enemy->GetPointOfInterest().first - actual_coords.first) < 2 && abs(enemy->GetPointOfInterest().second - actual_coords.second) < 2)
							{
								BOOST_LOG_TRIVIAL(trace) << "Enemy close to target location, reverting from targeting to wandering.";
								enemy->SetMovementState(wandering);
								enemy->SetPointOfInterest(random_x, random_y);
								enemy->SetIfFacingRight(random_x > enemy->GetX());
							}
						}
					}

					// Update NPCs
					std::vector<Npc*> npcs = current_dungeon_map->GetNpcs();
					for (std::vector<Npc*>::iterator it = npcs.begin(); it < npcs.end(); it++)
					{
						Npc* npc = *it;

						if (!npc->IsAlive()) continue;
						npc->Update();
					}

					// Update projectiles
					for (std::vector<std::unique_ptr<Projectile>>::iterator it = projectiles.begin(); it < projectiles.end(); it++)
					{
						(*it)->Update();
						double desired_x = (*it)->GetX() + ((*it)->GetSpeed() * cos((*it)->GetAngle()));
						double desired_y = (*it)->GetY() - ((*it)->GetSpeed() * sin((*it)->GetAngle()));
						std::pair<double, double> actual_coords = current_dungeon_map->ResultingCoordsFromAttemptedMove((*it)->GetX(), (*it)->GetY(), desired_x, desired_y, (*it)->GetWidth(), (*it)->GetHeight(), true);
						// Kill projectile if it's getting pushed back
						if (desired_x != actual_coords.first || desired_y != actual_coords.second)
						{
							(*it)->SetLifeTime(0);
						}
						(*it)->SetX(actual_coords.first);
						(*it)->SetY(actual_coords.second);

					}

					//Update dropped items
					std::vector<std::shared_ptr<DroppedItem>> dropped_items = current_dungeon_map->GetDroppedItems();
					for (std::vector<std::shared_ptr<DroppedItem>>::iterator it = dropped_items.begin(); it < dropped_items.end(); it++)
					{
						(*it)->Update();
					}

					// Check for collisions between projectiles and enemies
					enemies = current_dungeon_map->GetEnemies();
					for (std::vector<std::unique_ptr<Projectile>>::iterator projectile = projectiles.begin(); projectile < projectiles.end(); projectile++)
					{
						if ((*projectile)->GetIfActive() && (*projectile)->GetIfHurtsEnemies())
						{
							for (std::vector<Enemy*>::iterator enemy = enemies.begin(); enemy < enemies.end(); enemy++)
							{
								if ((*enemy)->IsAlive() && !(*enemy)->IsInInvincibilityFrames())
								{
									if (CheckCollisionFromCentre((*projectile)->GetX(), (*projectile)->GetY(), (*projectile)->GetWidth(), (*projectile)->GetHeight(),
										(*enemy)->GetX(), (*enemy)->GetY(), (*enemy)->GetWidth(), (*enemy)->GetHeight()))
									{

										if ((*enemy)->ApplyDamage((*projectile)->GetDamage()))
										{
											KillEnemy(*enemy);
										}
										(*projectile)->Deactivate();
									}
								}
							}
						}
					}

					// Check for collisions between projectiles and clutter
					std::vector<std::shared_ptr<ClutterItem>> clutter = current_dungeon_map->GetClutter();
					for (std::vector<std::unique_ptr<Projectile>>::iterator projectile = projectiles.begin(); projectile < projectiles.end(); projectile++)
					{
						if ((*projectile)->GetIfActive() && (*projectile)->GetIfHurtsEnemies())
						{
							for (std::vector<std::shared_ptr<ClutterItem>>::iterator clutter_item = clutter.begin(); clutter_item < clutter.end(); clutter_item++)
							{
								if (CheckCollisionFromCentre((*projectile)->GetX(), (*projectile)->GetY(), (*projectile)->GetWidth(), (*projectile)->GetHeight(),
									(*clutter_item)->GetX(), (*clutter_item)->GetY(), (*clutter_item)->GetWidth(), (*clutter_item)->GetHeight()))
								{
									DestroyClutterItem(*clutter_item);
								}
							}
						}
					}

					// Check for collisions between projectiles and enemy generators
					auto generators = current_dungeon_map->GetEnemyGenerators();
					for (std::vector<std::unique_ptr<Projectile>>::iterator projectile = projectiles.begin(); projectile < projectiles.end(); projectile++)
					{
						if ((*projectile)->GetIfActive() && (*projectile)->GetIfHurtsEnemies())
						{
							for (std::vector<EnemyGenerator*>::iterator generator = generators.begin(); generator < generators.end(); generator++)
							{
								if (CheckCollisionFromCentre((*projectile)->GetX(), (*projectile)->GetY(), (*projectile)->GetWidth(), (*projectile)->GetHeight(),
									(*generator)->GetX(), (*generator)->GetY(), (*generator)->GetWidth(), (*generator)->GetHeight()))
								{
									(*generator)->ApplyDamage((*projectile)->GetDamage());
									(*projectile)->Deactivate();
								}
							}
						}
					}

					// Check for collisions between enemies and player
					if (!player->IsInInvincibilityFrames())
					{
						for (std::vector<Enemy*>::iterator enemy = enemies.begin(); enemy < enemies.end(); enemy++)
						{
							if ((*enemy)->IsAlive())
							{
								if (CheckCollisionFromCentre(player->GetX(), player->GetY(), player->GetWidth(), player->GetHeight(),
									(*enemy)->GetX(), (*enemy)->GetY(), (*enemy)->GetWidth(), (*enemy)->GetHeight()))
								{

									if (player->ApplyDamage((*enemy)->GetMeleeDamage()))
									{
										BOOST_LOG_TRIVIAL(debug) << "Player killed!";
									}
								}
							}
						}
					}

					// Check for collisions between player and dropped items
					dropped_items = current_dungeon_map->GetDroppedItems();
					for (std::vector<std::shared_ptr<DroppedItem>>::iterator it = dropped_items.begin(); it < dropped_items.end(); it++)
					{
						if (CheckCollisionFromCentre(player->GetX(), player->GetY(), player->GetWidth(), player->GetHeight(),
							(*it)->GetX(), (*it)->GetY(), (*it)->GetWidth(), (*it)->GetHeight()))
						{
							switch ((*it)->GetType())
							{
							case drop_item_gold:
								player->AddGold((*it)->GetValue());
								(*it)->SetIsCollected(true);
								break;
							case drop_item_armour_random:
							{
								std::shared_ptr<Item> item = (*it)->GetItem();
								player->AddItem(item);
								(*it)->SetIsCollected(true);
								BOOST_LOG_TRIVIAL(debug) << "Player picked up armour '" << item->GetName() << "'";
								break;
							}
							case drop_item_weapon_random:
							{
								std::shared_ptr<Item> item = (*it)->GetItem();
								player->AddItem(item);
								(*it)->SetIsCollected(true);
								BOOST_LOG_TRIVIAL(debug) << "Player picked up weapon '" << item->GetName() << "'";
								break;
							}
							case drop_item_potion:
								player->AddItem((*it)->GetItem());
								(*it)->SetIsCollected(true);
								break;
							case drop_item_quest:
								player->AddItem((*it)->GetItem());
								(*it)->SetIsCollected(true);
								break;
							default:
								BOOST_LOG_TRIVIAL(error) << "Player bumped into a dropped item of type " << (*it)->GetType() << " but that drop type isn't handled.";
							}
						}
					}

					// If player is swinging a weapon, check for collisions
					if (player->GetPrimaryEquippedWeapon() && player->IsDirectlyPrimaryAttacking())
					{
						this->PlayerDirectAttack(player->GetPrimaryAttackDamage());
					}
					if (player->GetSecondaryEquippedWeapon() && player->IsDirectlySecondaryAttacking())
					{
						this->PlayerDirectAttack(player->GetSecondaryAttackDamage());
					}

					if (player->IsPrimaryDefending())
					{
						SDL_Rect primary_shield_rect = { player->GetX() + (player->GetShieldDistanceFromPlayer() * cos(player->GetPrimaryDefendingAngle())) - 8,
														 player->GetY() - (player->GetShieldDistanceFromPlayer() * sin(player->GetPrimaryDefendingAngle())) - 8,
														 16, 16 };
						bool defence_broken = false;
						for (std::vector<Enemy*>::iterator enemy = enemies.begin(); enemy < enemies.end(); enemy++)
						{
							if ((*enemy)->IsAlive())
							{
								if (CheckCollisionFromCentre(primary_shield_rect.x + 8, primary_shield_rect.y + 8, primary_shield_rect.w, primary_shield_rect.h,
									(*enemy)->GetX(), (*enemy)->GetY(), (*enemy)->GetWidth(), (*enemy)->GetHeight()))
								{
									(*enemy)->Stun(60);
									// TODO: For testing, hit hard. Change this to actual enemy attack power
									if (player->PrimaryDefendBlock(100)) break;
								}
							}
						}
					}

					if (player->IsSecondaryDefending())
					{
						SDL_Rect secondary_shield_rect = { player->GetX() + (player->GetShieldDistanceFromPlayer() * cos(player->GetSecondaryDefendingAngle())) - 8,
							                               player->GetY() - (player->GetShieldDistanceFromPlayer() * sin(player->GetSecondaryDefendingAngle())) - 8,
							                               16, 16 };
						bool defence_broken = false;
						for (std::vector<Enemy*>::iterator enemy = enemies.begin(); enemy < enemies.end(); enemy++)
						{
							if ((*enemy)->IsAlive())
							{
								if (CheckCollisionFromCentre(secondary_shield_rect.x + 8, secondary_shield_rect.y + 8, secondary_shield_rect.w, secondary_shield_rect.h,
									(*enemy)->GetX(), (*enemy)->GetY(), (*enemy)->GetWidth(), (*enemy)->GetHeight()))
								{
									(*enemy)->Stun(60);
									// TODO: For testing, hit hard. Change this to actual enemy attack power
									if (player->SecondaryDefendBlock(100)) break;
								}
							}
						}
					}

					// Delete expired projectiles
					projectiles.erase(std::remove_if(projectiles.begin(), projectiles.end(),
						[](std::unique_ptr<Projectile> &pc) { return !pc->GetIfActive(); }), projectiles.end());

					// Delete expired entities from dungeon map
					current_dungeon_map->RemoveDeadEnemies();
					current_dungeon_map->RemoveCollectedDroppedItems();
					current_dungeon_map->RemoveDestroyedClutter();
					current_dungeon_map->RemoveDestroyedEnemyGenerators();

					std::vector<Furniture*> furniture = current_dungeon_map->GetFurniture();
					for (std::vector<Furniture*>::iterator it = furniture.begin(); it < furniture.end(); it++)
					{
						(*it)->Update();
					}

					generators = current_dungeon_map->GetEnemyGenerators();
					for (std::vector<EnemyGenerator*>::iterator it = generators.begin(); it < generators.end(); it++)
					{
						(*it)->Update();
					}

					// Update quest
					if (current_quest)
					{
						if (current_quest->GetState() == quest_in_progress)
						{
							switch (current_quest->GetQuestCompletionCriteriaType())
							{
							case reach_a_location:
								BOOST_LOG_TRIVIAL(trace) << "About to test if player has reached quest location...";
								if (current_dungeon_map->GetDungeonMapType() == current_quest->GetDestination())
								{
									if (current_quest->IsNearCompletionLocation((int)player->GetX(), (int)player->GetY(), 10))
									{
										BOOST_LOG_TRIVIAL(debug) << "Player is close to quest target, marking quest as complete";
										current_quest->SetState(quest_complete);
										current_dungeon_map->OpenExitBackToVillageNear(player->GetX(), player->GetY());
										current_dungeon_map->AddFurniture(new Furniture(current_dungeon_map->GetExitX(), current_dungeon_map->GetExitY(), 32, 32, sprite_animation_furniture_map_exit));
										BOOST_LOG_TRIVIAL(debug) << "Finished adding exit to level for player to use";
									}
								}
								else
								{
									BOOST_LOG_TRIVIAL(trace) << "Current map " << current_dungeon_map->GetDungeonMapType() << " is not the quest's target map " << current_quest->GetDestination();
								}
								break;
							case find_an_item:
								if (player->HasQuestItem(current_quest->GetQuestItemId()))
								{
									BOOST_LOG_TRIVIAL(debug) << "Player has the item for this item collection quest, quest complete!";
									current_quest->SetState(quest_complete);
									current_dungeon_map->OpenExitBackToVillageNear(player->GetX(), player->GetY());
									current_dungeon_map->AddFurniture(new Furniture(current_dungeon_map->GetExitX(), current_dungeon_map->GetExitY(), 32, 32, sprite_animation_furniture_map_exit));
									player->RemoveQuestItem(current_quest->GetQuestItemId());
								}
								break;
							default:
								BOOST_LOG_TRIVIAL(error) << "Unknown quest type when updating current quest: " << current_quest->GetQuestCompletionCriteriaType();
								break;
							}
						}
					}
				}
			} 
			else // Player is not alive 
			{
				if (player_input->StartTapped())
				{
					GameOver();
					WriteSaveFile(this->GetSavePath() + "\\SaveGame.dat");
					TransitionPlayingToTitle();
				}
			}
			break;
		}
		case(gamestate_village) :
		{
			if (ui_delay_town_menu_select->Ready())
			{
				if (player_input->DownPressed())
				{
					if (!AnyDialogOpen())
					{
						current_town_menu_selection += 1;
						ui_delay_town_menu_select->Activate();
						if (current_town_menu_selection >= (int)current_town_menu->GetOptions()->size())
						{
							current_town_menu_selection = 0;
						}
					}
					else {
						ui_delay_town_menu_select->Activate();
						open_dialogs.back()->InputDown();
					}
				}
				if (player_input->UpPressed())
				{
					if (!AnyDialogOpen())
					{
						current_town_menu_selection -= 1;
						ui_delay_town_menu_select->Activate();
						if (current_town_menu_selection < 0)
						{
							current_town_menu_selection = current_town_menu->GetOptions()->size() - 1;
						}
					}
					else {
						ui_delay_town_menu_select->Activate();
						open_dialogs.back()->InputUp();
					}
				}
				// No left or right for town menu
				if (player_input->LeftPressed())
				{
					if (AnyDialogOpen())
					{
						ui_delay_town_menu_select->Activate();
						open_dialogs.back()->InputLeft();
					}
				}
				if (player_input->RightPressed())
				{
					if (AnyDialogOpen())
					{
						ui_delay_town_menu_select->Activate();
						open_dialogs.back()->InputRight();
					}
				}
				if (player_input->ConfirmTapped())
				{
					if (!AnyDialogOpen())
					{
						if (current_town_menu->GetOptions()->at(current_town_menu_selection).GetTownMenuOptionId() == town_quests)
						{
							BOOST_LOG_TRIVIAL(debug) << "Opening quest list...";
							open_dialogs.push_back(new AvailableQuestDialog(*ttffont_ui_20, available_quests_in_village));
							ui_delay_town_menu_select->Activate();
						}
						if (current_town_menu->GetOptions()->at(current_town_menu_selection).GetTownMenuOptionId() == town_ventureforth)
						{
							if (current_quest == nullptr)
							{
								open_dialogs.push_back(new MessageDialog(*ttffont_ui_20, "You must choose a quest before venturing forth!"));
								ui_delay_town_menu_select->Activate();
							}
							else
							{
								BOOST_LOG_TRIVIAL(debug) << "Venturing forth...";
								ui_delay_town_menu_select->Activate();
								if (!quest_prepared)
								{
									PrepareQuest(*current_quest);
								}
								current_game_state = gamestate_playing;
							}
						}
						if (current_town_menu->GetOptions()->at(current_town_menu_selection).GetTownMenuOptionId() == town_merchant)
						{
							BOOST_LOG_TRIVIAL(debug) << "Opening merchant dialog...";
							open_dialogs.push_back(new MerchantWelcomeDialog(*ttffont_ui_20));
							ui_delay_town_menu_select->Activate();
						}
						if (current_town_menu->GetOptions()->at(current_town_menu_selection).GetTownMenuOptionId() == town_banker)
						{
							BOOST_LOG_TRIVIAL(debug) << "Opening banker dialog...";
							open_dialogs.push_back(new BankerWelcomeDialog(*ttffont_ui_20, bank_balance));
							ui_delay_town_menu_select->Activate();
						}
					}
					else
					{
						if (open_dialogs.size() > 0)
						{
							BOOST_LOG_TRIVIAL(debug) << "Confirming dialog...";
							UIDialog top_most_dialog = *(open_dialogs.back());
							UIDialogType type = top_most_dialog.GetDialogType();
							BOOST_LOG_TRIVIAL(debug) << "...of type '" << type << "'";
							switch (type)
							{
								case uidlg_available_quests:
								{
									std::string quest_title = "";
									AvailableQuestDialog* available_quest_dialog = (AvailableQuestDialog*)open_dialogs.back();
									current_quest = available_quest_dialog->InputConfirm();

									quest_title = current_quest->GetTitle();
									BOOST_LOG_TRIVIAL(debug) << "Closing quest list, selecting quest '" << quest_title << "'";
									open_dialogs.pop_back();
									delete available_quest_dialog;
									ui_delay_town_menu_select->Activate();
									break;
								}
								case uidlg_quest_completed:
								{
									open_dialogs.pop_back();
									ui_delay_town_menu_select->Activate();
									TurnInQuest();
									break;
								}
								case uidlg_message:
								{
									open_dialogs.pop_back();
									ui_delay_town_menu_select->Activate();
									break;
								}
								case uidlg_merchant_welcome:
								{
									MerchantWelcomeDialog* merchant_welcome_dialog = (MerchantWelcomeDialog*)open_dialogs.back();
									UIDialogType sub_dialog_type = merchant_welcome_dialog->InputConfirm();
									switch (sub_dialog_type)
									{
										case uidlg_merchant_buy:
										{
											open_dialogs.push_back(new MerchantBuyDialog(*ttffont_ui_20, merchant_items, item_description_index, *player));
											break;
										}
										case uidlg_merchant_sell:
										{
											open_dialogs.push_back(new MerchantSellDialog(*ttffont_ui_20, *player));
											break;
										}
										case uidlg_merchant_invest:
										{
											open_dialogs.push_back(new MerchantInvestDialog(*ttffont_ui_20, CostForNextMerchantLevel(), *player));
											break;
										}
									}
									ui_delay_town_menu_select->Activate();
									break;
								}
								case uidlg_merchant_buy:
								{
									std::shared_ptr<Item> selected_item = ((MerchantBuyDialog*)open_dialogs.back())->InputConfirm();
									BOOST_LOG_TRIVIAL(debug) << "Player has selected to buy " << selected_item->GetName() << " at cost " << player->BuyingPriceFor(selected_item) << " from the merchant.";
									if (player->GetGold() >= player->BuyingPriceFor(selected_item))
									{
										player->SetGold(player->GetGold() - player->BuyingPriceFor(selected_item));
										player->AddItem(selected_item);
										merchant_items.erase(std::remove_if(merchant_items.begin(), merchant_items.end(), [selected_item](std::shared_ptr<Item> i) { return i == selected_item; }));
										((MerchantBuyDialog*)open_dialogs.back())->SetInventory(merchant_items);
									}
									else
									{
										open_dialogs.push_back(new MessageDialog(*ttffont_ui_20, "You Cannot Afford That!"));
									}
									break;
								}
								case uidlg_merchant_sell:
								{
									std::shared_ptr<Item> selected_item = ((MerchantSellDialog*)open_dialogs.back())->InputConfirm();
									BOOST_LOG_TRIVIAL(debug) << "Player has selected to sell " << selected_item->GetName() << " to merchant for " << player->SellingPriceFor(selected_item);
									player->AddGold(player->SellingPriceFor(selected_item));
									player->RemoveItem(selected_item);
									break;
								}
								case uidlg_merchant_invest:
								{
									if (player->GetGold() >= CostForNextMerchantLevel())
									{
										BOOST_LOG_TRIVIAL(debug) << "Upgrading merchant to level " << std::to_string(merchant_level + 1);
										player->SetGold(player->GetGold() - CostForNextMerchantLevel());
										merchant_level++;
										FillMerchantInventory();
										open_dialogs.pop_back();
										open_dialogs.push_back(new MessageDialog(*ttffont_ui_20, "Merchant upgraded to level " + std::to_string(merchant_level) + "!"));
									}
									else
									{
										open_dialogs.push_back(new MessageDialog(*ttffont_ui_20, "You Cannot Afford That!"));
									}
									break;
								}
								case uidlg_banker_welcome:
								{
									UIDialogType sub_dialog_type = ((BankerWelcomeDialog*)open_dialogs.back())->InputConfirm();
									switch (sub_dialog_type)
									{
										case(uidlg_banker_deposit) :
										{
											// TODO: Set the max amount
											BankerDepositDialog* deposit_dlg = new BankerDepositDialog(*ttffont_ui_20, *ui_icon_up_arrow, *ui_icon_down_arrow);
											open_dialogs.push_back(deposit_dlg);
											break;
										}
										case(uidlg_banker_withdraw) :
										{
											BankerWithdrawDialog* withdraw_dlg = new BankerWithdrawDialog(*ttffont_ui_20, *ui_icon_up_arrow, *ui_icon_down_arrow);
											open_dialogs.push_back(withdraw_dlg);
											break;
										}
									}
									break;
								}
								case uidlg_banker_deposit:
								{
									int deposit_amount = ((BankerDepositDialog*)open_dialogs.back())->InputConfirm();
									BOOST_LOG_TRIVIAL(debug) << "Player wants to deposit " << std::to_string(deposit_amount);
									double actual_amount = std::min((double)deposit_amount, player->GetGold());
									if (deposit_amount != actual_amount)
									{
										BOOST_LOG_TRIVIAL(debug) << "Player can only afford to deposit " << std::to_string(actual_amount);
									}
									bank_balance += actual_amount;
									player->SetGold(player->GetGold() - actual_amount);
									BOOST_LOG_TRIVIAL(debug) << "Closing banker deposit dialogue.";
									BankerDepositDialog* banker_deposit_dialog = (BankerDepositDialog*)open_dialogs.back();
									open_dialogs.pop_back();
									delete banker_deposit_dialog;
									ui_delay_town_menu_select->Activate();
									break;
								}
								case uidlg_banker_withdraw:
								{
									int withdraw_amount = ((BankerWithdrawDialog*)open_dialogs.back())->InputConfirm();
									BOOST_LOG_TRIVIAL(debug) << "Player wants to withdraw " << std::to_string(withdraw_amount);
									double actual_amount = std::min(withdraw_amount, bank_balance);
									if (withdraw_amount != actual_amount)
									{
										BOOST_LOG_TRIVIAL(debug) << "Player can only afford to withdraw " << std::to_string(actual_amount);
									}
									bank_balance -= actual_amount;
									player->SetGold(player->GetGold() + actual_amount);
									BOOST_LOG_TRIVIAL(debug) << "Closing banker withdraw dialogue.";
									BankerWithdrawDialog* banker_withdraw_dialog = (BankerWithdrawDialog*)open_dialogs.back();
									open_dialogs.pop_back();
									delete banker_withdraw_dialog;
									ui_delay_town_menu_select->Activate();
									break;
								}
								default:
									BOOST_LOG_TRIVIAL(warning) << "Unhandled player confirmation press on uidlg of type " << type;
							}
						}
						else
						{
							BOOST_LOG_TRIVIAL(error) << "Trying to process ConfirmPressed on a UI dialog but open_dialogs has size 0";
						}
						BOOST_LOG_TRIVIAL(debug) << "After processing ConfirmPressed on topmost dialog, size of open dialogs vector is: " << open_dialogs.size();
					}
				}
				if (player_input->CancelPressed())
				{
					if (AnyDialogOpen())
					{
						switch (open_dialogs.back()->GetDialogType())
						{
							case uidlg_available_quests:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing quest list...";
								AvailableQuestDialog* available_quest_dialog = (AvailableQuestDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete available_quest_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_merchant_welcome:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing merchant welcome dialogue...";
								MerchantWelcomeDialog* merchant_welcome_dialog = (MerchantWelcomeDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete merchant_welcome_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_merchant_buy:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing merchant buy dialogue...";
								MerchantBuyDialog* merchant_buy_dialog = (MerchantBuyDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete merchant_buy_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_merchant_sell:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing merchant sell dialogue...";
								MerchantSellDialog* merchant_sell_dialog = (MerchantSellDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete merchant_sell_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_merchant_invest:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing merchant invest dialogue...";
								MerchantInvestDialog* merchant_invest_dialog = (MerchantInvestDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete merchant_invest_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_banker_welcome:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing banker welcome dialogue.";
								BankerWelcomeDialog* banker_welcome_dialog = (BankerWelcomeDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete banker_welcome_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_banker_deposit:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing banker deposit dialogue.";
								BankerDepositDialog* banker_deposit_dialog = (BankerDepositDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete banker_deposit_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
							case uidlg_banker_withdraw:
							{
								BOOST_LOG_TRIVIAL(debug) << "Closing banker withdraw dialogue.";
								BankerWithdrawDialog* banker_withdraw_dialog = (BankerWithdrawDialog*)open_dialogs.back();
								open_dialogs.pop_back();
								delete banker_withdraw_dialog;
								ui_delay_town_menu_select->Activate();
								break;
							}
						}
					}
				}
			}

			// Update NPCs
			std::vector<Npc*> npcs = village_map->GetNpcs();
			for (std::vector<Npc*>::iterator it = npcs.begin(); it < npcs.end(); it++)
			{
				Npc* npc = *it;

				if (!npc->IsAlive()) continue;
				npc->Update();
			}

			break;
		}
		default:
		{
			BOOST_LOG_TRIVIAL(error) << "Updating defaulted on current_game_state when it should always be defined. Value is: " << current_game_state;
		}
	}
}

void Game::Render()
{
	// Switch to the statically sized texture
	SDL_SetRenderTarget(ren, static_sized_texture);

	// Clear
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	// Draw things...

	switch (current_game_state)
	{
		case(gamestate_title_screen) :
		{
			int w, h;
			SDL_QueryTexture(string_texture_ui_titlescreen_title, NULL, NULL, &w, &h);
			SDL_Rect dest;
			dest.x = (render_width / 2) - (w / 2);
			dest.y = (render_height / 6);
			dest.w = w;
			dest.h = h;
			/*BOOST_LOG_TRIVIAL(debug) << "Rendering title string with width " << w << ", height " << h << " and x,y " << dest.x << "," << dest.y
			<< " on render width " << render_width << " and height " << render_height;*/
			SDL_RenderCopy(ren, string_texture_ui_titlescreen_title, NULL, &dest);
			break;
		}
		case(gamestate_playing) :
		{
			// Draw the map
			BOOST_LOG_TRIVIAL(trace) << "About to render the map";
			current_dungeon_map->Render(ren, (int)player->GetX(), (int)player->GetY());

			// Draw enemy generators
			BOOST_LOG_TRIVIAL(trace) << "About to render enemy generators";
			std::vector<EnemyGenerator*> enemy_generators = current_dungeon_map->GetEnemyGenerators();
			for (std::vector<EnemyGenerator*>::iterator it = enemy_generators.begin(); it < enemy_generators.end(); it++)
			{
				SDL_Rect generator_rect;
				generator_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetX() - (*it)->GetWidth() / 2);
				generator_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetY() - (*it)->GetHeight() / 2);
				generator_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				generator_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				BOOST_LOG_TRIVIAL(trace) << "Rendering enemy generator at " << (int)(*it)->GetX() << "," << (int)(*it)->GetY();
				SDL_RenderCopy(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(), &(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &generator_rect);
			}

			// Draw furniture
			BOOST_LOG_TRIVIAL(trace) << "About to render furniture";
			std::vector<Furniture*> furniture = current_dungeon_map->GetFurniture();
			for (std::vector<Furniture*>::iterator it = furniture.begin(); it < furniture.end(); it++)
			{
				SDL_Rect furniture_rect;
				furniture_rect.x = (int)((*it)->GetX() + (render_width / 2) - (int)player->GetX() - (*it)->GetWidth() / 2);
				furniture_rect.y = (int)((*it)->GetY() + (render_height / 2) - (int)player->GetY() - (*it)->GetHeight() / 2);
				furniture_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				furniture_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				BOOST_LOG_TRIVIAL(trace) << "Rendering furniture with x,y,w,h = " << furniture_rect.x << "," << furniture_rect.y << "," << furniture_rect.w << "," << furniture_rect.h;
				SDL_RenderCopy(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(), &(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &furniture_rect);
			}

			// Draw clutter
			std::vector<std::shared_ptr<ClutterItem>> clutter = current_dungeon_map->GetClutter();
			for (std::vector<std::shared_ptr<ClutterItem>>::iterator it = clutter.begin(); it < clutter.end(); it++)
			{
				SDL_Rect clutter_rect;
				clutter_rect.x = (int)((*it)->GetX() + (render_width / 2) - (int)player->GetX() - (*it)->GetWidth() / 2);
				clutter_rect.y = (int)((*it)->GetY() + (render_height / 2) - (int)player->GetY() - (*it)->GetHeight() / 2);
				clutter_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				clutter_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				SDL_RenderCopy(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(), &(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &clutter_rect);
			}

			// Draw dropped items
			BOOST_LOG_TRIVIAL(trace) << "About to render dropped items";
			std::vector<std::shared_ptr<DroppedItem>> items = current_dungeon_map->GetDroppedItems();
			for (std::vector<std::shared_ptr<DroppedItem>>::iterator it = items.begin(); it < items.end(); it++)
			{
				SDL_Rect item_rect;
				item_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (int)player->GetX() - (*it)->GetWidth() / 2);
				item_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (int)player->GetY() - (*it)->GetHeight() / 2);
				item_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				item_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				SDL_RenderCopy(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(), &(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &item_rect);
			}

			// Draw enemies
			BOOST_LOG_TRIVIAL(trace) << "About to render enemies";
			SDL_RendererFlip enemy_flip = SDL_FLIP_NONE;
			std::vector<Enemy*>& enemies = current_dungeon_map->GetEnemies();
			for (std::vector<Enemy*>::iterator it = enemies.begin(); it < enemies.end(); it++)
			{
				SDL_Rect enemy_rect;
				enemy_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetX());
				enemy_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetY());
				enemy_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				enemy_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				if ((*it)->GetIfFacingRight())
				{
					enemy_flip = SDL_FLIP_NONE;
				}
				else
				{
					enemy_flip = SDL_FLIP_HORIZONTAL;
				}
				BOOST_LOG_TRIVIAL(trace) << "About to render enemy of type " << (*it)->GetType() << " with sprite rect of (" << enemy_rect.x << "," << enemy_rect.y << "," << enemy_rect.w << "," << enemy_rect.h << ")";
				SDL_RenderCopyEx(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(),
					&(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &enemy_rect,
					0, NULL, enemy_flip);
				if ((*it)->IsStunned())
				{
					SDL_Rect stun_rect;
					stun_rect.x = enemy_rect.x + (enemy_rect.w / 2) - (ui_icon_effect_stun->GetWidth() / 2);
					stun_rect.y = enemy_rect.y - (ui_icon_effect_stun->GetHeight() / 2);
					stun_rect.w = ui_icon_effect_stun->GetWidth();
					stun_rect.h = ui_icon_effect_stun->GetHeight();
					SDL_RenderCopy(ren, ui_icon_effect_stun->GetSpriteAnimation()->GetSpriteSheet(), &(ui_icon_effect_stun->GetSpriteAnimation()->GetAnimationFrame(ui_icon_effect_stun->GetAnimationPosition())), &stun_rect);
				}
			}

			// Draw NPCs
			SDL_RendererFlip npc_flip = SDL_FLIP_NONE;
			std::vector<Npc*> npcs = current_dungeon_map->GetNpcs();
			for (std::vector<Npc*>::iterator it = npcs.begin(); it < npcs.end(); it++)
			{
				SDL_Rect npc_rect;
				npc_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetX());
				npc_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetY());
				npc_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				npc_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				if ((*it)->GetIfFacingRight())
				{
					npc_flip = SDL_FLIP_NONE;
				}
				else
				{
					npc_flip = SDL_FLIP_HORIZONTAL;
				}
				SDL_RenderCopyEx(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(),
					&(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &npc_rect,
					0, NULL, npc_flip);
			}

			// Draw the player
			SDL_Rect rect_render_player;
			rect_render_player.x = (render_width / 2) - (MAP_TILE_SIZE / 2);
			rect_render_player.y = (render_height / 2) - (MAP_TILE_SIZE / 2);
			rect_render_player.w = MAP_TILE_SIZE;
			rect_render_player.h = MAP_TILE_SIZE;
			SDL_RendererFlip player_flip = SDL_FLIP_NONE;
			if (!player->GetIfFacingRight())
			{
				player_flip = SDL_FLIP_HORIZONTAL;
			}
			SDL_RenderCopyEx(ren, player->GetSpriteAnimation()->GetSpriteSheet(),
				&player->GetSpriteAnimation()->GetAnimationFrame(player->GetAnimationPosition()), &rect_render_player,
				0, NULL, player_flip);

			// Draw primary and secondary weapons
			DrawPlayerWeapon(true);
			DrawPlayerWeapon(false);

			if (player->IsPrimaryDefending())
			{
				DrawPlayerShield(true);
			}
			if (player->IsSecondaryDefending())
			{
				DrawPlayerShield(false);
			}

			// Draw projectiles
			SDL_RendererFlip projectile_flip = SDL_FLIP_NONE;;
			for (std::vector<std::unique_ptr<Projectile>>::iterator it = projectiles.begin(); it < projectiles.end(); it++)
			{
				SDL_Rect projectile_rect;
				projectile_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (int)player->GetX() - (int)(*it)->GetSpriteAnimation()->GetAnimationFrame(0).w / 2);
				projectile_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (int)player->GetY() - (int)(*it)->GetSpriteAnimation()->GetAnimationFrame(0).h / 2);
				projectile_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				projectile_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				if ((*it)->GetIfFacingRight())
				{
					projectile_flip = SDL_FLIP_NONE;
				}
				else
				{
					// Have the "bottom" of the sprite always face down
					projectile_flip = SDL_FLIP_VERTICAL;
				}
				SDL_RenderCopyEx(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(),
					&(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &projectile_rect,
					to_render_angle((*it)->GetAngle()), NULL, projectile_flip);
			}

			// Draw Fog of War
			current_dungeon_map->RenderFogOfWar(ren, basic_128_texture, (int)player->GetX(), (int)player->GetY());

			// Draw UI
			SDL_Texture* gold_and_xp = CreateTextureForString(ren, *ttffont_ui_20, std::string("LVL ") + StringForWholePart(player->GetLevel()) + std::string(" Gold: ") + StringForWholePart(player->GetGold()) + std::string(" Next lvl: ") + StringForWholePart(player->GetXPForNextLevel()), SDL_Color{ 255,255,0,255 }, render_width);
			int w, h;
			SDL_QueryTexture(gold_and_xp, NULL, NULL, &w, &h);
			SDL_Rect dest_rect = SDL_Rect{ render_width - 40 - w, 30, w, h };
			SDL_RenderCopy(ren, gold_and_xp, NULL, &dest_rect);
			SDL_DestroyTexture(gold_and_xp);

			// Player health bar
			SDL_SetTextureColorMod(basic_128_texture, 0, 0, 0);
			for (int i = -1; i <= ui_element_rect_player_health_bar.h; i++)
			{
				dest_rect = SDL_Rect{ ui_element_rect_player_health_bar.x + i - 2, ui_element_rect_player_health_bar.y + (ui_element_rect_player_health_bar.h - i), ui_element_rect_player_health_bar.w + 4, 1 };
				SDL_RenderCopy(ren, basic_128_texture, NULL, &dest_rect);
			}
			SDL_SetTextureColorMod(basic_128_texture, 190, 10, 10);
			int fill_width = (int)(((double)ui_element_rect_player_health_bar.w / player->GetMaxHealth()) * player->GetHealth());
			for (int i = 0; i < ui_element_rect_player_health_bar.h; i++)
			{
				dest_rect = SDL_Rect{ ui_element_rect_player_health_bar.x + i, ui_element_rect_player_health_bar.y + (ui_element_rect_player_health_bar.h - i), fill_width, 1 };
				SDL_RenderCopy(ren, basic_128_texture, NULL, &dest_rect);
			}
			SDL_SetTextureColorMod(basic_128_texture, 255, 255, 255);

			// Potions underneath health bar
			int number_of_health_potions_to_render = std::min(player->GetNumberOfHealthPotions(), kNumberOfHealthPotionsShown);
			if (number_of_health_potions_to_render > 0)
			{
				for (int i = 0; i < number_of_health_potions_to_render; i++)
				{
					dest_rect = SDL_Rect{ (int)ui_icon_health_potion->GetX() + i * (ui_icon_health_potion->GetWidth() - 4), (int)ui_icon_health_potion->GetY(), ui_icon_health_potion->GetWidth(), ui_icon_health_potion->GetHeight() };
					SDL_RenderCopy(ren, ui_icon_health_potion->GetSpriteAnimation()->GetSpriteSheet(), &ui_icon_health_potion->GetSpriteAnimation()->GetAnimationFrame(ui_icon_health_potion->GetAnimationPosition()), &dest_rect);
				}
			}

			if (AnyDialogOpen())
			{
				for (std::vector<UIDialog*>::iterator it = open_dialogs.begin(); it < open_dialogs.end(); it++)
				{
					if (*it == nullptr)
					{
						BOOST_LOG_TRIVIAL(error) << "An open dialog is a null pointer!";
					}
					else
					{
						(*it)->Render(ren, basic_128_texture);
					}
				}
			}

			// Draw Game Over overlay
			if (!player->IsAlive())
			{
				SDL_SetTextureColorMod(basic_128_texture, 0, 0, 0);
				SDL_SetTextureAlphaMod(basic_128_texture, 128);
				SDL_RenderCopy(ren, basic_128_texture, NULL, NULL);
				SDL_SetTextureColorMod(basic_128_texture, 255, 255, 255);
				SDL_SetTextureAlphaMod(basic_128_texture, 255);

				SDL_Texture* game_over_texture = CreateTextureForString(ren, *ttffont_anglican_60, "GAME OVER", SDL_Color{ 255,40,40,255 }, render_width);
				int w, h;
				SDL_QueryTexture(game_over_texture, NULL, NULL, &w, &h);
				SDL_Rect dest_rect = SDL_Rect{ (render_width / 2) - (w / 2), (render_height / 2) - (h / 2), w, h };
				SDL_RenderCopy(ren, game_over_texture, NULL, &dest_rect);
				SDL_DestroyTexture(game_over_texture);
			}

			break;
		}
		case(gamestate_village) :
		{
			// Draw the village map
			village_map->Render(ren, village_map->GetWidthInPixels()/2, village_map->GetHeightInPixels()/2);

			// Render buildings
			for (std::vector<Building*>::iterator it = buildings.begin(); it < buildings.end(); it++)
			{
				Building* building = *it;
				building->StepForwardAnimation();
				SDL_Rect dest_rect = SDL_Rect{ (building->GetTileLocationRect().x * kVillageMapTileSize) + kVillageMapLeftMargin,
					(building->GetTileLocationRect().y * kVillageMapTileSize) + kVillageMapTopMargin,
					building->GetTileLocationRect().w * kVillageMapTileSize,
					building->GetTileLocationRect().h * kVillageMapTileSize };
				SDL_RenderCopy(ren, building->GetSpriteAnimation()->GetSpriteSheet(), &building->GetSpriteAnimation()->GetAnimationFrame(building->GetCurrentAnimationFrameIndex()), &dest_rect);
			}

			// Draw NPCs
			SDL_RendererFlip npc_flip = SDL_FLIP_NONE;
			std::vector<Npc*> npcs = village_map->GetNpcs();
			for (std::vector<Npc*>::iterator it = npcs.begin(); it < npcs.end(); it++)
			{
				SDL_Rect npc_rect;
				npc_rect.x = (int)(*it)->GetX() + ((render_width / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetX());
				npc_rect.y = (int)(*it)->GetY() + ((render_height / 2) - (MAP_TILE_SIZE / 2) - (int)player->GetY());
				npc_rect.w = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).w;
				npc_rect.h = (*it)->GetSpriteAnimation()->GetAnimationFrame(0).h;
				if ((*it)->GetIfFacingRight())
				{
					npc_flip = SDL_FLIP_NONE;
				}
				else
				{
					npc_flip = SDL_FLIP_HORIZONTAL;
				}
				SDL_RenderCopyEx(ren, (*it)->GetSpriteAnimation()->GetSpriteSheet(),
					&(*it)->GetSpriteAnimation()->GetAnimationFrame((*it)->GetAnimationPosition()), &npc_rect,
					0, NULL, npc_flip);
			}

			// Render current menu...
			//...Render the title...
			int w, h;
			SDL_QueryTexture(current_town_menu->GetTitleTexture(), NULL, NULL, &w, &h);
			SDL_Rect dest_rect = SDL_Rect{ kVillageMenuLeftMargin + 16, 32, w, h };
			SDL_RenderCopy(ren, current_town_menu->GetTitleTexture(), NULL, &dest_rect);

			//...Then render the menu options
			std::vector<TownMenuOption>* current_menu_options = current_town_menu->GetOptions();
			int button_width = ui_element_rect_button.w;
			int button_height = ui_element_rect_button.h;
			int button_spacing = (int)(button_height * 1.5);
			for (int i = 0; i < (int)current_menu_options->size(); i++)
			{
				SDL_Rect button_position = SDL_Rect{ kVillageMenuLeftMargin, kVillageMenuTopMargin + (i * button_spacing), button_width, button_height };
				if (i == current_town_menu_selection && !AnyDialogOpen())
				{
					SDL_SetTextureColorMod(spritesheet_ui, 255, 255, 255);
				}
				else
				{
					SDL_SetTextureColorMod(spritesheet_ui, 80, 80, 80);
				}
				SDL_RenderCopy(ren, spritesheet_ui, &ui_element_rect_button, &button_position);
				SDL_SetTextureColorMod(spritesheet_ui, 255, 255, 255);
				SDL_QueryTexture(current_menu_options->at(i).GetTitleTexture(), NULL, NULL, &w, &h);
				dest_rect = SDL_Rect{ kVillageMenuLeftMargin + (button_width / 2) - (w / 2), kVillageMenuTopMargin + (i * button_spacing) + (button_height / 2) - (h / 2), w, h };
				//BOOST_LOG_TRIVIAL(debug) << "Drawing button label at (" << dest_rect.x << "," << dest_rect.y << ") with width " << dest_rect.w << " and height " << dest_rect.h;
				SDL_RenderCopy(ren, current_menu_options->at(i).GetTitleTexture(), NULL, &dest_rect);
			}

			if (AnyDialogOpen())
			{
				for (std::vector<UIDialog*>::iterator it = open_dialogs.begin(); it < open_dialogs.end(); it++)
				{
					if (*it == nullptr)
					{
						BOOST_LOG_TRIVIAL(error) << "An open dialog is a null pointer!";
					}
					else
					{
						(*it)->Render(ren, basic_128_texture);
					}
				}
			}

			break;
		}
		default:
		{
			BOOST_LOG_TRIVIAL(error) << "Rendering defaulted on current_game_state when it should always be defined. Value is: " << current_game_state;
		}
	}

	// Switch to window render and render the statically sized texture to the required window size
	SDL_SetRenderTarget(ren, NULL);
	SDL_SetRenderDrawColor(ren, 15, 15, 15, 255);
	SDL_RenderClear(ren);
	SDL_RenderCopy(ren, static_sized_texture, NULL, &display_rect);

	// Update the screen
	SDL_RenderPresent(ren);
}

void Game::AddMerchantToVillage()
{
	if (is_merchant_in_village)
	{
		BOOST_LOG_TRIVIAL(debug) << "Not adding merchant to village as they are already there!";
	}
	else
	{
		BOOST_LOG_TRIVIAL(debug) << "Adding merchant to village";
		is_merchant_in_village = true;
		buildings.push_back(new Building(SDL_Rect{ kMerchantBuildingLeft, kMerchantBuildingTop, 2, 2 }, sprite_animation_building_merchant_hut));
		Npc* merchant_npc = new Npc("Merchant", -444, -146);
		merchant_npc->SetSpriteAnimation(sprite_animation_npc_male1_merchant);
		village_map->AddNpc(merchant_npc);
	}
}

void Game::AddBankerToVillage()
{
	if (is_banker_in_village)
	{
		BOOST_LOG_TRIVIAL(debug) << "Not adding banker to village as they are already there!";
	}
	else
	{
		BOOST_LOG_TRIVIAL(debug) << "Adding banker to village";
		is_banker_in_village = true;
		buildings.push_back(new Building(SDL_Rect{ kBankerBuildingLeft, kBankerBuildingTop, 2, 2 }, sprite_animation_building_banker_stall));
		Npc* banker_npc = new Npc("Banker", kBankerBuildingLeft * 32 + 24, kBankerBuildingTop * 32 + 56);
		banker_npc->SetSpriteAnimation(sprite_animation_npc_banker_facing_right);
		village_map->AddNpc(banker_npc);
	}
}

void Game::FillMerchantInventory()
{
	merchant_items.clear();

	DropTable* merchant_inventory_droptable = new DropTable();
	merchant_inventory_droptable->AddDropChance(0.34, drop_item_potion);
	merchant_inventory_droptable->AddDropChance(0.34, drop_item_armour_random);
	merchant_inventory_droptable->AddDropChance(0.34, drop_item_weapon_random);

	double random_chance = RandomDoubleZeroToOne();
	DropItemType inventory_item_type;

	for (int i = 0; i < kMerchantItemsMax; i++)
	{
		random_chance = RandomDoubleZeroToOne();
		inventory_item_type = merchant_inventory_droptable->GetDropType(random_chance);
		switch (inventory_item_type)
		{
		    case drop_item_potion:
				merchant_items.push_back((std::shared_ptr<Item>)new PotionItem(potion_type_healing));
			    break;
			case drop_item_armour_random:
				merchant_items.push_back(item_generator->GenerateRandomArmour(merchant_level));
				break;
			case drop_item_weapon_random:
				merchant_items.push_back(item_generator->GenerateRandomWeapon(merchant_level));
				break;
		    default:
			    BOOST_LOG_TRIVIAL(warning) << "Defaulted on type of item (it was '" << inventory_item_type << "') to add to merchant inventory. Defaulting to a health potion.";
			    merchant_items.push_back(std::make_shared<PotionItem>(potion_type_healing));
		}
	}
	delete(merchant_inventory_droptable);
}

int Game::CostForNextMerchantLevel()
{
	return (int)round(pow(100 * merchant_level, 1.02));
}

void Game::PrepareDungeonMap()
{
	BOOST_LOG_TRIVIAL(debug) << "Preparing dungeon map...";
	map_display_centre_pixel_x = (current_dungeon_map->GetStartX()) - (MAP_TILE_SIZE / 2);
	map_display_centre_pixel_y = (current_dungeon_map->GetStartY()) - (MAP_TILE_SIZE / 2);
	player->SetX((current_dungeon_map->GetStartX()) - (MAP_TILE_SIZE / 2));
	player->SetY((current_dungeon_map->GetStartY()) - (MAP_TILE_SIZE / 2));

	projectiles.clear();

	// Prepare objects in map
	current_dungeon_map->ClearEnemies();
	current_dungeon_map->ClearDroppedItems();
	//Get enemy generators from map and set their sprite animation and max health
	std::vector<EnemyGenerator*> enemy_generators = current_dungeon_map->GetEnemyGenerators();
	for (std::vector<EnemyGenerator*>::iterator it = enemy_generators.begin(); it < enemy_generators.end(); it++)
	{
		switch ((*it)->GetEnemyGeneratorType())
		{
		case yurt:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_yurt);
			break;
		case tree:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_tree);
			break;
		case stump:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_stump);
			break;
		case bonepile:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_bonepile);
			break;
		case grave:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_grave);
			break;
		case hole:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_hole);
			break;
		case rock_mound_small:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_rockmound_small);
			break;
		case rock_mound:
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_rockmound);
			break;
		default:
			BOOST_LOG_TRIVIAL(warning) << "Defaulted on enemy generator type (" << (*it)->GetEnemyGeneratorType() << "), giving it the yurt sprite animation";
			(*it)->SetSpriteAnimation(sprite_animation_enemy_generator_yurt);
		}

		// Set health for enemy generators on the map
		(*it)->SetMaxHealth(10 * current_quest->GetBaseLevel());
		(*it)->SetHealth((*it)->GetMaxHealth());
	}

	//Get clutter from map and set sprite animations
	std::vector<std::shared_ptr<ClutterItem>> clutter_items = current_dungeon_map->GetClutter();
	for (std::vector<std::shared_ptr<ClutterItem>>::iterator it = clutter_items.begin(); it < clutter_items.end(); it++)
	{
		switch ((*it)->GetClutterType())
		{
		case small_vase:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_vase_small);
			break;
		case big_vase:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_vase_big);
			break;
		case barrel:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_barrel);
			break;
		case small_crate:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_crate_small);
			break;
		case medium_crate:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_crate_medium);
			break;
		case small_funerary_vase:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_funerary_vase_small);
			break;
		case big_funerary_vase:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_funerary_vase_big);
			break;
		case coffin:
			(*it)->SetSpriteAnimation(sprite_animation_clutter_coffin);
			break;
		default:
			BOOST_LOG_TRIVIAL(warning) << "Defaulted on clutter type " << (*it)->GetClutterType() << ", giving it the medium crate sprite animation";
			(*it)->SetSpriteAnimation(sprite_animation_clutter_crate_medium);
		}
	}

	current_dungeon_map->ClearFurniture();
	current_dungeon_map->AddFurniture(new Furniture(current_dungeon_map->GetExitX(), current_dungeon_map->GetExitY(), 32, 32, sprite_animation_furniture_map_exit));
	current_dungeon_map->ClearNpcs();

	if (current_dungeon_map->GetDungeonMapType() == current_quest->GetDestination())
	{
		if (current_quest->GetQuestItemId() != quest_item_none)
		{
			current_dungeon_map->AddDroppedItem(QuestItemFromQuestItemId(current_quest->GetQuestItemId(), current_quest->GetTargetX(), current_quest->GetTargetY(), sprite_animation_item_sparkle));
		}

		// Quest line specifics
		if (current_quest->GetQuestLineType() == merchant_questline)
		{
			if (current_quest->GetQuestLineId() == 0)
			{
				BOOST_LOG_TRIVIAL(debug) << "Putting Fergal on the map";
				Npc* fergal = new Npc("Fergal", current_quest->GetTargetX(), current_quest->GetTargetY());
				fergal->SetSpriteAnimation(sprite_animation_npc_male1_waving);
				current_dungeon_map->AddNpc(fergal);
			}
		}
		if (current_quest->GetQuestLineType() == banker_questline)
		{
			if (current_quest->GetQuestLineId() == 1)
			{
				BOOST_LOG_TRIVIAL(debug) << "Putting wounded banker on the map";
				Npc* wounded_banker = new Npc("Wounded Stranger", current_quest->GetTargetX(), current_quest->GetTargetY());
				wounded_banker->SetSpriteAnimation(sprite_animation_npc_banker_injured);
				current_dungeon_map->AddNpc(wounded_banker);
			}
		}
	}
	BOOST_LOG_TRIVIAL(debug) << "Finished preparing dungeon map.";
}

// Changes needed for a game over. Does not include saving changes
void Game::GameOver()
{
	delete(player);
	player = new Player();
}