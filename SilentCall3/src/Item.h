#include <string>
#include "../common.cpp"

#pragma once

/*! \file Item.h
* A class representing an item */
class Item
{
public:
	Item();
	Item(ItemType item_type);
	~Item();

	ItemType GetItemType();

	QuestItem GetQuestItemId();
	void SetQuestItemId(QuestItem quest_item);

	std::string GetName();
	void SetName(std::string name);

	int GetDescriptionId();
	void SetDescriptionId(int id);

	double GetValue();
	void SetValue(double v);

protected:
	ItemType item_type;
	std::string name = "NO ITEM NAME";
	int description_id = 0;

private:
	QuestItem quest_item = quest_item_none;
	double value = 1;
};

