#include "Actor.h"

Actor::Actor()
{
}

Actor::~Actor()
{
}

double Actor::GetX()
{
	return this->x;
}

void  Actor::SetX(double x)
{
	this->x = x;
}

double  Actor::GetY()
{
	return this->y;
}

void  Actor::SetY(double y)
{
	this->y = y;
}

SpriteAnimation* Actor::GetSpriteAnimation()
{
	return this->sprite_animation;
}

void Actor::SetSpriteAnimation(SpriteAnimation* sa)
{
	this->sprite_animation = sa;
}

int Actor::GetAnimationPosition()
{
	return this->animation_position;
}

void Actor::SetAnimationPosition(int position)
{
	this->animation_position = position;
}

int Actor::GetWidth()
{
	return this->width;
}

int Actor::GetHeight()
{
	return this->height;
}

void Actor::SetWidth(int w)
{
	this->width = w;
}

void Actor::SetHeight(int h)
{
	this->height = h;
}

double Actor::GetHealth()
{
	return this->health;
}

bool Actor::SetHealth(double health)
{
	this->health = fmax(0, health);
	if (this->health == 0 && alive)
	{
		alive = false;
		return true;
	}
	return false;
}

double Actor::GetMaxHealth()
{
	return this->max_health;
}

void Actor::SetMaxHealth(double health)
{
	this->max_health = health;
	this->health = fmin(this->health, this->max_health);
}

bool Actor::ApplyDamage(double damage)
{
	this->health = fmax(0, this->health - damage);
	if (this->health == 0 && alive)
	{
		alive = false;
		return true;
	}
	else {
		current_invincibility_frames = total_invincibility_frames;
		return false;
	}
}

bool Actor::IsAlive()
{
	return this->alive;
}

bool Actor::IsInInvincibilityFrames()
{
	return this->current_invincibility_frames > 0;
}

bool Actor::GetIfFacingRight()
{
	return this->facing_right;
}

void Actor::SetIfFacingRight(bool right)
{
	this->facing_right = right;
}

void Actor::ActorUpdate()
{
	animation_position++;
	if (animation_position >= sprite_animation->GetAnimationLength())
	{
		animation_position = 0;
	}

	if (current_invincibility_frames > 0) current_invincibility_frames--;
}


