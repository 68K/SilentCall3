#include "Npc.h"

Npc::Npc()
{
}

Npc::Npc(std::string title, double x, double y)
	: Actor()
{
	this->title = title;
	this->SetX(x);
	this->SetY(y);
}

Npc::~Npc()
{
}

std::string Npc::GetTitle()
{
	return this->title;
}

void Npc::SetTitle(std::string title)
{
	this->title = title;
}

double Npc::GetMovementSpeed()
{
	return this->movement_speed;
}

void Npc::SetMovementSpeed(double speed)
{
	this->movement_speed = speed;
}

bool Npc::GetIfMoving()
{
	return moving;
}

void Npc::SetIfMoving(bool moving)
{
	this->moving = moving;
}

EnemyMovementState Npc::GetMovementState()
{
	return movement_state;
}

void Npc::SetMovementState(EnemyMovementState state)
{
	this->movement_state = state;
}

std::pair<int, int>* Npc::GetPointOfInterest()
{
	return point_of_interest;
}

void Npc::SetPointOfInterest(int x, int y)
{
	delete(point_of_interest);
	point_of_interest = new std::pair<int, int>(x, y);
}

double Npc::GetMeleeDamage()
{
	return this->melee_damage;
}

void Npc::SetMeleeDamage(double damage)
{
	this->melee_damage = damage;
}

double Npc::GetProjectileDamage()
{
	return this->projectile_damage;
}

void Npc::SetProjectileDamage(double damage)
{
	this->projectile_damage = damage;
}

int Npc::GetLevel()
{
	return this->level;
}

void Npc::SetLevel(int level)
{
	this->level = level;
}

int Npc::GetGold()
{
	return gold;
}

void Npc::SetGold(int gold)
{
	this->gold = gold;
}

int Npc::GetXP()
{
	return xp;
}

void Npc::SetXP(int xp)
{
	this->xp = xp;
}

void Npc::Update()
{
	ActorUpdate();
}
