#include "../../common.cpp"
#include "QuestSpec.h"

#pragma once
/*! \file QuestLine.h
* A class representing a series of quests (each a QuestSpec specification)
* Also records whether each quest has been completed */
class QuestLine
{
public:
	QuestLine();
	~QuestLine();
	QuestLine(QuestLineType quest_line_type);

	void AddQuestSpec(QuestSpec* spec);

	/* \brief  Get next quest available for this questline, based on progress in current questlines
	 * @param  questline_progress    A map from questline types to the highest quest completed in that line.
	 * @return QuestSpec*            quest spec of the next quest in this questline that has requirements satisfied
	 * @return nullptr               if no quests in this questline satisfy the current progress 
	 */
	QuestSpec* GetNextAvailableQuest(std::map<QuestLineType, int> questline_progress);

private:
	QuestLineType quest_line_type;
	int max_quest_completed = -1;
	std::vector<QuestSpec*> quest_specs;
};

