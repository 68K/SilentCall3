#include "QuestSpec.h"

QuestSpec::QuestSpec()
{
}


QuestSpec::~QuestSpec()
{
}

QuestSpec::QuestSpec(QuestLineType quest_line_type, int id, std::string title, std::pair<QuestLineType, int>* questline_requirement,
	                 std::string description, DungeonMapType map, int base_level, QuestCompletionCriteriaType completion_criteria_type,
	                 DungeonMapType target_map, int quest_target_x, int quest_target_y, QuestItem quest_item_id)
{
	this->quest_line_type = quest_line_type;
	this->id = id;
	this->quest_title = title;
	this->questline_requirement = questline_requirement;
	this->description = description;
	this->map = map;
	this->base_level = base_level;
	this->completion_criteria_type = completion_criteria_type;
	this->target_map = target_map;
	this->quest_target_x = quest_target_x;
	this->quest_target_y = quest_target_y;
	this->quest_item_id = quest_item_id;
}

QuestLineType QuestSpec::GetQuestLineType()
{
	return quest_line_type;
}

int QuestSpec::GetId()
{
	return id;
}

std::string QuestSpec::GetTitle()
{
	return quest_title;
}

std::string QuestSpec::GetDescription()
{
	return description;
}

DungeonMapType QuestSpec::GetMapType()
{
	return map;
}

int QuestSpec::GetBaseLevel()
{
	return base_level;
}

std::pair<QuestLineType, int>* QuestSpec::GetQuestlineRequirement()
{
	return questline_requirement;
}

QuestCompletionCriteriaType QuestSpec::GetCompletionCriteriaType()
{
	return completion_criteria_type;
}

DungeonMapType QuestSpec::GetCompletionTargetMap()
{
	return target_map;
}

int QuestSpec::GetCompletionTargetX()
{
	return quest_target_x;
}

int QuestSpec::GetCompletionTargetY()
{
	return quest_target_y;
}

QuestItem QuestSpec::GetQuestItemId()
{
	return quest_item_id;
}