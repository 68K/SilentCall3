#include "QuestLine.h"



QuestLine::QuestLine()
{
}


QuestLine::~QuestLine()
{
}


QuestLine::QuestLine(QuestLineType quest_line_type)
{
	this->quest_line_type = quest_line_type;
}

void QuestLine::AddQuestSpec(QuestSpec* spec)
{
	quest_specs.push_back(spec);
}

QuestSpec* QuestLine::GetNextAvailableQuest(std::map<QuestLineType, int> questline_progress)
{
	for (std::vector<QuestSpec*>::iterator it = quest_specs.begin(); it != quest_specs.end(); it++)
	{
		// If the player isn't already past this quest...
		if (questline_progress[quest_line_type] <= (*it)->GetId())
		{
			// ...and meets the requirements for it...
			if (questline_progress[(*it)->GetQuestlineRequirement()->first] > (*it)->GetQuestlineRequirement()->second )
			{
				return *it;
			}
    	}
	}
	return nullptr;
}