#include "../../common.cpp"
#include "../../Quest.h"

#pragma once
/*! \file QuestSpec.h
* A class representing the outline and requirements for a set quest.
* When the requirements are met during gameplay, a Quest can be generated from this spec */
class QuestSpec
{
public:
	QuestSpec();
	~QuestSpec();
	QuestSpec(QuestLineType quest_line_type, int id, std::string title, std::pair<QuestLineType, int>* questline_requirement, 
		      std::string description, DungeonMapType map, int base_level, QuestCompletionCriteriaType completion_criteria_type,
		      DungeonMapType target_map, int quest_target_x, int quest_target_y, QuestItem quest_item_id);

	QuestLineType GetQuestLineType();
	int GetId();
	std::string GetTitle();
	std::string GetDescription();
	DungeonMapType GetMapType();
	int GetBaseLevel();
	QuestCompletionCriteriaType GetCompletionCriteriaType();
	DungeonMapType GetCompletionTargetMap();
	int GetCompletionTargetX();
	int GetCompletionTargetY();
	QuestItem GetQuestItemId();

	std::pair<QuestLineType, int>* GetQuestlineRequirement();

private:
	int id = 0;
	QuestLineType quest_line_type; //Quest line this quest is on. NOTE: Do not assume it's on same questline as its requirement
	std::string quest_title;
	std::string description;
	DungeonMapType map;
	int base_level;
	std::pair<QuestLineType, int>* questline_requirement;

	QuestCompletionCriteriaType completion_criteria_type = defeat_an_enemy;
	DungeonMapType target_map;
	int quest_target_x = 0;
	int quest_target_y = 0;
	// TODO: Need 'enemy spec' and 'item spec' classes to describe enemies or items to generate for this quest
	QuestItem quest_item_id = quest_item_none;
};

