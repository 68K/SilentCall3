#pragma once
#include "Actor.h"

class ClutterItem :
	public Actor
{
public:
	ClutterItem(ClutterType type);
	~ClutterItem();

	ClutterType GetClutterType();
private:
	ClutterType type;
};

