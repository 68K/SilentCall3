#include "BestiaryEntry.h"

BestiaryEntry::BestiaryEntry()
{
}

BestiaryEntry::BestiaryEntry(int bestiary_id, EnemyType type, EnemyGeneratorType generator_type, DropTableType drop_table,
	                         std::string title, int base_level, int base_experience,
	                         double health, int width, int height, double melee_damage, double projectile_damage,
	                         int alarm_duration)
{
	this->bestiary_id = bestiary_id;
	this->type = type;
	this->generator_type = generator_type;
	this->drop_table = drop_table;
	this->title = title;
	this->base_level = base_level;
	this->base_experience = base_experience;
	this->health = health;
	this->width = width;
	this->height = height;
	this->melee_damage = melee_damage;
	this->projectile_damage = projectile_damage;
	this->alarm_duration = alarm_duration;
}

BestiaryEntry::~BestiaryEntry()
{
}