#include "Item.h"



Item::Item()
{
}

Item::Item(ItemType item_type)
{
	this->item_type = item_type;
}

Item::~Item()
{
}

ItemType Item::GetItemType()
{
	return this->item_type;
}

QuestItem Item::GetQuestItemId()
{
	return this->quest_item;
}

void Item::SetQuestItemId(QuestItem quest_item)
{
	this->quest_item = quest_item;
}

std::string Item::GetName()
{
	return this->name;
}

void Item::SetName(std::string name)
{
	this->name = name;
}

int Item::GetDescriptionId()
{
	return this->description_id;
}

void Item::SetDescriptionId(int id)
{
	this->description_id = id;
}

double Item::GetValue()
{
	return this->value;
}

void Item::SetValue(double v)
{
	this->value = v;
}