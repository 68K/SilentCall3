#include "ArmourItem.h"
#include "WeaponItem.h"
#include <utility>  

#pragma once
class ItemGenerator
{
public:
	ItemGenerator();
	~ItemGenerator();

	std::shared_ptr<ArmourItem> GenerateRandomArmour(int level);

	std::shared_ptr<WeaponItem> GenerateRandomWeapon(int level);
	std::shared_ptr<WeaponItem> GenerateRandomWeapon(int level, WeaponType weapon_type);

private:

	std::pair<int, std::string> GetRandomWord(int level, std::map<int, std::string> word_set);

	std::map<int, std::string> quality_adjectives;
	std::map<int, std::string> armour_names;
	std::map<int, std::string> sword_names;
	std::map<int, std::string> shield_names;
	std::map<int, std::string> staff_names;
	std::map<int, std::string> axe_names;
	std::map<int, std::string> spear_names;
	std::map<int, std::string> club_names;
	std::map<int, std::string> bow_names;

	std::pair<int, std::string> GetRandomQualityAdjective(int level);
	std::pair<int, std::string> GetRandomArmourType(int level);
	std::string CreateRandomArmourName(int level);
	
	std::pair<int, std::string> PickRandomWeaponBaseName(int level, WeaponType weapon_type);
	std::string CreateRandomWeaponName(int level, WeaponType weapon_type);

};

