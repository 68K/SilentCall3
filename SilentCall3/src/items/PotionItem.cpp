#include "PotionItem.h"

PotionItem::PotionItem()
{
	this->item_type = item_type_potion;
}

PotionItem::PotionItem(PotionType type)
{
	this->type = type;
	this->item_type = item_type_potion;
	this->SetValue(20);
	switch (type)
	{
	   case potion_type_healing:
		   this->name = "Health Potion";
		   this->description_id = 2;
		   break;
	   default:
		   this->type = potion_type_healing;
		   this->name = "Health Potion";
		   this->description_id = 1;
		   break;
	}
}

PotionItem::~PotionItem()
{
}

PotionType PotionItem::GetPotionType()
{
	return this->type;
}
