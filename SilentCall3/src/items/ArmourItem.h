#include "../Item.h"

#pragma once
class ArmourItem:
	public Item
{
public:
	ArmourItem();
	~ArmourItem();

	int GetArmourLevel();
	void SetArmourLevel(int armour_level);

private:
	int armour_level = 1;
};

