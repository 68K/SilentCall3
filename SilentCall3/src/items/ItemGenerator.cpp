#include "ItemGenerator.h"

ItemGenerator::ItemGenerator()
{
	quality_adjectives[1] = "Poor";
	quality_adjectives[3] = "Weak";
	quality_adjectives[4] = "Rubbish";
	quality_adjectives[5] = "Defective";
	quality_adjectives[6] = "Inferior";
	quality_adjectives[7] = "Shabby";
	quality_adjectives[8] = "Impaired";
	quality_adjectives[9] = "Marred";
	quality_adjectives[10] = "Lowly";
	quality_adjectives[11] = "Paltry";
	quality_adjectives[12] = "Mediocre";
	quality_adjectives[13] = "Humble";
	quality_adjectives[14] = "Feeble";
	quality_adjectives[15] = "Modest";
	quality_adjectives[16] = "Meager";
	quality_adjectives[17] = "Ordinary";
	quality_adjectives[18] = "Middling";
	quality_adjectives[19] = "Fair";
	quality_adjectives[20] = "Second-Rate";
	quality_adjectives[21] = "Average";
	quality_adjectives[22] = "Common";
	quality_adjectives[23] = "So-So";
	quality_adjectives[24] = "Simple";
	quality_adjectives[25] = "Adequate";
	quality_adjectives[26] = "Moderate";
	quality_adjectives[27] = "Decent";
	quality_adjectives[28] = "Standard";
	quality_adjectives[29] = "Worthy";
	quality_adjectives[30] = "Dependable";
	quality_adjectives[31] = "Solid";
	quality_adjectives[32] = "Dandy";
	quality_adjectives[33] = "Fine";
	quality_adjectives[34] = "Great";
	quality_adjectives[35] = "Superb";
	quality_adjectives[36] = "Remarkable";
	quality_adjectives[37] = "Rare";
	quality_adjectives[38] = "Superior";
	quality_adjectives[39] = "Exceptional";
	quality_adjectives[40] = "Peerless";
	quality_adjectives[41] = "Excellent";
	quality_adjectives[42] = "Magnificent";
	quality_adjectives[43] = "Brilliant";
	quality_adjectives[44] = "Supreme";
	quality_adjectives[45] = "Legendary";
	quality_adjectives[46] = "Ultimate";
	quality_adjectives[47] = "Almighty";

	armour_names[0] = "Leather Brigandine";
	armour_names[10] = "Leather Cuirass";
	armour_names[20] = "Iron Brigandine";
	armour_names[30] = "Boiled Leather Brigandine";
	armour_names[40] = "Boiled Leather Cuirass";
	armour_names[50] = "Boiled Iron Brigandine";
	armour_names[60] = "Hardened Leather Brigandine";
	armour_names[70] = "Hardened Leather Cuirass";
	armour_names[80] = "Hardened Iron Brigandine";
	armour_names[90] = "Iron Hauberk";
	armour_names[100] = "Iron Cuirass";
	armour_names[110] = "Bronze Brigandine";
	armour_names[120] = "Bronze Hauberk";
	armour_names[130] = "Bronze Cuirass";
	armour_names[140] = "Steel Brigandine";
	armour_names[150] = "Steel Hauberk";
	armour_names[200] = "Steel Cuirass";
	armour_names[300] = "Dwarven Armour";
	armour_names[400] = "Chainmail Armour";
	armour_names[500] = "Splintmail Armour";
	armour_names[600] = "Elven Chainmail";
	armour_names[700] = "Plate Armour";
	armour_names[800] = "Carburised Cuirass";
	armour_names[900] = "Dwarven Plate";
	armour_names[1000] = "Mycenaean Plate";
	armour_names[1100] = "Royal Chainmail";
	armour_names[1300] = "Daemon Armour";
	armour_names[1400] = "Dragonscale";
	armour_names[1500] = "Lord's Armour";

	sword_names[0] = "Sword";

	shield_names[0] = "Small Shield";
	shield_names[5] = "Buckler";
	shield_names[10] = "Shield";

	staff_names[0] = "Wooden Staff";

	axe_names[0] = "Small Axe";

	spear_names[0] = "Spear";

	club_names[0] = "Rough Club";

	bow_names[0] = "Small Bow";
}

ItemGenerator::~ItemGenerator()
{
}

std::shared_ptr<ArmourItem> ItemGenerator::GenerateRandomArmour(int level)
{
	std::shared_ptr<ArmourItem> result = std::make_shared<ArmourItem> ();
	result->SetArmourLevel(level);
	result->SetName(CreateRandomArmourName(level));
	result->SetValue(10 * level);
	result->SetDescriptionId(12);
	return result;
}

std::shared_ptr<WeaponItem> ItemGenerator::GenerateRandomWeapon(int level)
{
	WeaponType random_weapon_type = (WeaponType)(rand() % weapon_type_MAX);
	return this->GenerateRandomWeapon(level, random_weapon_type);
}

std::shared_ptr<WeaponItem> ItemGenerator::GenerateRandomWeapon(int level, WeaponType weapon_type)
{
	std::shared_ptr<WeaponItem> result = std::make_shared<WeaponItem> (weapon_type);
	if (result->GetWeaponType() == weapon_type_shield)
	{
		result->SetDefenseDamageThreshold(level);
	}
	else
	{
		result->SetAttackLevel(level);
	}
	result->SetName(this->CreateRandomWeaponName(level, weapon_type));
	result->SetValue(10 * level);
	switch (weapon_type)
	{
		case weapon_type_axe:
			result->SetDescriptionId(8);
			break;
		case weapon_type_bow:
			result->SetDescriptionId(6);
			break;
		case weapon_type_club:
			result->SetDescriptionId(7);
			break;
		case weapon_type_shield:
			result->SetDescriptionId(10);
			break;
		case weapon_type_spear:
			result->SetDescriptionId(11);
			break;
		case weapon_type_staff:
			result->SetDescriptionId(9);
			break;
		case weapon_type_sword:
			result->SetDescriptionId(5);
			break;
	}
	return result;
}

std::pair<int, std::string> ItemGenerator::GetRandomWord(int level, std::map<int, std::string> word_set)
{
	std::vector<std::pair<int, std::string>> possible_words;
	for (std::map<int, std::string>::iterator it = word_set.begin(); it != word_set.end(); it++)
	{
		if (it->first <= level)
		{
			possible_words.push_back(*it);
		}
	}
	int random_pick = rand() % possible_words.size();
	std::pair<int, std::string> result = possible_words.at(random_pick);
	return std::make_pair(level - result.first, result.second);
}

std::string ItemGenerator::CreateRandomArmourName(int level)
{
	int remainder = level;
	std::string result = "";
	// Pick an armour
	std::pair<int, std::string> armour_type_with_remainder = GetRandomArmourType(remainder);
	result = armour_type_with_remainder.second;
	remainder = remainder - armour_type_with_remainder.first;
	if (remainder > 0)
	{
		// Pick an adjective
		std::pair<int, std::string> adjective_with_remainder = GetRandomQualityAdjective(remainder);
		result = adjective_with_remainder.second + " " + result;
		remainder = remainder - adjective_with_remainder.first;
		// Make up the remainder with a + number
		if (remainder > 0)
		{
			result = "+" + std::to_string(remainder) + " " + result;
		}
	}
	return result;
}

std::pair<int, std::string> ItemGenerator::GetRandomQualityAdjective(int level)
{
	return GetRandomWord(level, quality_adjectives);
}

std::pair<int, std::string> ItemGenerator::GetRandomArmourType(int level)
{
	return GetRandomWord(level, armour_names);
}

std::pair<int, std::string> ItemGenerator::PickRandomWeaponBaseName(int level, WeaponType weapon_type)
{
	std::vector<std::pair<int, std::string>> possible_weapon_base_names;
	std::map<int, std::string> weapon_base_names;
	switch (weapon_type)
	{
		case weapon_type_axe:
			weapon_base_names = axe_names;
			break;
		case weapon_type_bow:
			weapon_base_names = bow_names;
			break;
		case weapon_type_club:
			weapon_base_names = club_names;
			break;
		case weapon_type_spear:
			weapon_base_names = spear_names;
			break;
		case weapon_type_staff:
			weapon_base_names = staff_names;
			break;
		case weapon_type_sword:
			weapon_base_names = sword_names;
			break;
		case weapon_type_shield:
			weapon_base_names = shield_names;
			break;
		default:
			BOOST_LOG_TRIVIAL(warning) << "GetRandomWeaponBaseName - Defaulting to a sword. No handling for weapon type " << weapon_type;
			weapon_base_names = sword_names;
	}
	return GetRandomWord(level, weapon_base_names);
}

std::string ItemGenerator::CreateRandomWeaponName(int level, WeaponType weapon_type)
{
	int remainder = level;
	std::string result = "";
	// Pick a weapon
	std::pair<int, std::string> weapon_base_with_remainder = PickRandomWeaponBaseName(remainder, weapon_type);
	result = weapon_base_with_remainder.second;
	remainder = remainder - weapon_base_with_remainder.first;
	if (remainder > 0)
	{
		// Pick an adjective
		std::pair<int, std::string> adjective_with_remainder = GetRandomQualityAdjective(remainder);
		result = adjective_with_remainder.second + " " + result;
		remainder = remainder - adjective_with_remainder.first;
		// Make up the remainder with a + number
		if (remainder > 0)
		{
			result = "+" + std::to_string(remainder) + " " + result;
		}
	}
	return result;
}