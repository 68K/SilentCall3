#include "WeaponItem.h"

WeaponItem::WeaponItem(WeaponType weapon_type)
{
	this->item_type = item_type_weapon;
	this->weapon_type = weapon_type;
}

WeaponItem::~WeaponItem()
{
}

WeaponType WeaponItem::GetWeaponType()
{
	return this->weapon_type;
}

int WeaponItem::GetAttackLevel()
{
	return this->attack_level;
}

void WeaponItem::SetAttackLevel(int attack_level)
{
	this->attack_level = attack_level;
}

bool WeaponItem::FiresProjectiles()
{
	if (this->weapon_type == weapon_type_axe || this->weapon_type == weapon_type_staff)
	{
		return false;
	}
	return true;
}

double WeaponItem::GetDefenseAngle()
{
	return this->defense_angle;
}

void WeaponItem::SetDefenseAngle(double angle)
{
	this->defense_angle = angle;
}

int WeaponItem::GetDefenseDamageThreshold()
{
	return this->defense_damage_threshold;
}

void WeaponItem::SetDefenseDamageThreshold(int dt)
{
	this->defense_damage_threshold = dt;
}

int WeaponItem::GetDefenseRecoveryPeriod()
{
	return this->defense_recovery_period;
}

void WeaponItem::SetDefenseRecoveryPeriod(int drp)
{
	this->defense_recovery_period = drp;
}

double WeaponItem::GetDefenseSpeedAdjust()
{
	return this->defense_speed_adjust;
}

void WeaponItem::SetDefenseSpeedAdjust(double dsa)
{
	this->defense_speed_adjust = dsa;
}