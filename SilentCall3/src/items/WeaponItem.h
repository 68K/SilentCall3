#pragma once
#include "..\Item.h"
class WeaponItem :
	public Item
{
public:
	WeaponItem(WeaponType weapon_type);
	~WeaponItem();

	WeaponType GetWeaponType();

	int GetAttackLevel();
	void SetAttackLevel(int attack_level);

	/*! \brief If this weapon fires projectiles. Staffs and axes do not.
	*
	* @return true if the weapon fires projectiles
	*/
	bool FiresProjectiles();

	double GetDefenseAngle();
	void SetDefenseAngle(double angle);
	int GetDefenseDamageThreshold();
	void SetDefenseDamageThreshold(int dt);
	int GetDefenseRecoveryPeriod();
	void SetDefenseRecoveryPeriod(int drp);
	double GetDefenseSpeedAdjust();
	void SetDefenseSpeedAdjust(double dsa);

private:
	WeaponType weapon_type = weapon_type_sword;
	int attack_level = 1;

	// Defense properties
	double defense_angle = 5;
	int defense_damage_threshold = 2;
	int defense_recovery_period = 20;
	double defense_speed_adjust = 0.6;
};

