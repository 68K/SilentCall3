#include "../../common.cpp"
#include "../Item.h"

#pragma once
/*! \file PotionItem.h
* A subclass of Item representing a potion */
class PotionItem :
	public Item
{
public:
	PotionItem();
	PotionItem(PotionType type);
	~PotionItem();

	PotionType GetPotionType();

private:
	PotionType type;
};

