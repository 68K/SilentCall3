#include "ArmourItem.h"

ArmourItem::ArmourItem()
{
	this->item_type = item_type_armour;
}

ArmourItem::~ArmourItem()
{
}

int ArmourItem::GetArmourLevel()
{
	return this->armour_level;
}

void ArmourItem::SetArmourLevel(int armour_level)
{
	this->armour_level = armour_level;
}