#include <SDL.h>
#include <string>
#include "../common.cpp"
#include "Npc.h"
#include "../TmxMap.h"

#pragma once
/*! \file PresentationMap.h
* A class representing a map used for presentation (not gameplay). */
class PresentationMap
{
public:
	PresentationMap();
	~PresentationMap();

	void LoadTmxMap(DungeonMapType map_type);

	void LoadTextures(SDL_Renderer *ren);

	void Render(SDL_Renderer *ren, int centre_pixel_x, int centre_pixel_y);

	std::vector<Npc*> GetNpcs();
	void AddNpc(Npc* npc);
	void ClearNpcs();

	int GetWidthInPixels();
	int GetHeightInPixels();

private:
	std::map<DungeonMapType, std::string> DungeonMapFileNames;

	TmxMap* tmx_map = nullptr;

	SDL_Texture* texture_walls;

	DungeonMapType map_type;

	int w = 0;
	int h = 0;
	int tileset_columns = 0;
	int tile_size = 0;

	std::vector<Npc*> npcs;

};

