#include "Actor.h"
#include "../common.cpp"
#include "../SpriteAnimation.h"

#pragma once
/*! \file Npc.h
* A base class representing a Non-Player Character in the game */
class Npc: public Actor
{
public:
	Npc();
	Npc(std::string title, double x, double y);
	~Npc();

	std::string GetTitle();
	void SetTitle(std::string title);

	double GetMovementSpeed();
	void SetMovementSpeed(double speed);

	bool GetIfMoving();
	void SetIfMoving(bool moving);
	EnemyMovementState GetMovementState();
	void SetMovementState(EnemyMovementState state);
	std::pair<int, int>* GetPointOfInterest();
	void SetPointOfInterest(int x, int y);

	double GetMeleeDamage();
	void SetMeleeDamage(double damage);
	double GetProjectileDamage();
	void SetProjectileDamage(double damage);

	int GetLevel();
	void SetLevel(int level);

	int GetGold();
	void SetGold(int gold);
	int GetXP();
	void SetXP(int xp);

	// Step forward sprite animation etc
	void Update();

private:

	std::string title = "UNINITIALISED NPC TITLE";

	double movement_speed = 0.8;
	bool moving = false;
	EnemyMovementState movement_state = idle;
	std::pair<int, int>* point_of_interest = nullptr;

	double melee_damage = 1;
	double projectile_damage = 1;

	int level = 1;

	int gold = 1;
	int xp = 1;
};

