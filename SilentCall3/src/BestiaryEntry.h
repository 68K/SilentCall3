#include "../common.cpp"
#include "../Enemy.h"

#pragma once

class BestiaryEntry
{
public:
	BestiaryEntry();
	BestiaryEntry(int bestiary_id, EnemyType type, EnemyGeneratorType generator_type, DropTableType drop_table,
		          std::string title, int base_level, int base_experience,
		          double health, int width, int height, double melee_damage, double projectile_damage,
		          int alarm_duration);
	~BestiaryEntry();

	// Make these all public for convenience
	int bestiary_id = 0;
	EnemyType type;
	EnemyGeneratorType generator_type; // TODO: Allow for multiple generator types
	DropTableType drop_table;
	std::string title;
	int base_level = 1;
	int base_experience = 1;
	double health;
	int width = 14;
	int height = 28;
	double melee_damage = 1;
	double projectile_damage = 1;
	int alarm_duration = 0;
};