#include "DropTable.h"

DropTable::DropTable()
{
}

DropTable::~DropTable()
{
}

void DropTable::AddDropChance(double chance, DropItemType type)
{
	drop_table[type] += chance;
	if (GetTotalChance() > 1)
	{
		double scale_down = 1 / GetTotalChance();
		for (std::map<DropItemType, double>::iterator it = drop_table.begin(); it != drop_table.end(); it++)
		{
			drop_table[it->first] = drop_table[it->first] * scale_down;
		}
	}
}

DropItemType DropTable::GetDropType(double roll)
{
	double total_checked = 0;
	for (std::map<DropItemType, double>::iterator it = drop_table.begin(); it != drop_table.end(); it++)
	{
		total_checked += drop_table[it->first];
		if (roll <= total_checked)
		{
			return it->first;
		}
	}
	return drop_item_nothing;
}

double DropTable::GetTotalChance()
{
	double result = 0;
	for (std::map<DropItemType, double>::iterator it = drop_table.begin(); it != drop_table.end(); it++)
	{
		result += drop_table[it->first];
	}
	return result;
}