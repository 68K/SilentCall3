#include "../common.cpp"
#include "../SpriteAnimation.h"

#pragma once

/*! \file Actor.h
* A class representing an actor in the game.
* Has physical properties, a SpriteAnimation and health properties */

class Actor
{
public:
	Actor();
	~Actor();

	double GetX();
	void SetX(double x);
	double GetY();
	void SetY(double y);

	SpriteAnimation* GetSpriteAnimation();
	void SetSpriteAnimation(SpriteAnimation* sa);
	int GetAnimationPosition();
	void SetAnimationPosition(int position);

	int GetWidth();
	int GetHeight();
	void SetWidth(int w);
	void SetHeight(int h);

	double GetHealth();

	/*! \brief Set enemy's health explicitly
	*
	* @param  double health the new health value
	* @return true if this update caused death
	*/
	bool SetHealth(double health);

	double GetMaxHealth();

	/*! \brief Set enemy's maximum health
	* Does not set current health to the new maximum
	*
	* @param  double health the new max health value
	* @return void
	*/
	void SetMaxHealth(double health);

	/*! \brief Apply damage to this enemy
	*
	* @param  double damage to apply to this enemy
	* @return true if this update caused death
	*/
	bool ApplyDamage(double damage);

	bool IsAlive();

	bool IsInInvincibilityFrames();

	bool GetIfFacingRight();
	void SetIfFacingRight(bool right);

	// Step forward sprite animation etc
	void ActorUpdate();

private:
	double x = 0;
	double y = 0;
	int width = 14;
	int height = 28;

	double health = 50;
	double max_health = 50;
	bool alive = true;
	int total_invincibility_frames = 10;
	int current_invincibility_frames = 0;

	SpriteAnimation* sprite_animation;
	int animation_position = 0;

	bool facing_right = true;
};

