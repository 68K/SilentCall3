#include "Actor.h"
#include "../common.cpp"
#include "Item.h"
#include "../SpriteAnimation.h"

#pragma once

/*! \file DroppedItem.h
* A class representing an item lying on the ground in a dungeon map */
class DroppedItem: public Actor
{
public:
	DroppedItem();
	DroppedItem(DropItemType type, int value, double x, double y, int w, int h, SpriteAnimation* sa);
	~DroppedItem();

	DropItemType GetType();
	int GetValue();

	std::shared_ptr<Item> GetItem();
	void SetItem(std::shared_ptr<Item> item);

	bool GetIsCollected();
	void SetIsCollected(bool collected);

	// Step forward sprite animation etc
	void Update();

private:
	DropItemType type = drop_item_gold;
	bool collected = false;
	int value = 1;
	std::shared_ptr<Item> item = nullptr;
};

