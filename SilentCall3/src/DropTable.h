#include "../common.cpp"

#pragma once

/*! \file DropTable.h
* A class for a drop table - a percentage distribution for what might drop when
* an enemy dies. May be nothing, or gold, or a generic item, or a unique item 
* This class will just return the type of item dropped. The calling class will 
* create a suitable actual item. */
class DropTable
{
public:
	DropTable();
	~DropTable();

	/*! \brief Add a chance of a drop type to this table
	* If the type already exists in the drop table; add the chance
	* If the chance pushes the total chance above 1.0; scale down
	*/
	void AddDropChance(double chance, DropItemType type);

	/*! \brief Return a type of item dropped for a given double.
	* It's assumed the calling class has chosen this double randomly.
	*/
	DropItemType GetDropType(double roll);
	
private:
	std::map<DropItemType, double> drop_table;

	double GetTotalChance();
};

