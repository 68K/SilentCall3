#include "SpriteAnimationFrames.h"



SpriteAnimationFrames::SpriteAnimationFrames()
{
}


SpriteAnimationFrames::~SpriteAnimationFrames()
{
}

void SpriteAnimationFrames::Initialise()
{
	player_facing_right.push_back({ 0,0 });

	player_moving_right.push_back({ 64,0 });
	player_moving_right.push_back({ 96,0 });
	player_moving_right.push_back({ 128,0 });
	player_moving_right.push_back({ 96,0 });

	npc_male1_waving.push_back({ 0, 256 });
	npc_male1_waving.push_back({ 0, 256 });
	npc_male1_waving.push_back({ 0, 256 });
	npc_male1_waving.push_back({ 0, 256 });
	npc_male1_waving.push_back({ 32, 256 });
	npc_male1_waving.push_back({ 64, 256 });
	npc_male1_waving.push_back({ 96, 256 });
	npc_male1_waving.push_back({ 128, 256 });
	npc_male1_waving.push_back({ 160, 256 });
	npc_male1_waving.push_back({ 128, 256 });
	npc_male1_waving.push_back({ 96, 256 });
	npc_male1_waving.push_back({ 128, 256 });
	npc_male1_waving.push_back({ 160, 256 });
	npc_male1_waving.push_back({ 128, 256 });
	npc_male1_waving.push_back({ 96, 256 });

	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 192, 256 });
	npc_male1_merchant.push_back({ 224, 256 });
	npc_male1_merchant.push_back({ 224, 256 });
	npc_male1_merchant.push_back({ 224, 256 });
	npc_male1_merchant.push_back({ 224, 256 });
	npc_male1_merchant.push_back({ 192, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 0, 256 });
	npc_male1_merchant.push_back({ 256, 256 });
	npc_male1_merchant.push_back({ 288, 256 });
	npc_male1_merchant.push_back({ 320, 256 });
	npc_male1_merchant.push_back({ 288, 256 });
	npc_male1_merchant.push_back({ 256, 256 });
	npc_male1_merchant.push_back({ 288, 256 });
	npc_male1_merchant.push_back({ 320, 256 });
	npc_male1_merchant.push_back({ 288, 256 });
	npc_male1_merchant.push_back({ 256, 256 });


	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 32, 416 });
	npc_banker_facing_right.push_back({ 64, 416 });
	npc_banker_facing_right.push_back({ 64, 416 });
	npc_banker_facing_right.push_back({ 96, 416 });
	npc_banker_facing_right.push_back({ 128, 416 });
	npc_banker_facing_right.push_back({ 160, 416 });
	npc_banker_facing_right.push_back({ 192, 416 });
	npc_banker_facing_right.push_back({ 224, 416 });
	npc_banker_facing_right.push_back({ 224, 416 });
	npc_banker_facing_right.push_back({ 224, 416 });
	npc_banker_facing_right.push_back({ 224, 416 });
	npc_banker_facing_right.push_back({ 256, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 0, 416 });
	npc_banker_facing_right.push_back({ 288, 416 });
	npc_banker_facing_right.push_back({ 320, 416 });
	npc_banker_facing_right.push_back({ 320, 416 });
	npc_banker_facing_right.push_back({ 320, 416 });
	npc_banker_facing_right.push_back({ 320, 416 });
	npc_banker_facing_right.push_back({ 288, 416 });

	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 32, 448 });
	npc_banker_injured.push_back({ 32, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 0, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 96, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 96, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 96, 448 });
	npc_banker_injured.push_back({ 64, 448 });
	npc_banker_injured.push_back({ 96, 448 });
	npc_banker_injured.push_back({ 128, 448 });
	npc_banker_injured.push_back({ 128, 448 });
	npc_banker_injured.push_back({ 160, 448 });
	npc_banker_injured.push_back({ 160, 448 });
	npc_banker_injured.push_back({ 192, 448 });
	npc_banker_injured.push_back({ 192, 448 });
	npc_banker_injured.push_back({ 224, 448 });
	npc_banker_injured.push_back({ 224, 448 });
	npc_banker_injured.push_back({ 128, 448 });
	npc_banker_injured.push_back({ 128, 448 });

	enemy_goblin_facing_right.push_back({ 0,128 });

	enemy_goblin_moving_right.push_back({ 0,128 });
	enemy_goblin_moving_right.push_back({ 32,128 });

	enemy_goblin_alarmed_facing_right.push_back({ 64,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 96,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 64,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 96,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 64,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 96,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 64,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 96,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 128,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 160,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 128,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 160,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 128,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 160,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 128,128 });
	enemy_goblin_alarmed_facing_right.push_back({ 160,128 });

	enemy_crow_facing_right.push_back({ 0, 160 });
	enemy_crow_facing_right.push_back({ 32, 160 });
	enemy_crow_facing_right.push_back({ 64, 160 });
	enemy_crow_facing_right.push_back({ 96, 160 });
	enemy_crow_facing_right.push_back({ 64, 160 });
	enemy_crow_facing_right.push_back({ 32, 160 });
	enemy_crow_facing_right.push_back({ 0, 160 });

	enemy_spider_facing_right.push_back({ 0, 192 });

	enemy_spider_moving_right.push_back({ 0, 192 });
	enemy_spider_moving_right.push_back({ 32, 192 });

	enemy_shroom_facing_right.push_back({ 0, 224 });
	enemy_shroom_facing_right.push_back({ 32, 224 });
	enemy_shroom_facing_right.push_back({ 32, 224 });

	enemy_shroom_moving_right.push_back({ 0, 224 });
	enemy_shroom_moving_right.push_back({ 32, 224 });
	enemy_shroom_moving_right.push_back({ 64, 224 });
	enemy_shroom_moving_right.push_back({ 32, 224 });

	enemy_madskeleton_facing_right.push_back({ 0, 288 });

	enemy_madskeleton_moving_right.push_back({ 0, 288 });
	enemy_madskeleton_moving_right.push_back({ 32, 288 });

	enemy_badskeleton_facing_right.push_back({ 0, 320 });

	enemy_badskeleton_moving_right.push_back({ 0, 320 });
	enemy_badskeleton_moving_right.push_back({ 32, 320 });

	enemy_zombie_facing_right.push_back({ 0, 352 });
	enemy_zombie_facing_right.push_back({ 64, 352 });

	enemy_zombie_moving_right.push_back({ 0, 352 });
	enemy_zombie_moving_right.push_back({ 32, 352 });
	enemy_zombie_moving_right.push_back({ 64, 352 });

	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 32, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 32, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 0, 384 });
	enemy_slime_facing_right.push_back({ 64, 384 });
	enemy_slime_facing_right.push_back({ 96, 384 });
	enemy_slime_facing_right.push_back({ 64, 384 });

	enemy_slime_moving_right.push_back({ 64,384 });
	enemy_slime_moving_right.push_back({ 64,384 });
	enemy_slime_moving_right.push_back({ 96,384 });
	enemy_slime_moving_right.push_back({ 96,384 });
	enemy_slime_moving_right.push_back({ 64,384 });
	enemy_slime_moving_right.push_back({ 96,384 });
	enemy_slime_moving_right.push_back({ 64,384 });
	enemy_slime_moving_right.push_back({ 96,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 128,384 });
	enemy_slime_moving_right.push_back({ 160,384 });
	enemy_slime_moving_right.push_back({ 192,384 });
	enemy_slime_moving_right.push_back({ 224,384 });
	enemy_slime_moving_right.push_back({ 224,384 });
	enemy_slime_moving_right.push_back({ 224,384 });
	enemy_slime_moving_right.push_back({ 192,384 });
	enemy_slime_moving_right.push_back({ 192,384 });
	enemy_slime_moving_right.push_back({ 0,384 });
	enemy_slime_moving_right.push_back({ 0,384 });

	furniture_map_exit.push_back({ 0,0 });
	furniture_map_exit.push_back({ 32,0 });
	furniture_map_exit.push_back({ 64,0 });
	furniture_map_exit.push_back({ 96,0 });
	furniture_map_exit.push_back({ 128,0 });
	furniture_map_exit.push_back({ 160,0 });
	furniture_map_exit.push_back({ 192,0 });
	furniture_map_exit.push_back({ 224,0 });

	clutter_small_vase.push_back({0, 32});
	clutter_big_vase.push_back({ 32,32 });
	clutter_barrel.push_back({ 64,32 });
	clutter_small_crate.push_back({ 96, 32 });
	clutter_medium_crate.push_back({ 128, 32 });
	clutter_small_funerary_vase.push_back({ 0,64 });
	clutter_big_funerary_vase.push_back({ 32, 64 });
	clutter_coffin.push_back({ 64,64 });

	building_camp.push_back({ 0,0 });
	building_camp.push_back({ 96,0 });
	building_camp.push_back({ 192,0 });
	building_camp.push_back({ 288,0 });

	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 64, 160 });
	building_merchant_hut.push_back({ 0, 160 });
	building_merchant_hut.push_back({ 64, 160 });

	building_banker_stall.push_back({ 0, 224 });

	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 0,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 192,0 });
	enemy_generator_yurt.push_back({ 192,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 128,0 });
	enemy_generator_yurt.push_back({ 64,0 });
	enemy_generator_yurt.push_back({ 64,0 });

	enemy_generator_tree.push_back({ 0,64 });

	enemy_generator_stump.push_back({ 0,128 });

	enemy_generator_bonepile.push_back({ 0,160 });

	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 64,160 });
	enemy_generator_grave.push_back({ 96,160 });
	enemy_generator_grave.push_back({ 96,160 });
	enemy_generator_grave.push_back({ 96,160 });
	enemy_generator_grave.push_back({ 96,160 });
	enemy_generator_grave.push_back({ 128,160 });
	enemy_generator_grave.push_back({ 160,160 });
	enemy_generator_grave.push_back({ 128,160 });
	enemy_generator_grave.push_back({ 160,160 });
	enemy_generator_grave.push_back({ 128,160 });
	enemy_generator_grave.push_back({ 160,160 });
	enemy_generator_grave.push_back({ 128,160 });
	enemy_generator_grave.push_back({ 160,160 });
	enemy_generator_grave.push_back({ 96,160 });

	enemy_generator_hole.push_back({ 0,192 });

	enemy_generator_rockmound_small.push_back({ 32,192 });

	enemy_generator_rockmound.push_back({ 64,192 });

	projectile_player_blade.push_back({ 0,0 });
	projectile_player_blade.push_back({ 32,0 });
	
	projectile_player_shield.push_back({ 0, 320 });

	projectile_spear.push_back({0, 128});

	projectile_club.push_back({ 0, 192 });
	projectile_club.push_back({ 32, 192 });
	projectile_club.push_back({ 32, 192 });

	projectile_arrow.push_back({ 64, 224 });

	weapon_staff.push_back({ 0, 32 });
	weapon_staff.push_back({ 64, 32 });
	weapon_staff.push_back({ 128, 32 });
	weapon_staff.push_back({ 128, 32 });
	weapon_staff.push_back({ 192, 32 });
	weapon_staff.push_back({ 192, 32 });
	weapon_staff.push_back({ 192, 32 });
	weapon_staff.push_back({ 192, 32 });

	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });
	weapon_axe.push_back({ 0, 256 });

	weapon_bow.push_back({ 0, 224 });
	weapon_bow.push_back({ 0, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 0, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 0, 224 });
	weapon_bow.push_back({ 32, 224 });
	weapon_bow.push_back({ 0, 224 });
	weapon_bow.push_back({ 32, 224 });

	effect_stun.push_back({ 0, 352 });
	effect_stun.push_back({ 32, 352 });
	effect_stun.push_back({ 64, 352 });
	effect_stun.push_back({ 96, 352 });
	effect_stun.push_back({ 128, 352 });

	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 512,0 });
	item_weapon.push_back({ 544,0 });
	item_weapon.push_back({ 0,0 });
	item_weapon.push_back({ 32,0 });
	item_weapon.push_back({ 64,0 });
	item_weapon.push_back({ 96,0 });
	item_weapon.push_back({ 128,0 });
	item_weapon.push_back({ 160,0 });
	item_weapon.push_back({ 192,0 });
	item_weapon.push_back({ 224,0 });
	item_weapon.push_back({ 256,0 });
	item_weapon.push_back({ 288,0 });
	item_weapon.push_back({ 320,0 });
	item_weapon.push_back({ 352,0 });
	item_weapon.push_back({ 384,0 });
	item_weapon.push_back({ 416,0 });
	item_weapon.push_back({ 448,0 });
	item_weapon.push_back({ 480,0 });

	item_equipment.push_back({ 0, 32 });
	item_equipment.push_back({ 32, 32 });

	item_gold.push_back({ 0, 64 });
	item_gold.push_back({ 32, 64 });
	item_gold.push_back({ 64, 64 });
	item_gold.push_back({ 96, 64 });
	item_gold.push_back({ 128, 64 });
	item_gold.push_back({ 160, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 224, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 224, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 224, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 224, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });
	item_gold.push_back({ 192, 64 });

	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 32, 96 });
	item_health_potion.push_back({ 64, 96 });
	item_health_potion.push_back({ 96, 96 });
	item_health_potion.push_back({ 128, 96 });
	item_health_potion.push_back({ 160, 96 });
	item_health_potion.push_back({ 192, 96 });
	item_health_potion.push_back({ 224, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 256, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 256, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 256, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 256, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });
	item_health_potion.push_back({ 0, 96 });

	item_sparkle.push_back({ 0, 128 });
	item_sparkle.push_back({ 32, 128 });
	item_sparkle.push_back({ 64, 128 });
	item_sparkle.push_back({ 96, 128 });

	ui_arrow_down.push_back({ 0, 96 });

	ui_arrow_up.push_back({ 0, 128 });

}
