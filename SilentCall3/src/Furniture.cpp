#include "Furniture.h"

Furniture::Furniture()
{
}

Furniture::Furniture(double x, double y, int w, int h, SpriteAnimation* sa)
	: Actor()
{
	this->SetX(x);
	this->SetY(y);
	this->SetWidth(w);
	this->SetHeight(h);
	this->SetSpriteAnimation(sa);
	this->SetAnimationPosition(0);

	this->SetMaxHealth(1000);
	this->SetHealth(1000);
}

Furniture::~Furniture()
{
}

bool Furniture::IsPassable()
{
	return passable;
}

void Furniture::SetIsPassable(bool passable)
{
	this->passable = passable;
}

bool Furniture::IsMovable()
{
	return movable;
}

void Furniture::SetIsMovable(bool movable)
{
	this->movable = movable;
}

bool Furniture::IsDestructible()
{
	return destructible;
}

void Furniture::SetIsDestructible(bool destructible)
{
	this->destructible = destructible;
}

void Furniture::Update()
{
	this->ActorUpdate();
}


