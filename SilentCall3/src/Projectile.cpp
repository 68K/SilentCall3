#include "Projectile.h"



Projectile::Projectile()
{
}

Projectile::Projectile(double x, double y, double w, double h, SpriteAnimation* sprite_animation, bool hurts_enemies)
{
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->sprite_animation = sprite_animation;
	this->hurts_enemies = hurts_enemies;
	this->hurts_player = !hurts_enemies;
}

Projectile::~Projectile()
{
}

double Projectile::GetX()
{
	return x;
}

void Projectile::SetX(double x)
{
	this->x = x;
}

double Projectile::GetY()
{
	return y;
}

void Projectile::SetY(double y)
{
	this->y = y;
}

int Projectile::GetWidth()
{
	return w;
}

void Projectile::SetWidth(int w)
{
	this->w = w;
}

int Projectile::GetHeight()
{
	return h;
}

void Projectile::SetHeight(int h)
{
	this->h = h;
}

double Projectile::GetAngle()
{
	return this->angle;
}

void Projectile::SetAngle(double angle)
{
	this->angle = angle;
}

double Projectile::GetSpeed()
{
	return speed;
}

void Projectile::SetSpeed(double speed)
{
	this->speed = speed;
}

bool Projectile::GetIfActive()
{
	return lifetime > 0;
}

void Projectile::Deactivate()
{
	this->lifetime = 0;
}

void Projectile::SetLifeTime(int lifetime)
{
	this->lifetime = lifetime;
}

SpriteAnimation* Projectile::GetSpriteAnimation()
{
	return sprite_animation;
}

void Projectile::SetSpriteAnimation(SpriteAnimation* sa)
{
	this->sprite_animation = sa;
	this->sprite_animation_position = 0;
}

int Projectile::GetAnimationPosition()
{
	return sprite_animation_position;
}

void Projectile::SetIfFacingRight(bool facing_right)
{
	this->facing_right = facing_right;
}

bool Projectile::GetIfFacingRight()
{
	return this->facing_right;
}

void Projectile::SetIfHurtsEnemies(bool hurts_enemies)
{
	this->hurts_enemies = hurts_enemies;
}

bool Projectile::GetIfHurtsEnemies()
{
	return hurts_enemies;
}

void Projectile::SetIfHurtsPlayer(bool hurts_player)
{
	this->hurts_player = hurts_player;
}

bool Projectile::GetIfHurtsPlayer()
{
	return hurts_player;
}

void Projectile::SetDamage(int damage)
{
	this->damage = damage;
}

int Projectile::GetDamage()
{
	return damage;
}

void Projectile::Update()
{
	if (lifetime > 0)
	{
		lifetime--;
	}
	sprite_animation_position++;
	if (sprite_animation_position >= sprite_animation->GetAnimationLength())
	{
		sprite_animation_position = 0;
	}
}