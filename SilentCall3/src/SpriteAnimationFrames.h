#include "../SpriteAnimation.h"

#pragma once
/*! \file SpriteAnimationFrames.h
* A class holding the lengthy sprite animation definitions */
class SpriteAnimationFrames
{
public:
	SpriteAnimationFrames();
	~SpriteAnimationFrames();

	void Initialise();

	// Player
	std::vector<SpriteAnimation::AnimationFrame> player_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> player_moving_right;

	// NPCs
	std::vector<SpriteAnimation::AnimationFrame> npc_male1_waving;
	std::vector<SpriteAnimation::AnimationFrame> npc_male1_merchant;
	std::vector<SpriteAnimation::AnimationFrame> npc_banker_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> npc_banker_injured;

	// Enemies
	std::vector<SpriteAnimation::AnimationFrame> enemy_goblin_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_goblin_alarmed_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_goblin_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_crow_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_spider_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_spider_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_shroom_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_shroom_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_madskeleton_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_madskeleton_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_badskeleton_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_badskeleton_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_zombie_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_zombie_moving_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_slime_facing_right;
	std::vector<SpriteAnimation::AnimationFrame> enemy_slime_moving_right;

	// Furniture
	std::vector<SpriteAnimation::AnimationFrame> furniture_map_exit;

	// Clutter
	std::vector<SpriteAnimation::AnimationFrame> clutter_small_vase;
	std::vector<SpriteAnimation::AnimationFrame> clutter_big_vase;
	std::vector<SpriteAnimation::AnimationFrame> clutter_barrel;
	std::vector<SpriteAnimation::AnimationFrame> clutter_small_crate;
	std::vector<SpriteAnimation::AnimationFrame> clutter_medium_crate;
	std::vector<SpriteAnimation::AnimationFrame> clutter_small_funerary_vase;
	std::vector<SpriteAnimation::AnimationFrame> clutter_big_funerary_vase;
	std::vector<SpriteAnimation::AnimationFrame> clutter_coffin;

	// Buildings
	std::vector<SpriteAnimation::AnimationFrame> building_camp;
	std::vector<SpriteAnimation::AnimationFrame> building_merchant_hut;
	std::vector<SpriteAnimation::AnimationFrame> building_banker_stall;

	// Enemy generators
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_yurt;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_tree;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_stump;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_bonepile;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_grave;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_hole;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_rockmound_small;
	std::vector<SpriteAnimation::AnimationFrame> enemy_generator_rockmound;

	// Projectiles
	std::vector<SpriteAnimation::AnimationFrame> projectile_player_blade;
	std::vector<SpriteAnimation::AnimationFrame> projectile_player_shield;
	std::vector<SpriteAnimation::AnimationFrame> projectile_spear;
	std::vector<SpriteAnimation::AnimationFrame> projectile_club;
	std::vector<SpriteAnimation::AnimationFrame> projectile_arrow;

	// Weapons
	std::vector<SpriteAnimation::AnimationFrame> weapon_staff;
	std::vector<SpriteAnimation::AnimationFrame> weapon_axe;
	std::vector<SpriteAnimation::AnimationFrame> weapon_bow;

	// Effects
	std::vector<SpriteAnimation::AnimationFrame> effect_stun;

	// Items
	std::vector<SpriteAnimation::AnimationFrame> item_weapon;
	std::vector<SpriteAnimation::AnimationFrame> item_equipment;
	std::vector<SpriteAnimation::AnimationFrame> item_gold;
	std::vector<SpriteAnimation::AnimationFrame> item_health_potion;
	std::vector<SpriteAnimation::AnimationFrame> item_sparkle;

	// UI elements
	std::vector<SpriteAnimation::AnimationFrame> ui_arrow_down;
	std::vector<SpriteAnimation::AnimationFrame> ui_arrow_up;
};

