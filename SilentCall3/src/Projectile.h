#include "../SpriteAnimation.h"
#pragma once

class Projectile
{
public:
	Projectile();
	Projectile(double x, double y, double w, double h, SpriteAnimation* sprite_animation, bool hurts_enemies);
	~Projectile();

	double GetX();
	void SetX(double x);
	double GetY();
	void SetY(double y);
	int GetWidth();
	void SetWidth(int w);
	int GetHeight();
	void SetHeight(int h);
	double GetAngle();
	void SetAngle(double angle);
	double GetSpeed();
	void SetSpeed(double speed);
	bool GetIfActive();
	void Deactivate();
	void SetLifeTime(int lifetime);

	SpriteAnimation* GetSpriteAnimation();
	void SetSpriteAnimation(SpriteAnimation* sa);
	int GetAnimationPosition();
	// Need to track if facing right in addition to the angle; as the sprite may have a bottom side that should not rotate to face upwards
	void SetIfFacingRight(bool facing_right);
	bool GetIfFacingRight();

	void SetIfHurtsEnemies(bool hurts_enemies);
	bool GetIfHurtsEnemies();
	void SetIfHurtsPlayer(bool hurts_player);
	bool GetIfHurtsPlayer();
	void SetDamage(int damage);
	int GetDamage();

	void Update();

private:
	double x;
	double y;
	double w = 2;
	double h = 2;
	double angle = 0;
	double speed = 1;
	int lifetime = 5;

	SpriteAnimation* sprite_animation;
	int sprite_animation_position = 0;
	bool facing_right = true;

	bool hurts_enemies = true;
	bool hurts_player = false;
	int damage = 5;
};

