#include "DroppedItem.h"



DroppedItem::DroppedItem()
{
}

DroppedItem::DroppedItem(DropItemType type, int value, double x, double y, int w, int h, SpriteAnimation* sa)
	: Actor()
{
	this->type = type;
	this->value = value;
	this->SetX(x);
	this->SetY(y);
	this->SetWidth(w);
	this->SetHeight(h);
	this->SetSpriteAnimation(sa);
	this->SetAnimationPosition(0);
}

DroppedItem::~DroppedItem()
{
}

DropItemType DroppedItem::GetType()
{
	return this->type;
}

int DroppedItem::GetValue()
{
	return this->value;
}

std::shared_ptr<Item> DroppedItem::GetItem()
{
	return this->item;
}

void DroppedItem::SetItem(std::shared_ptr<Item> item)
{
	this->item = item;
}

bool DroppedItem::GetIsCollected()
{
	return collected;
}

void DroppedItem::SetIsCollected(bool collected)
{
	this->collected = collected;
}

void DroppedItem::Update()
{
	this->ActorUpdate();
}
