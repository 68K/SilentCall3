#include "Bestiary.h"

Bestiary::Bestiary()
{
}


Bestiary::~Bestiary()
{
	BOOST_LOG_TRIVIAL(debug) << "Deleting bestiary entries as part of bestiary destructor";
	for (std::vector<BestiaryEntry*>::iterator it = entries.begin(); it < entries.end(); it++)
	{
		delete(*it);
	}
	entries.clear();
}

void Bestiary::FillDropTables()
{
	drop_tables[no_drops] = new DropTable();
	drop_tables[no_drops]->AddDropChance(1.0, drop_item_nothing);
	drop_tables[only_gold] = new DropTable();
	drop_tables[only_gold]->AddDropChance(0.75, drop_item_nothing);
	drop_tables[only_gold]->AddDropChance(0.25, drop_item_gold);
	drop_tables[gold_potion] = new DropTable();
	drop_tables[gold_potion]->AddDropChance(0.75, drop_item_nothing);
	drop_tables[gold_potion]->AddDropChance(0.15, drop_item_gold);
	drop_tables[gold_potion]->AddDropChance(0.10, drop_item_potion);
	drop_tables[gold_potion_weapon_armour] = new DropTable();
	drop_tables[gold_potion_weapon_armour]->AddDropChance(0.75, drop_item_nothing);
	drop_tables[gold_potion_weapon_armour]->AddDropChance(0.05, drop_item_gold);
	drop_tables[gold_potion_weapon_armour]->AddDropChance(0.05, drop_item_potion);
	drop_tables[gold_potion_weapon_armour]->AddDropChance(0.05, drop_item_armour_random);
	drop_tables[gold_potion_weapon_armour]->AddDropChance(0.05, drop_item_weapon_random);
	drop_tables[sometimes_unique_armour] = new DropTable();
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_gold);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_potion);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_armour_random);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_weapon_random);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_armour_unique);
	drop_tables[sometimes_unique_weapon] = new DropTable();
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_gold);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_potion);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_armour_random);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_weapon_random);
	drop_tables[sometimes_unique_armour]->AddDropChance(0.20, drop_item_weapon_unique);
	drop_tables[rarely_unique_armour] = new DropTable();
	drop_tables[rarely_unique_armour]->AddDropChance(0.23, drop_item_gold);
	drop_tables[rarely_unique_armour]->AddDropChance(0.23, drop_item_potion);
	drop_tables[rarely_unique_armour]->AddDropChance(0.23, drop_item_armour_random);
	drop_tables[rarely_unique_armour]->AddDropChance(0.23, drop_item_weapon_random);
	drop_tables[rarely_unique_armour]->AddDropChance(0.08, drop_item_armour_unique);
	drop_tables[rarely_unique_weapon] = new DropTable();
	drop_tables[rarely_unique_weapon]->AddDropChance(0.23, drop_item_gold);
	drop_tables[rarely_unique_weapon]->AddDropChance(0.23, drop_item_potion);
	drop_tables[rarely_unique_weapon]->AddDropChance(0.23, drop_item_armour_random);
	drop_tables[rarely_unique_weapon]->AddDropChance(0.23, drop_item_weapon_random);
	drop_tables[rarely_unique_weapon]->AddDropChance(0.08, drop_item_weapon_unique);
}

void Bestiary::FillEntries()
{
	entries.push_back(new BestiaryEntry(0, goblin, yurt, gold_potion_weapon_armour, "Goblin Grunt", 1, 5, 18, 14, 28, 8, 0, 80));
	entries.push_back(new BestiaryEntry(1, crow, tree, gold_potion, "Mad Raven", 1, 8, 7, 20, 8, 10, 0, 0));
	entries.push_back(new BestiaryEntry(2, spider, stump, gold_potion, "Fake Widow", 1, 8, 20, 20, 14, 18, 0, 0));
	entries.push_back(new BestiaryEntry(3, shroom, stump, gold_potion, "Psilocyde", 1, 10, 20, 20, 20, 18, 0, 0));
	entries.push_back(new BestiaryEntry(4, mad_skeleton, bonepile, gold_potion_weapon_armour, "Weak Raise", 3, 8, 30, 18, 14, 20, 0, 80));
	entries.push_back(new BestiaryEntry(5, bad_skeleton, bonepile, gold_potion_weapon_armour, "Skeleton Soldier", 4, 12, 40, 30, 14, 25, 0, 0));
	entries.push_back(new BestiaryEntry(6, mad_skeleton, grave, gold_potion_weapon_armour, "Langdeid", 4, 10, 35, 18, 14, 32, 0, 80));
	entries.push_back(new BestiaryEntry(7, zombie, grave, gold_potion_weapon_armour, "Restless", 3, 9, 40, 18, 14, 20, 0, 0));
	entries.push_back(new BestiaryEntry(8, slime, hole, gold_potion_weapon_armour, "Cave Slime", 10, 20, 70, 20, 20, 35, 0, 0));
	entries.push_back(new BestiaryEntry(9, spider, rock_mound_small, gold_potion, "Rock Weaver", 10, 20, 60, 20, 14, 45, 0, 0));
	entries.push_back(new BestiaryEntry(10, goblin, rock_mound, gold_potion_weapon_armour, "Kallikantzaros", 10, 20, 65, 14, 28, 70, 0, 80));
}

Enemy* Bestiary::GenerateEnemy(EnemyGeneratorType generator, int max_level, double x, double y)
{
	std::vector<BestiaryEntry*> possibilities;
	for (std::vector<BestiaryEntry*>::iterator it = entries.begin(); it < entries.end(); it++)
	{
		if ((*it)->generator_type == generator && (*it)->base_level <= max_level)
		{
			possibilities.push_back(*it);
		}
	}
	// If no possibilities; log an error and return a goblin
	if (possibilities.empty())
	{
		BOOST_LOG_TRIVIAL(error) << "No suitable enemies for generator type " << generator 
			                     << " and max level " << max_level 
			                     << " - returning an enemy of the first entry in this bestiary";
		return this->GenerateEnemyFromBestiaryEntry(entries.front(), max_level, x, y);
	}
	// Else return a random possibility
	return this->GenerateEnemyFromBestiaryEntry(possibilities.at(rand() % possibilities.size()), max_level, x, y);
}

DropTable* Bestiary::GetDropTableForBestiaryId(int id)
{
	std::vector<BestiaryEntry*>::iterator it = std::find_if(entries.begin(), entries.end(), [&](BestiaryEntry* be) { return be->bestiary_id == id; });
	DropTable* result = drop_tables[(*it)->drop_table];
	if (result == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to determine a drop table for bestiary ID " << id << ". Returning a only-gold drop table";
		return drop_tables[only_gold];
	}
	return drop_tables[(*it)->drop_table];
}

Enemy* Bestiary::GenerateEnemyFromBestiaryEntry(BestiaryEntry* entry, int max_level, double x, double y)
{
	Enemy* enemy = new Enemy(entry->type, x, y);
	enemy->SetBestiaryId(entry->bestiary_id);
	int possible_level_diff = max_level - entry->base_level;
	int level = max_level;
	if (possible_level_diff > 0) // Guard against division by zero
	{
		level = max_level - (rand() % possible_level_diff);
	}
	enemy->SetLevel(level);
	enemy->SetMeleeDamage(entry->melee_damage);
	enemy->SetProjectileDamage(entry->projectile_damage);
	enemy->SetMaxHealth(entry->health);
	enemy->SetHealth(entry->health);
	enemy->SetXP(entry->base_experience);
	enemy->SetGold(level);
	enemy->SetDropTableType(entry->drop_table);
	enemy->SetWidth(entry->width);
	enemy->SetHeight(entry->height);
	enemy->SetAlarmDuration(entry->alarm_duration);
	return enemy;
}