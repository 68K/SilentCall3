#include "ClutterItem.h"

ClutterItem::ClutterItem(ClutterType type)
	:Actor()
{
	this->type = type;

	switch (type)
	{
	case small_vase:
		this->SetWidth(8);
		this->SetHeight(8);
		break;
	case big_vase:
		this->SetWidth(16);
		this->SetHeight(24);
		break;
	case barrel:
		this->SetWidth(16);
		this->SetHeight(24);
		break;
	case small_crate:
		this->SetWidth(12);
		this->SetHeight(12);
		break;
	case medium_crate:
		this->SetWidth(24);
		this->SetHeight(24);
		break;
	case small_funerary_vase:
		this->SetWidth(8);
		this->SetHeight(8);
		break;
	case big_funerary_vase:
		this->SetWidth(16);
		this->SetHeight(24);
		break;
	case coffin:
		this->SetWidth(16);
		this->SetHeight(32);
		break;
	default:
		BOOST_LOG_TRIVIAL(warning) << "Unhandled clutter type " << type << ". Defaulting width and height to 16.";
		this->SetWidth(16);
		this->SetHeight(16);
	}
}

ClutterItem::~ClutterItem()
{
}

ClutterType ClutterItem::GetClutterType()
{
	return this->type;
}