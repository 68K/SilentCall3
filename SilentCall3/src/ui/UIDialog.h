#pragma once
#include <SDL.h>
#include <vector>
#include "../../common.cpp"
#include "../Item.h"
#include "../../Player.h"
#include "../../Quest.h"
#include "UIIcon.h"

/*! \file UIDialog.h
* A class representing a dialog in the UI */

// A generic UI dialog
class UIDialog
{
public:
	UIDialog(TTF_Font& aFont);
	~UIDialog();

	UIDialogType GetDialogType();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	virtual void Render(SDL_Renderer *ren, SDL_Texture *texture);

	virtual void InputDown();
	virtual void InputUp();
	virtual void InputLeft();
	virtual void InputRight();
	void InputConfirm();


protected:
	UIDialogType type;
	SDL_Rect rect;
	SDL_Color bg_color;
	TTF_Font& font;
};

// Dialog for entering a number

class NumberInputDialog :
	public UIDialog
{
public:
	NumberInputDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow);
	~NumberInputDialog();

	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	void InputLeft();
	void InputRight();
	int InputConfirm();

protected:
	std::string title;
	int min_number = 0;
	int max_number = 9999999;
	int current_amount = 0;
	int selected_digit = 0;

	UIIcon& up_arrow;
	UIIcon& down_arrow;
};

// *** Specific dialogs ***

class AvailableQuestDialog :
	public UIDialog
{
public:
	AvailableQuestDialog(TTF_Font& font, std::vector<Quest*> a_quest_list);
	~AvailableQuestDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	Quest* InputConfirm();


private:
	std::vector<Quest*> quest_list;
	int current_selection = 0;
};

class QuestCompleteDialog :
	public UIDialog
{
public:
	QuestCompleteDialog(TTF_Font& font, Quest& quest);
	~QuestCompleteDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

private:
	Quest& quest;
};

class MessageDialog :
	public UIDialog
{
public:
	MessageDialog(TTF_Font& font, std::string message);
	~MessageDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

private:
	std::string message;
};

class MerchantWelcomeDialog :
	public UIDialog
{
public:
	MerchantWelcomeDialog(TTF_Font& font);
	~MerchantWelcomeDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	UIDialogType InputConfirm();

private:
	std::vector<std::string> options;
	int current_option_selected = 0;
};

class MerchantBuyDialog :
	public UIDialog
{
public:
	/* Need access to the merchant inventory and also the player to access their gold*/
	MerchantBuyDialog(TTF_Font& font, std::vector<std::shared_ptr<Item>>& inventory, std::map<int,std::string>& item_descriptions, Player& player);
	~MerchantBuyDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	std::shared_ptr<Item> InputConfirm();

	void SetInventory(std::vector<std::shared_ptr<Item>>& inventory);

private:
	std::vector<std::shared_ptr<Item>> inventory;
	std::map<int, std::string>& item_descriptions;
	Player& player;
	int current_selection = 0;
};

class MerchantSellDialog :
	public UIDialog
{
public:
	MerchantSellDialog(TTF_Font& font, Player &player);
	~MerchantSellDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	std::shared_ptr<Item> InputConfirm();

private:
	Player& player;
	int current_selection = 0;
};

class MerchantInvestDialog :
	public UIDialog
{
public:
	MerchantInvestDialog(TTF_Font& font, int next_invest_cost, Player &player);
	~MerchantInvestDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

private:
	Player& player;
	int next_invest_cost = 0;
};

class GamePauseDialog :
	public UIDialog
{
public:
	GamePauseDialog(TTF_Font& font);
	~GamePauseDialog();

	void InputUp();
	void InputDown();
	UIDialogType InputConfirm();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);
private:
	std::vector<std::string> options;
	int current_option_selected = 0;
};

class PlayerInventoryDialog :
	public UIDialog
{
public:
	PlayerInventoryDialog(TTF_Font& font, std::vector<std::shared_ptr<Item>>& item_list);
	~PlayerInventoryDialog();
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

private:
	std::vector<std::shared_ptr<Item>> item_list;
};

class PlayerEquipDialog :
	public UIDialog
{
public:
	PlayerEquipDialog(TTF_Font& font, Player* player);
	~PlayerEquipDialog();
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputUp();
	void InputDown();
	EquipSlot InputConfirm();
private:
	std::vector<EquipSlot> options;
	int current_option_selected = 0;
	Player* player;
};

class EquipChangeList :
	public UIDialog
{
public:
	EquipChangeList(TTF_Font& font, Player player, EquipSlot equip_slot, std::vector<std::shared_ptr<Item>> available_items);
	~EquipChangeList();
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputUp();
	void InputDown();
	std::shared_ptr<Item> InputConfirm();

	EquipSlot GetEquipSlot();
private:
	EquipSlot equip_slot;
	Player player;
	std::vector<std::shared_ptr<Item>> available_items;
	int current_option_selected = 0;
};

class BankerWelcomeDialog :
	public UIDialog
{
public:
	BankerWelcomeDialog(TTF_Font& font, int& balance);
	~BankerWelcomeDialog();

	/*! \brief Render this dialog
	*
	* @param *ren		Pointer to an SDL renderer to render to
	* @param *texture	A basic texture
	*/
	void Render(SDL_Renderer *ren, SDL_Texture *texture);

	void InputDown();
	void InputUp();
	UIDialogType InputConfirm();

private:
	std::vector<std::string> options;
	int current_option_selected = 0;
	int& current_balance;
};

class BankerDepositDialog :
	public NumberInputDialog
{
public:
	BankerDepositDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow);
	~BankerDepositDialog();
};

class BankerWithdrawDialog :
	public NumberInputDialog
{
public:
	BankerWithdrawDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow);
	~BankerWithdrawDialog();
};