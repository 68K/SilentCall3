#include "UIIcon.h"



UIIcon::UIIcon()
{
}


UIIcon::~UIIcon()
{
}

double UIIcon::GetX()
{
	return this->x;
}

void UIIcon::SetX(double x)
{
	this->x = x;
}

double UIIcon::GetY()
{
	return this->y;
}

void UIIcon::SetY(double y)
{
	this->y = y;
}

SpriteAnimation* UIIcon::GetSpriteAnimation()
{
	return this->sprite_animation;
}

void UIIcon::SetSpriteAnimation(SpriteAnimation* sa)
{
	this->sprite_animation = sa;
	this->animation_position = 0;
}

int UIIcon::GetAnimationPosition()
{
	return this->animation_position;
}

int UIIcon::GetWidth()
{
	return this->width;
}

int UIIcon::GetHeight()
{
	return this->height;
}

void UIIcon::SetWidth(int w)
{
	this->width = w;
}

void UIIcon::SetHeight(int h)
{
	this->height = h;
}

void UIIcon::Update()
{
	animation_position++;
	if (animation_position >= sprite_animation->GetAnimationLength())
	{
		animation_position = 0;
	}
}