#include "../../common.cpp"
#include "../../SpriteAnimation.h"

#pragma once

/*! \file UIIcon.h
* A class representing an animated property on the UI */
class UIIcon
{
public:
	UIIcon();
	~UIIcon();

	double GetX();
	void SetX(double x);
	double GetY();
	void SetY(double y);

	SpriteAnimation* GetSpriteAnimation();
	void SetSpriteAnimation(SpriteAnimation* sa);
	int GetAnimationPosition();

	int GetWidth();
	int GetHeight();
	void SetWidth(int w);
	void SetHeight(int h);

	void Update();

private:
	double x = 0;
	double y = 0;
	int width = 32;
	int height = 32;

	SpriteAnimation* sprite_animation = nullptr;
	int animation_position = 0;

};

