#include <math.h>
#include "UIDialog.h"
#include "../../common.cpp"

// Generic dialog
UIDialog::UIDialog(TTF_Font& aFont)
	: font(aFont)
{
	type = uidlg_generic;
	bg_color = SDL_Color{ 40, 200, 40, 200 };
}

UIDialog::~UIDialog()
{
}

UIDialogType UIDialog::GetDialogType()
{
	return type;
}

void UIDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	SDL_SetTextureColorMod(texture, bg_color.r, bg_color.g, bg_color.b);
	SDL_SetTextureAlphaMod(texture, bg_color.a);
	SDL_RenderCopy(ren, texture, NULL, &rect);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 255);
}

void UIDialog::InputDown()
{
	BOOST_LOG_TRIVIAL(debug) << "Unhandled instance of InputDown";
}

void UIDialog::InputUp()
{
	BOOST_LOG_TRIVIAL(debug) << "Unhandled instance of InputUp";
}

void UIDialog::InputLeft()
{
	// Nothing to log, some dialogs have no left/right input
}

void UIDialog::InputRight()
{
	// Nothing to log, some dialogs have no left/right input
}

void UIDialog::InputConfirm()
{
	BOOST_LOG_TRIVIAL(debug) << "Unhandled instance of InputConfirm";
}

// NumberInputDialog

NumberInputDialog::NumberInputDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow)
	: UIDialog(font), up_arrow(up_arrow), down_arrow(down_arrow)
{
	rect = SDL_Rect{ 640, 300, 300, 300 };
	// Place selection at least significant digit
	selected_digit = (int)log10((double)max_number);
}

NumberInputDialog::~NumberInputDialog() {};

void NumberInputDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, title, SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 20, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	// Display the current amount, 0 padded.
	std::string current_value_string = StringForWholePart(current_amount);
	std::string max_value_string = StringForWholePart(max_number);
	int padding_digits = max_value_string.length() - current_value_string.length();
	for (int i = 0; i < padding_digits; i++)
	{
		current_value_string = "0" + current_value_string;
	}
	SDL_Texture* current_value_texture = CreateTextureForString(ren, font, current_value_string, SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(current_value_texture, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + (rect.h / 2) - (h /2), w, h };
	SDL_RenderCopy(ren, current_value_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	// Highlight the current digit
	// To be precise (not assume fixed width font) need to measure the digit being highlighted 
	// and also the length of the number up to the highlighted digit
	int highlighted_digit_w, highlighted_digit_h;
	SDL_Texture* highlighted_digit_texture = CreateTextureForString(ren, font, current_value_string.substr(selected_digit, 1), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(highlighted_digit_texture, NULL, NULL, &highlighted_digit_w, &highlighted_digit_h);
	SDL_DestroyTexture(highlighted_digit_texture);

	// Measure the width of the string from the start of the full number to the selected digit
	int highlighted_head_w, highlighted_head_h;
	SDL_Texture* highlighted_head_texture = CreateTextureForString(ren, font, current_value_string.substr(0, selected_digit + 1), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(highlighted_head_texture, NULL, NULL, &highlighted_head_w, &highlighted_head_h);
	SDL_DestroyTexture(highlighted_digit_texture);

	// Draw over the highlighted digit
	dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w /2)  + highlighted_head_w - highlighted_digit_w, rect.y + (rect.h / 2) - (h / 2), highlighted_digit_w, h };
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 128);
	SDL_RenderCopy(ren, texture, NULL, &dest_rect);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 255);

	// Draw indicator arrows
	up_arrow.SetX(rect.x + (rect.w / 2) - (w / 2) + (highlighted_head_w - (highlighted_digit_w / 2)) - (up_arrow.GetWidth() / 2));
	up_arrow.SetY(rect.y + (rect.h / 2) - (h / 2) + up_arrow.GetHeight());
	dest_rect = SDL_Rect{ (int)up_arrow.GetX(), (int)up_arrow.GetY(), up_arrow.GetWidth(), up_arrow.GetHeight() };
	SDL_RenderCopy(ren, up_arrow.GetSpriteAnimation()->GetSpriteSheet(), &up_arrow.GetSpriteAnimation()->GetAnimationFrame(up_arrow.GetAnimationPosition()), &dest_rect);

	down_arrow.SetX(rect.x + (rect.w / 2) - (w / 2) + (highlighted_head_w - (highlighted_digit_w / 2)) - (down_arrow.GetWidth() / 2));
	down_arrow.SetY(rect.y + (rect.h / 2) - (h / 2) - down_arrow.GetHeight());
	dest_rect = SDL_Rect{ (int)down_arrow.GetX(), (int)down_arrow.GetY(), down_arrow.GetWidth(), down_arrow.GetHeight() };
	SDL_RenderCopy(ren, down_arrow.GetSpriteAnimation()->GetSpriteSheet(), &down_arrow.GetSpriteAnimation()->GetAnimationFrame(down_arrow.GetAnimationPosition()), &dest_rect);
}

void NumberInputDialog::InputDown()
{
	int number_of_digits = (int)log10((double)max_number) + 1;
	int current_base = number_of_digits - selected_digit - 1;
	current_amount = std::max(min_number, current_amount - (int)pow(10, current_base));
}

void NumberInputDialog::InputUp()
{
	int number_of_digits = (int)log10((double)max_number) + 1;
	int current_base = number_of_digits - selected_digit - 1;
	current_amount = std::min(max_number, current_amount + (int)(pow(10, current_base)));
}

void NumberInputDialog::InputLeft()
{
	if (selected_digit > 0)
	{
		selected_digit--;
	}
}

void NumberInputDialog::InputRight()
{
	BOOST_LOG_TRIVIAL(debug) << "Pressing right on number input dialog.";
	int number_of_digits = (int)log10((double)max_number) + 1;
	BOOST_LOG_TRIVIAL(debug) << "The number has " << number_of_digits << " digits and the current digit (from left of string) is " << selected_digit;
	if (selected_digit < number_of_digits - 1)
	{
		selected_digit++;
	}
}

int NumberInputDialog::InputConfirm()
{
	return current_amount;
}

// Available quests dialog

AvailableQuestDialog::AvailableQuestDialog(TTF_Font& font, std::vector<Quest*> a_quest_list)
	: UIDialog(font)
{
	type = uidlg_available_quests;
	rect = SDL_Rect{ 150, 100, 800, 600 };
	quest_list = a_quest_list;
	BOOST_LOG_TRIVIAL(debug) << "Constructing AvailableQuestDialog with '" << quest_list.size() << "' quests.";
}

AvailableQuestDialog::~AvailableQuestDialog()
{

}

void AvailableQuestDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	if (quest_list.empty())
	{
		BOOST_LOG_TRIVIAL(error) << "Trying to render an empty list of quests!";
		return;
	}
	for (std::vector<Quest*>::iterator it = quest_list.begin(); it < quest_list.end(); it++)
	{
		int w, h;
		int index = std::distance(quest_list.begin(), it);
		std::string title = (*it)->GetTitle();
		//BOOST_LOG_TRIVIAL(debug) << "Rendering available quest: " << index << " with quest title '" << title << "'";
		SDL_Texture* quest_title_texture = CreateTextureForString(ren, font, title, SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(quest_title_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{rect.x + 20, rect.y + (20 * index), w, h};
		SDL_RenderCopy(ren, quest_title_texture, NULL, &dest_rect);
		SDL_DestroyTexture(quest_title_texture);

		// Highlight selected quest and draw its details
		if (index == current_selection)
		{
			SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 128);
			SDL_RenderCopy(ren, texture, NULL, &dest_rect);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 255);

			SDL_Texture* quest_description_texture = CreateTextureForString(ren, font, (*it)->GetDescription(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
			SDL_QueryTexture(quest_description_texture, NULL, NULL, &w, &h);
			dest_rect = SDL_Rect{ rect.x + 20, rect.y + (rect.h / 2), w, h };
			SDL_RenderCopy(ren, quest_description_texture, NULL, &dest_rect);
			SDL_DestroyTexture(quest_description_texture);
		}
	}
}

void AvailableQuestDialog::InputUp()
{
	if (current_selection > 0)
	{
		current_selection--;
	}
}

void AvailableQuestDialog::InputDown()
{
	if (current_selection < (int)quest_list.size() - 1)
	{
		current_selection++;
	}
}

Quest* AvailableQuestDialog::InputConfirm()
{
	return (quest_list.at(current_selection));
}

// Completed quest dialog
QuestCompleteDialog::QuestCompleteDialog(TTF_Font& font, Quest& quest)
	: UIDialog(font), quest(quest)
{
	type = uidlg_quest_completed;
	rect = SDL_Rect{ 600, 200, 400, 350 };
}

QuestCompleteDialog::~QuestCompleteDialog()
{

}

void QuestCompleteDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* message_texture = CreateTextureForString(ren, font, "Quest Completed!", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(message_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 20, w, h };
	SDL_RenderCopy(ren, message_texture, NULL, &dest_rect);
	SDL_DestroyTexture(message_texture);

	message_texture = CreateTextureForString(ren, font, quest.GetTitle(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(message_texture, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + 20, rect.y + 50, w, h };
	SDL_RenderCopy(ren, message_texture, NULL, &dest_rect);
	SDL_DestroyTexture(message_texture);
}

// Message Dialog

MessageDialog::MessageDialog(TTF_Font& font, std::string message)
	: UIDialog(font)
{
	this->message = message;
	type = uidlg_message;
	// TODO: Change width based on message
	rect = SDL_Rect{ 600, 200, 400, 350 };
}

MessageDialog::~MessageDialog()
{

}

void MessageDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* message_texture = CreateTextureForString(ren, font, message, SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(message_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + (rect.h / 2) - (h / 2), w, h };
	SDL_RenderCopy(ren, message_texture, NULL, &dest_rect);
	SDL_DestroyTexture(message_texture);
}

MerchantWelcomeDialog::MerchantWelcomeDialog(TTF_Font& font)
	: UIDialog(font)
{
	type = uidlg_merchant_welcome;
	rect = SDL_Rect{ 600, 200, 400, 350 };
	options.push_back("Buy");
	options.push_back("Sell");
	options.push_back("Invest");
}

MerchantWelcomeDialog::~MerchantWelcomeDialog()
{

}

void MerchantWelcomeDialog::InputDown()
{
	current_option_selected += 1;
	if (current_option_selected >= (int)options.size())
	{
		current_option_selected = 0;
	}
}

void MerchantWelcomeDialog::InputUp()
{
	current_option_selected -= 1;
	if (current_option_selected < 0)
	{
		current_option_selected = (int)options.size() - 1;
	}
}

UIDialogType MerchantWelcomeDialog::InputConfirm()
{
	if (options.at(current_option_selected) == "Buy")
	{
		return uidlg_merchant_buy;
	}
	if (options.at(current_option_selected) == "Sell")
	{
		return uidlg_merchant_sell;
	}
	if (options.at(current_option_selected) == "Invest")
	{
		return uidlg_merchant_invest;
	}
}

void MerchantWelcomeDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Merchant", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	for (std::vector<std::string>::iterator it = options.begin(); it < options.end(); it++)
	{
		int index = it - options.begin();
		SDL_Texture* option_texture = CreateTextureForString(ren, font, (*it), SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(option_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 50 + (index * 50), w, h };
		SDL_RenderCopy(ren, option_texture, NULL, &dest_rect);
		SDL_DestroyTexture(option_texture);

		if (index == current_option_selected)
		{
			SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 128);
			SDL_RenderCopy(ren, texture, NULL, &dest_rect);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 255);
		}
	}

}

MerchantBuyDialog::MerchantBuyDialog(TTF_Font& font, std::vector<std::shared_ptr<Item>>& inventory, std::map<int, std::string>& item_descriptions, Player& player)
	: player(player), item_descriptions(item_descriptions), UIDialog(font)
{
	type = uidlg_merchant_buy;
	rect = SDL_Rect{ 600, 150, 400, 500 };
	this->inventory = inventory;
	this->player = player;
}

MerchantBuyDialog::~MerchantBuyDialog()
{

}

void MerchantBuyDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Buy", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	for (std::vector<std::shared_ptr<Item>>::iterator it = inventory.begin(); it < inventory.end(); it++)
	{
		int index = it - inventory.begin();
		SDL_Texture* item_name_texture = CreateTextureForString(ren, font, (*it)->GetName(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(item_name_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 50 + (index * 50), w, h };
		SDL_RenderCopy(ren, item_name_texture, NULL, &dest_rect);
		SDL_DestroyTexture(item_name_texture);

		SDL_Texture* item_price_texture = CreateTextureForString(ren, font, StringForWholePart(player.BuyingPriceFor(*it)), SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(item_price_texture, NULL, NULL, &w, &h);
		dest_rect = SDL_Rect{ rect.x + rect.w - w - 10, rect.y + 50 + (index * 50), w, h };
		SDL_RenderCopy(ren, item_price_texture, NULL, &dest_rect);
		SDL_DestroyTexture(item_price_texture);

		if (index == current_selection)
		{
			SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 128);
			SDL_RenderCopy(ren, texture, NULL, &dest_rect);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 255);
		}
	}

	SDL_Texture* current_player_gold = CreateTextureForString(ren, font, "Gold: " + StringForWholePart(player.GetGold()), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(current_player_gold, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + rect.w - w - 10, rect.y + rect.h - h - 5, w, h };
	SDL_RenderCopy(ren, current_player_gold, NULL, &dest_rect);
	SDL_DestroyTexture(current_player_gold);

	SDL_Texture* current_item_description = CreateTextureForString(ren, font, item_descriptions.at(inventory.at(current_selection)->GetDescriptionId()), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(current_item_description, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + (rect.w/2) - (w/2), rect.y + rect.h - h - 30, w, h };
	SDL_RenderCopy(ren, current_item_description, NULL, &dest_rect);
	SDL_DestroyTexture(current_item_description);
}

void MerchantBuyDialog::InputDown()
{
	current_selection++;
	if (current_selection >= inventory.size())
	{
		current_selection = 0;
	}
}

void MerchantBuyDialog::InputUp()
{
	current_selection -= 1;
	if (current_selection < 0)
	{
		current_selection = (int)inventory.size() - 1;
	}
}

std::shared_ptr<Item> MerchantBuyDialog::InputConfirm()
{
	return inventory.at(current_selection);
}

void MerchantBuyDialog::SetInventory(std::vector<std::shared_ptr<Item>>& inventory)
{
	this->inventory = inventory;
}

MerchantSellDialog::MerchantSellDialog(TTF_Font& font, Player & player)
	: player(player), UIDialog(font)
{
	type = uidlg_merchant_sell;
	rect = SDL_Rect{ 600, 150, 400, 500 };
	this->player = player;
	BOOST_LOG_TRIVIAL(debug) << "Created a MerchantSellDialog displaying " << player.GetItems().size() << " items.";
}

MerchantSellDialog::~MerchantSellDialog()
{

}

void MerchantSellDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);
	std::vector<std::shared_ptr<Item>> items = player.GetItems();

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Sell", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	if (items.size() == 0)
	{
		SDL_Texture* no_items_texture = CreateTextureForString(ren, font, "You have nothing to sell.", SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(no_items_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + rect.h / 2, w, h };
		SDL_RenderCopy(ren, no_items_texture, NULL, &dest_rect);
		SDL_DestroyTexture(no_items_texture);
	}
	else
	{
		for (std::vector<std::shared_ptr<Item>>::iterator it = items.begin(); it < items.end(); it++)
		{
			int index = it - items.begin();
			SDL_Texture* item_name_texture = CreateTextureForString(ren, font, (*it)->GetName(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
			SDL_QueryTexture(item_name_texture, NULL, NULL, &w, &h);
			SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 50 + (index * 50), w, h };
			SDL_RenderCopy(ren, item_name_texture, NULL, &dest_rect);
			SDL_DestroyTexture(item_name_texture);

			SDL_Texture* item_price_texture = CreateTextureForString(ren, font, StringForWholePart(player.SellingPriceFor(*it)), SDL_Color{ 255,255,255,255 }, rect.w - 40);
			SDL_QueryTexture(item_price_texture, NULL, NULL, &w, &h);
			dest_rect = SDL_Rect{ rect.x + rect.w - w - 10, rect.y + 50 + (index * 50), w, h };
			SDL_RenderCopy(ren, item_price_texture, NULL, &dest_rect);
			SDL_DestroyTexture(item_price_texture);

			if (index == current_selection)
			{
				SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
				SDL_SetTextureColorMod(texture, 255, 255, 255);
				SDL_SetTextureAlphaMod(texture, 128);
				SDL_RenderCopy(ren, texture, NULL, &dest_rect);
				SDL_SetTextureColorMod(texture, 255, 255, 255);
				SDL_SetTextureAlphaMod(texture, 255);
			}
		}
	}
	SDL_Texture* current_player_gold = CreateTextureForString(ren, font, "Gold: " + StringForWholePart(player.GetGold()), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(current_player_gold, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + rect.w - w - 10, rect.y + rect.h - h - 5, w, h };
	SDL_RenderCopy(ren, current_player_gold, NULL, &dest_rect);
	SDL_DestroyTexture(current_player_gold);
}

void MerchantSellDialog::InputDown()
{
	current_selection++;
	if (current_selection >= (int)player.GetItems().size())
	{
		current_selection = 0;
	}
}

void MerchantSellDialog::InputUp()
{
	current_selection -= 1;
	if (current_selection < 0)
	{
		current_selection = (int)player.GetItems().size() - 1;
	}
}

std::shared_ptr<Item> MerchantSellDialog::InputConfirm()
{
	return player.GetItems().at(current_selection);
}

MerchantInvestDialog::MerchantInvestDialog(TTF_Font& font, int next_invest_cost, Player &player)
	: player(player), UIDialog(font)
{
	type = uidlg_merchant_invest;
	rect = SDL_Rect{ 600, 200, 400, 350 };
	this->next_invest_cost = next_invest_cost;
	this->player = player;
	BOOST_LOG_TRIVIAL(debug) << "Created MerchantInvestDialog for cost " << this->next_invest_cost;
}

MerchantInvestDialog::~MerchantInvestDialog()
{

}

void MerchantInvestDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Invest", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	SDL_Texture* cost_texture = CreateTextureForString(ren, font, "Cost : " + std::to_string(next_invest_cost), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(cost_texture, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + (rect.h / 2) - (h / 2), w, h };
	SDL_RenderCopy(ren, cost_texture, NULL, &dest_rect);
	SDL_DestroyTexture(cost_texture);

	SDL_Texture* ok_texture = CreateTextureForString(ren, font, "OK", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(ok_texture, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + rect.h - h - 5, w, h };
	SDL_RenderCopy(ren, ok_texture, NULL, &dest_rect);
	SDL_DestroyTexture(ok_texture);

	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 128);
	SDL_RenderCopy(ren, texture, NULL, &dest_rect);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 255);

	SDL_Texture* current_player_gold = CreateTextureForString(ren, font, "Current Gold: " + StringForWholePart(player.GetGold()), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(current_player_gold, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + rect.w - w - 10, rect.y + rect.h - h - 5, w, h };
	SDL_RenderCopy(ren, current_player_gold, NULL, &dest_rect);
	SDL_DestroyTexture(current_player_gold);
}

GamePauseDialog::GamePauseDialog(TTF_Font& font)
	: UIDialog(font)
{
	type = uidlg_pausegame;
	rect = SDL_Rect{ 600, 200, 400, 350 };

	options.push_back("Inventory");
	options.push_back("Equip");
}

GamePauseDialog::~GamePauseDialog()
{

}

void GamePauseDialog::InputDown()
{
	current_option_selected += 1;
	if (current_option_selected >= (int)options.size())
	{
		current_option_selected = 0;
	}
}

void GamePauseDialog::InputUp()
{
	current_option_selected -= 1;
	if (current_option_selected < 0)
	{
		current_option_selected = (int)options.size() - 1;
	}
}

UIDialogType GamePauseDialog::InputConfirm()
{
	if (options.at(current_option_selected) == "Inventory")
	{
		return uidlg_character_inventory;
	}
	if (options.at(current_option_selected) == "Equip")
	{
		return uidlg_character_equip;
	}
}

void GamePauseDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Pause", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	for (std::vector<std::string>::iterator it = options.begin(); it < options.end(); it++)
	{
		int index = it - options.begin();
		SDL_Texture* option_texture = CreateTextureForString(ren, font, (*it), SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(option_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 50 + (index * 50), w, h };
		SDL_RenderCopy(ren, option_texture, NULL, &dest_rect);
		SDL_DestroyTexture(option_texture);

		if (index == current_option_selected)
		{
			SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 128);
			SDL_RenderCopy(ren, texture, NULL, &dest_rect);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 255);
		}
	}
}

PlayerInventoryDialog::PlayerInventoryDialog(TTF_Font& font, std::vector<std::shared_ptr<Item>>& item_list)
	: UIDialog(font)
{
	type = uidlg_character_inventory;
	rect = SDL_Rect{ 600, 200, 400, 350 };
	this->item_list = item_list;
}

PlayerInventoryDialog::~PlayerInventoryDialog()
{

}

void PlayerInventoryDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Inventory", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	if (item_list.empty())
	{
		SDL_Texture* notice_texture = CreateTextureForString(ren, font, "No Items", SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(notice_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + (rect.h / 2), w, h };
		SDL_RenderCopy(ren, notice_texture, NULL, &dest_rect);
		SDL_DestroyTexture(notice_texture);
	}
	else
	{
		for (std::vector<std::shared_ptr<Item>>::iterator it = item_list.begin(); it < item_list.end(); it++)
		{
			int w, h;
			int index = std::distance(item_list.begin(), it);
			std::string title = (*it)->GetName();
			SDL_Texture* item_texture = CreateTextureForString(ren, font, title, SDL_Color{ 255,255,255,255 }, rect.w - 40);
			SDL_QueryTexture(item_texture, NULL, NULL, &w, &h);
			SDL_Rect dest_rect = SDL_Rect{ rect.x + 20, rect.y + 40 + (h * index), w, h };
			SDL_RenderCopy(ren, item_texture, NULL, &dest_rect);
			SDL_DestroyTexture(item_texture);
		}
	}
}

PlayerEquipDialog::PlayerEquipDialog(TTF_Font& font, Player* player)
	: UIDialog(font)
{
	type = uidlg_character_equip;
	rect = SDL_Rect{ 600, 200, 400, 350 };
	this->player = player;

	options.push_back(equip_slot_primary_weapon);
	options.push_back(equip_slot_secondary_weapon);
	options.push_back(equip_slot_armour);
}

PlayerEquipDialog::~PlayerEquipDialog()
{

}

void PlayerEquipDialog::InputUp()
{
	if (current_option_selected > 0)
	{
		current_option_selected--;
	}
}

void PlayerEquipDialog::InputDown()
{
	if (current_option_selected < options.size())
	{
		current_option_selected++;
	}
}

EquipSlot PlayerEquipDialog::InputConfirm()
{
	return options.at(current_option_selected);
}

void PlayerEquipDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	int w, h;
	SDL_Texture* string_texture = CreateTextureForString(ren, font, "Equipment", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
	SDL_DestroyTexture(string_texture);

	for (int i = 0; i < options.size(); i++)
	{
		switch (options.at(i))
		{
		    case equip_slot_armour:
			{
				string_texture = CreateTextureForString(ren, font, "Armour", SDL_Color{ 255,255,255,255 }, rect.w - 40);
				SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
				dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 40 + i * 100, w, h };
				SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
				SDL_DestroyTexture(string_texture);

				if (player->GetEquippedArmour())
				{
					string_texture = CreateTextureForString(ren, font, player->GetEquippedArmour()->GetName(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
					SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
					dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 60 + i * 100, w, h };
					SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
					SDL_DestroyTexture(string_texture);
				}

				break;
			}
			case equip_slot_primary_weapon:
			{
				string_texture = CreateTextureForString(ren, font, "Weapon", SDL_Color{ 255,255,255,255 }, rect.w - 40);
				SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
				dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 40 + i * 100, w, h };
				SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
				SDL_DestroyTexture(string_texture);

				if (player->GetPrimaryEquippedWeapon())
				{
					string_texture = CreateTextureForString(ren, font, player->GetPrimaryEquippedWeapon()->GetName(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
					SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
					dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 60 + i * 100, w, h };
					SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
					SDL_DestroyTexture(string_texture);
				}

				break;
			}
			case equip_slot_secondary_weapon:
			{
				string_texture = CreateTextureForString(ren, font, "Secondary Weapon", SDL_Color{ 255,255,255,255 }, rect.w - 40);
				SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
				dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 40 + i * 100, w, h };
				SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
				SDL_DestroyTexture(string_texture);

				if (player->GetSecondaryEquippedWeapon())
				{
					string_texture = CreateTextureForString(ren, font, player->GetSecondaryEquippedWeapon()->GetName(), SDL_Color{ 255,255,255,255 }, rect.w - 40);
					SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
					dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 60 + i * 100, w, h };
					SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
					SDL_DestroyTexture(string_texture);
				}

				break;
			}
		}
	}

	// TODO: Re-using w and h here probably works but isn't guaranteed to cover the right area of both the slot name and the equipment name
	SDL_Rect current_selection_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 40 + current_option_selected * 100, w, h * 2 };
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 128);
	SDL_RenderCopy(ren, texture, NULL, &current_selection_rect);
	SDL_SetTextureColorMod(texture, 255, 255, 255);
	SDL_SetTextureAlphaMod(texture, 255);
}

EquipChangeList::EquipChangeList(TTF_Font& font, Player player, EquipSlot equip_slot, std::vector<std::shared_ptr<Item>> available_items)
	: UIDialog(font)
{
	this->equip_slot = equip_slot;
	this->player = player;
	this->available_items = available_items;
	type = uidlg_equip_change;
	rect = SDL_Rect{ 600, 200, 400, 350 };
}

EquipChangeList::~EquipChangeList()
{

}

void EquipChangeList::InputUp()
{
	if (current_option_selected > 0)
	{
		current_option_selected--;
	}
}

void EquipChangeList::InputDown()
{
	if (current_option_selected < available_items.size() - 1)
	{
		current_option_selected++;
	}
}

std::shared_ptr<Item> EquipChangeList::InputConfirm()
{
	return available_items.at(current_option_selected);
}

EquipSlot EquipChangeList::GetEquipSlot()
{
	return equip_slot;
}

void EquipChangeList::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);

	std::string title = "";
	switch (equip_slot)
	{
		case equip_slot_armour:
			title = "Select Armour";
			break;
		case equip_slot_primary_weapon:
			title = "Select Weapon";
			break;
		case equip_slot_secondary_weapon:
			title = "Select Secondary Weapon";
			break;
	}
	int w, h;
	SDL_Texture* string_texture = CreateTextureForString(ren, font, title, SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(string_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 5, w, h };
	SDL_RenderCopy(ren, string_texture, NULL, &dest_rect);
	SDL_DestroyTexture(string_texture);

	if (available_items.empty())
	{
		SDL_Texture* notice_texture = CreateTextureForString(ren, font, "No Items", SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(notice_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + (rect.h / 2), w, h };
		SDL_RenderCopy(ren, notice_texture, NULL, &dest_rect);
		SDL_DestroyTexture(notice_texture);
	}
	else
	{
		for (std::vector<std::shared_ptr<Item>>::iterator it = available_items.begin(); it < available_items.end(); it++)
		{
			int w, h;
			int index = std::distance(available_items.begin(), it);
			std::string title = (*it)->GetName();
			SDL_Texture* item_texture = CreateTextureForString(ren, font, title, SDL_Color{ 255,255,255,255 }, rect.w - 40);
			SDL_QueryTexture(item_texture, NULL, NULL, &w, &h);
			SDL_Rect dest_rect = SDL_Rect{ rect.x + 20, rect.y + 40 + (h * index), w, h };
			SDL_RenderCopy(ren, item_texture, NULL, &dest_rect);
			SDL_DestroyTexture(item_texture);

			if (index == current_option_selected)
			{
				// Render highlight of currently selected item
				SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
				SDL_SetTextureColorMod(texture, 255, 255, 255);
				SDL_SetTextureAlphaMod(texture, 128);
				SDL_RenderCopy(ren, texture, NULL, &dest_rect);
				SDL_SetTextureColorMod(texture, 255, 255, 255);
				SDL_SetTextureAlphaMod(texture, 255);

				// Render item comparison
				bool better = false;
				double diff = 0;
				switch (equip_slot)
				{
				case equip_slot_primary_weapon:
					better = ((std::static_pointer_cast<WeaponItem>)(*it).get())->GetAttackLevel() > player.GetPrimaryEquippedWeapon()->GetAttackLevel();
					diff = abs(((std::static_pointer_cast<WeaponItem>)(*it).get())->GetAttackLevel() - player.GetPrimaryEquippedWeapon()->GetAttackLevel());
					//BOOST_LOG_TRIVIAL(debug) << "Diff between current and select weapon is " << std::to_string(diff);
					break;
				case equip_slot_secondary_weapon:
					better = ((std::static_pointer_cast<WeaponItem>)(*it).get())->GetAttackLevel() > player.GetSecondaryEquippedWeapon()->GetAttackLevel();
					diff = abs(((std::static_pointer_cast<WeaponItem>)(*it).get())->GetAttackLevel() - player.GetSecondaryEquippedWeapon()->GetAttackLevel());
					break;
				case equip_slot_armour:
					better = ((std::static_pointer_cast<ArmourItem>)(*it).get())->GetArmourLevel() > player.GetEquippedArmour()->GetArmourLevel();
					diff = abs(((std::static_pointer_cast<ArmourItem>)(*it).get())->GetArmourLevel() - player.GetEquippedArmour()->GetArmourLevel());
					break;
				default:
					BOOST_LOG_TRIVIAL(warning) << "Defaulted on rendering item comparison. Equip slot: " << equip_slot;
				}
				if (diff != 0)
				{
					SDL_Color diff_color = better ? SDL_Color{ 255,100,100,255 } : SDL_Color{ 100,100,255,255 };
					std::string sign = better ? "+" : "-";
					std::string diff_string = sign + std::to_string((int)diff);
					//BOOST_LOG_TRIVIAL(debug) << "About to render item diff '" << diff_string << "'";
					SDL_Texture* diff_texture = CreateTextureForString(ren, font, diff_string, diff_color, rect.w - 40);
					SDL_QueryTexture(diff_texture, NULL, NULL, &w, &h);
					SDL_Rect dest_rect = SDL_Rect{ rect.x + 40, rect.y + rect.h - 24, w, h };
					SDL_RenderCopy(ren, diff_texture, NULL, &dest_rect);
					SDL_DestroyTexture(diff_texture);
				}
			}
		}
	}
}

BankerWelcomeDialog::BankerWelcomeDialog(TTF_Font& font, int& balance)
	: UIDialog(font), current_balance(balance)
{
	type = uidlg_banker_welcome;
	rect = SDL_Rect{ 600, 200, 400, 250 };

	options.push_back("Deposit");
	options.push_back("Withdraw");
}

BankerWelcomeDialog::~BankerWelcomeDialog()
{

}

void BankerWelcomeDialog::Render(SDL_Renderer *ren, SDL_Texture *texture)
{
	UIDialog::Render(ren, texture);
	int w, h;

	SDL_Texture* title_texture = CreateTextureForString(ren, font, "Welcome to the bank of Gem", SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(title_texture, NULL, NULL, &w, &h);
	SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 10, w, h };
	SDL_RenderCopy(ren, title_texture, NULL, &dest_rect);
	SDL_DestroyTexture(title_texture);

	for (std::vector<std::string>::iterator it = options.begin(); it < options.end(); it++)
	{
		int index = it - options.begin();
		SDL_Texture* option_texture = CreateTextureForString(ren, font, (*it), SDL_Color{ 255,255,255,255 }, rect.w - 40);
		SDL_QueryTexture(option_texture, NULL, NULL, &w, &h);
		SDL_Rect dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + 50 + (index * 50), w, h };
		SDL_RenderCopy(ren, option_texture, NULL, &dest_rect);
		SDL_DestroyTexture(option_texture);

		if (index == current_option_selected)
		{
			SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 128);
			SDL_RenderCopy(ren, texture, NULL, &dest_rect);
			SDL_SetTextureColorMod(texture, 255, 255, 255);
			SDL_SetTextureAlphaMod(texture, 255);
		}
	}

	SDL_Texture* balance_texture = CreateTextureForString(ren, font, std::string("Balance: ") + StringForWholePart(current_balance), SDL_Color{ 255,255,255,255 }, rect.w - 40);
	SDL_QueryTexture(balance_texture, NULL, NULL, &w, &h);
	dest_rect = SDL_Rect{ rect.x + (rect.w / 2) - (w / 2), rect.y + rect.h - 10 - h, w, h };
	SDL_RenderCopy(ren, balance_texture, NULL, &dest_rect);
	SDL_DestroyTexture(balance_texture);
}

void BankerWelcomeDialog::InputUp()
{
	if (current_option_selected > 0)
	{
		current_option_selected--;
	}
}

void BankerWelcomeDialog::InputDown()
{
	if (current_option_selected < options.size() - 1)
	{
		current_option_selected++;
	}
}

UIDialogType BankerWelcomeDialog::InputConfirm()
{
	if (options.at(current_option_selected) == "Deposit")
	{
		return uidlg_banker_deposit;
	}
	if (options.at(current_option_selected) == "Withdraw")
	{
		return uidlg_banker_withdraw;
	}
}

BankerDepositDialog::BankerDepositDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow)
	: NumberInputDialog(font, up_arrow, down_arrow)
{
	this->title = "Amount To Deposit";
	this->type = uidlg_banker_deposit;
}

BankerDepositDialog::~BankerDepositDialog() {}

BankerWithdrawDialog::BankerWithdrawDialog(TTF_Font& font, UIIcon& up_arrow, UIIcon& down_arrow)
	: NumberInputDialog(font, up_arrow, down_arrow)
{
	this->title = "Amount To Withdraw";
	this->type = uidlg_banker_withdraw;
}

BankerWithdrawDialog::~BankerWithdrawDialog() {}