#include "TownMenuOption.h"



TownMenuOption::TownMenuOption()
{
}

TownMenuOption::TownMenuOption(TownMenuOptionId anId, std::string aTitle, SDL_Texture* aTitleTexture, bool isEnabled)
{
	id = anId;
	title = aTitle;
	title_texture = aTitleTexture;
	enable = isEnabled;
}


TownMenuOption::~TownMenuOption()
{
}

void TownMenuOption::Enable()
{
	enable = true;
}

void TownMenuOption::Disable()
{
	enable = false;
}

SDL_Texture* TownMenuOption::GetTitleTexture()
{
	return title_texture;
}

TownMenuOptionId TownMenuOption::GetTownMenuOptionId()
{
	return id;
}
