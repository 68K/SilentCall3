#include "Quest.h"

Quest::Quest()
{
}

Quest::Quest(std::string title, std::string description, DungeonMapType map, int base_level, QuestCompletionCriteriaType completion_criteria_type,
	         int target_x, int target_y, QuestItem quest_item_id, DungeonMapType target_map, Enemy* target_enemy,
			 QuestLineType quest_line_type, int quest_line_id)
{
	this->title = title;
	this->description = description;
	this->map = map;
	this->base_level = base_level;
	this->completion_criteria = completion_criteria_type;
	this->target_x = target_x;
	this->target_y = target_y;
	this->quest_item_id = quest_item_id;
	this->target_map = target_map;
	this->target_enemy = target_enemy;
	this->quest_line_type = quest_line_type;
	this->quest_line_id = quest_line_id;
}

Quest::~Quest()
{
}

std::string Quest::GetTitle()
{
    return title;
}

std::string Quest::GetDescription()
{
	return description;
}

QuestState Quest::GetState()
{
	return state;
}

void Quest::SetState(QuestState state)
{
	this->state = state;
}

int Quest::GetBaseLevel()
{
	return base_level;
}

DungeonMapType Quest::GetMapType()
{
	return map;
}

DungeonMapType Quest::GetDestination()
{
	return target_map;
}

QuestCompletionCriteriaType Quest::GetQuestCompletionCriteriaType()
{
	return completion_criteria;
}

QuestLineType Quest::GetQuestLineType()
{
	return quest_line_type;
}

int Quest::GetQuestLineId()
{
	return quest_line_id;
}

int Quest::GetTargetX()
{
	return this->target_x;
}

int Quest::GetTargetY()
{
	return this->target_y;
}

QuestItem Quest::GetQuestItemId()
{
	return this->quest_item_id;
}

bool Quest::IsNearCompletionLocation(int x, int y, int threshold)
{
	if (target_x < 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "Quest::IsNearCompletionLocation is returning true because target_x is less than 0 (implies \"anywhere on this map\")";
		return true;
	}
	if (distance_between(x, y, target_x, target_y) < threshold)
	{
		BOOST_LOG_TRIVIAL(trace) << "Quest::IsNearCompletionLocation is returning true because (" << x << "," << y << ") is within threshold " << threshold;
		return true;
	}
	else
	{
		BOOST_LOG_TRIVIAL(trace) << "Quest::IsNearCompletionLocation is returning false because (" << x << "," << y << ") is beyond threshold " << threshold << " of target (" << target_x << "," << target_y << ")";
		return false;
	}
}