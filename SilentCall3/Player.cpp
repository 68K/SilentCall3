#include "Player.h"
#include "common.cpp"
#include "src/items/PotionItem.h"

Player::Player()
{
}

Player::Player(int x, int y, SpriteAnimation * sa)
{
	x = x;
	y = y;
	sprite_animation = sa;
}

Player::~Player()
{
}

double Player::GetX()
{
	return x;
}

double Player::GetY()
{
	return y;
}

void Player::SetX(double x)
{
	this->x = x;
}

void Player::SetY(double y)
{
	this->y = y;
}

void Player::SetXYPosition(double x, double y)
{
	this->x = x;
	this->y = y;
}

SpriteAnimation * Player::GetSpriteAnimation()
{
	return sprite_animation;
}

void Player::SetSpriteAnimation(SpriteAnimation* sa)
{
	sprite_animation = sa;
	animation_position = 0;
}

int Player::GetAnimationPosition()
{
	return animation_position;
}

double Player::GetMovementSpeed()
{
	return movement_speed;
}

void Player::SetMovementSpeed(double speed)
{
	movement_speed = speed;
}

double Player::GetEffectiveMovementSpeed()
{
	double effective_speed = movement_speed;
	if (IsPrimaryDefending())
	{
		effective_speed = effective_speed * GetPrimaryEquippedWeapon()->GetDefenseSpeedAdjust();
	}
	if (IsSecondaryDefending())
	{
		effective_speed = effective_speed * GetSecondaryEquippedWeapon()->GetDefenseSpeedAdjust();
	}
	return effective_speed;
}

int Player::GetWidth()
{
	return width;
}

int Player::GetHeight()
{
	return height;
}

bool Player::GetIfFacingRight()
{
	return facing_right;
}

void Player::SetIfFacingRight(bool right)
{
	facing_right = right;
}

bool Player::GetIfPointingUp()
{
	return this->pointing_up;
}

void Player::SetIfPointingUp(bool is_pointing_up)
{
	this->pointing_up = is_pointing_up;
}

bool Player::GetIfPointingDown()
{
	return this->pointing_down;
}

void Player::SetIfPointingDown(bool is_pointing_down)
{
	this->pointing_down = is_pointing_down;
}

bool Player::GetIfPointingLeft()
{
	return this->pointing_left;
}

void Player::SetIfPointingLeft(bool is_pointing_left)
{
	this->pointing_left = is_pointing_left;
}

bool Player::GetIfPointingRight()
{
	return this->pointing_right;
}

void Player::SetIfPointingRight(bool is_pointing_right)
{
	this->pointing_right = is_pointing_right;
}

double Player::GetPointingAngle()
{
	if (pointing_right && !pointing_up && !pointing_down) return 0;
	if (pointing_right && pointing_up)
	{
		BOOST_LOG_TRIVIAL(debug) << "Player is pointing up and right";
		return (double)(M_PI / 4);
	}
	if (pointing_up && !pointing_left && !pointing_right) return M_PI / 2;
	if (pointing_left && pointing_up) return (double)(M_PI - (M_PI / 4));
	if (pointing_left && !pointing_up && !pointing_down) return M_PI;
	if (pointing_left && pointing_down) return M_PI + (M_PI / 4);
	if (pointing_down && !pointing_left && !pointing_right) return M_PI + (M_PI / 2);
	return 2 * M_PI - (M_PI / 4);
}

bool Player::GetIfMoving()
{
	return moving;
}

void Player::SetIfMoving(bool moving)
{
	this->moving = moving;
}

void Player::Update()
{
	// Update direction pointing by comparing last position to current
	if (x == previous_x && y != previous_y)
	{
		SetIfPointingRight(false);
		SetIfPointingLeft(false);
	}
	if (previous_x < x)
	{
		SetIfPointingRight(true);
		SetIfPointingLeft(false);
	}
	if (previous_x > x)
	{
		SetIfPointingLeft(true);
		SetIfPointingRight(false);
	}
	if (y == previous_y && x != previous_x)
	{
		SetIfPointingDown(false);
		SetIfPointingUp(false);
	}
	if (previous_y < y)
	{
		BOOST_LOG_TRIVIAL(trace) << "Player pointing down";
		SetIfPointingDown(true);
		SetIfPointingUp(false);
	}
	if (previous_y > y)
	{
		SetIfPointingUp(true);
		SetIfPointingDown(false);
	}

	// Now make the current position the "old" position
	previous_x = x;
	previous_y = y;

	animation_position++;
	if (animation_position >= sprite_animation->GetAnimationLength())
	{
		animation_position = 0;
	}
	if (primary_attack_cooldown_current > 0)
	{
		primary_attack_cooldown_current--;
	}
	if (secondary_attack_cooldown_current > 0)
	{
		secondary_attack_cooldown_current--;
	}
	if (primary_defend_cooldown_current > 0)
	{
		primary_defend_cooldown_current--;
	}
	if (secondary_defend_cooldown_current > 0)
	{
		secondary_defend_cooldown_current--;
	}

	if (potion_cooldown_current > 0)
	{
		potion_cooldown_current--;
	}

	if (current_invincibility_frames > 0)
	{
		current_invincibility_frames--;
	}
	if (primary_weapon_animation)
	{
		primary_weapon_animation_current_frame++;
		if (primary_weapon_animation_current_frame >= primary_weapon_animation->GetAnimationLength())
		{
			primary_weapon_animation = nullptr;
			directly_primary_attacking = false;
		}
	}
	if (secondary_weapon_animation)
	{
		secondary_weapon_animation_current_frame++;
		if (secondary_weapon_animation_current_frame >= secondary_weapon_animation->GetAnimationLength())
		{
			secondary_weapon_animation = nullptr;
			directly_secondary_attacking = false;
		}
	}
}

bool Player::IsReadyToPrimaryAttack()
{
	return primary_attack_cooldown_current == 0;
}

void Player::PrimaryAttack(SpriteAnimation* sprite_animation)
{
	primary_attack_cooldown_current = primary_attack_cooldown_duration;

	if (equipped_primary_weapon)
	{
		if (sprite_animation)
		{
			primary_weapon_animation_current_frame = 0;
			primary_weapon_animation = sprite_animation;
			if (!equipped_primary_weapon->FiresProjectiles())
			{
				directly_primary_attacking = true;
			}
		}
	}
}

bool Player::IsDirectlyPrimaryAttacking()
{
	return this->directly_primary_attacking;
}

int Player::GetPrimaryAttackDamage()
{
	if (equipped_primary_weapon)
	{
		return this->equipped_primary_weapon->GetAttackLevel();
	}
	else
	{
		BOOST_LOG_TRIVIAL(warning) << "Player's attack damage queried when no primary weapon was equipped. Returning 1";
		return 1;
	}
}

bool Player::IsReadyToSecondaryAttack()
{
	return secondary_attack_cooldown_current == 0;
}

void Player::SecondaryAttack(SpriteAnimation* sprite_animation)
{
	secondary_attack_cooldown_current = secondary_attack_cooldown_duration;

	if (equipped_secondary_weapon)
	{
		if (sprite_animation)
		{
			secondary_weapon_animation_current_frame = 0;
			secondary_weapon_animation = sprite_animation;
			if (!equipped_secondary_weapon->FiresProjectiles())
			{
				directly_secondary_attacking = true;
			}
		}
	}
}

bool Player::IsDirectlySecondaryAttacking()
{
	return this->directly_secondary_attacking;
}

int Player::GetSecondaryAttackDamage()
{
	if (equipped_secondary_weapon)
	{
		return this->equipped_secondary_weapon->GetAttackLevel();
	}
	else
	{
		BOOST_LOG_TRIVIAL(warning) << "Player's attack damage queried when no secondary weapon was equipped. Returning 1";
		return 1;
	}
}

double Player::GetShieldDistanceFromPlayer()
{
	return shield_distance_from_player;
}

bool Player::IsReadyToPrimaryDefend()
{
	if (IsPrimaryDefending())
	{
		return false;
	}
	if (equipped_primary_weapon == nullptr)
	{
		return false;
	}
	if (equipped_primary_weapon->GetWeaponType() != weapon_type_shield)
	{
		return false;
	}
	if (primary_defend_cooldown_current > 0)
	{
		return false;
	}
	return true;
}

bool Player::IsPrimaryDefending()
{
	return directly_primary_defending;
}

void Player::PrimaryDefend()
{
	if (equipped_primary_weapon == nullptr)
	{
		BOOST_LOG_TRIVIAL(warning) << "Attempt to use primary weapon slot to defend when player has nothing equipped!";
		return;
	}
	if (equipped_primary_weapon->GetDefenseRecoveryPeriod() > 0)
	{
		primary_defend_cooldown_current = 0;
		primary_defending_angle = GetPointingAngle();
		directly_primary_defending = true;
	}
}

void Player::StopPrimaryDefend()
{
	primary_defend_cooldown_current = 0;
	directly_primary_defending = false;
}

bool Player::PrimaryDefendBlock(int damage)
{
	if (equipped_primary_weapon == nullptr)
	{
		BOOST_LOG_TRIVIAL(warning) << "Attempt to use primary weapon slot to defend when player has nothing equipped!";
		return false;
	}
	if (damage > equipped_primary_weapon->GetDefenseDamageThreshold())
	{
		StopPrimaryDefend();
		primary_attack_cooldown_current = equipped_primary_weapon->GetDefenseRecoveryPeriod();
		return true;
	}
	return false;
}

double Player::GetPrimaryDefendingAngle()
{
	return primary_defending_angle;
}

bool Player::IsReadyToSecondaryDefend()
{
	if (IsSecondaryDefending())
	{
		return false;
	}
	if (equipped_secondary_weapon == nullptr)
	{
		return false;
	}
	if (equipped_secondary_weapon->GetWeaponType() != weapon_type_shield)
	{
		return false;
	}
	if (secondary_defend_cooldown_current > 0)
	{
		return false;
	}
	return true;
}

bool Player::IsSecondaryDefending()
{
	return directly_secondary_defending;
}

void Player::SecondaryDefend()
{
	if (equipped_secondary_weapon == nullptr)
	{
		BOOST_LOG_TRIVIAL(warning) << "Attempt to use secondary weapon slot to defend when player has nothing equipped!";
		return;
	}
	if (equipped_secondary_weapon->GetDefenseRecoveryPeriod() > 0)
	{
		secondary_defend_cooldown_current = 0;
		secondary_defending_angle = GetPointingAngle();
		directly_secondary_defending = true;
	}
}

void Player::StopSecondaryDefend()
{
	secondary_defend_cooldown_current = 0;
	directly_secondary_defending = false;
}

bool Player::SecondaryDefendBlock(int damage)
{
	if (equipped_secondary_weapon == nullptr)
	{
		BOOST_LOG_TRIVIAL(warning) << "Attempt to use secondary weapon slot to defend when player has nothing equipped!";
		return false;
	}
	if (damage > equipped_secondary_weapon->GetDefenseDamageThreshold())
	{
		StopSecondaryDefend();
		secondary_attack_cooldown_current = equipped_secondary_weapon->GetDefenseRecoveryPeriod();
		return true;
	}
	return false;
}

double Player::GetSecondaryDefendingAngle()
{
	return secondary_defending_angle;
}

bool Player::IsReadyToDrinkPotion()
{
	return potion_cooldown_current == 0;
}

void Player::DrinkHealthPotion()
{
	potion_cooldown_current = potion_cooldown_duration;

	int index = -1;
	for (std::vector<std::shared_ptr<Item>>::iterator it = items.begin(); it < items.end(); it++)
	{
		if ((*it)->GetItemType() == item_type_potion)
		{
			PotionItem* potion = (PotionItem*)(*it).get();
			if (potion->GetPotionType() == potion_type_healing)
			{
				index = std::find(items.begin(), items.end(), (*it)) - items.begin();;
				break;
			}
		}
	}

	if (index == -1)
	{
		BOOST_LOG_TRIVIAL(warning) << "Could not remove a health potion from player - found none in items!";
		return;
	}
	else
	{
		this->SetHealth(std::min(this->GetMaxHealth(), this->GetHealth() + kPotionHealingAmount));
		BOOST_LOG_TRIVIAL(debug) << "Deleting health potion from inventory at index " << index;
		items.erase(items.begin() + index);
	}
}

double Player::GetXP()
{
	return this->xp;
}

void Player::SetXP(double xp)
{
	this->xp = floor(xp);
}

bool Player::AddXP(double xp)
{
	double xp_to_allocate = xp;
	if (xp_to_allocate >= this->GetXPForNextLevel() && this->GetXPForNextLevel() > 0)
	{
		while (xp_to_allocate >= this->GetXPForNextLevel() && this->GetXPForNextLevel() > 0)
		{
			xp_to_allocate -= this->GetXPForNextLevel();
			this->xp += this->GetXPForNextLevel();
			this->LevelUp();
		}
		return true;
	}
	else
	{
		this->xp = floor(this->xp + xp);
		return false;
	}
}

double Player::GetXPForNextLevel()
{
	return this->xp_needed_for_level(this->level + 1) - this->xp;
}

double Player::GetGold()
{
	return gold;
}

void Player::SetGold(double gold)
{
	this->gold = floor(gold);
}

void Player::AddGold(double gold)
{
	this->gold = floor(this->gold + gold);
}

int Player::GetLevel()
{
	return level;
}

double Player::GetHealth()
{
	return current_health;
}

double Player::GetMaxHealth()
{
	return max_health;
}

bool Player::SetHealth(double health)
{
	this->current_health = fmax(0, health);
	if (this->current_health == 0 && alive)
	{
		alive = false;

		// Cancel current attack immediately
		primary_weapon_animation = nullptr;
		directly_primary_attacking = false;

		return true;
	}
	return false;
}

void Player::SetMaxHealth(double max_health)
{
	if (floor(max_health) <= this->GetMaxHealth())
	{
		BOOST_LOG_TRIVIAL(warning) << "Trying to set player max health to a value not greater than current max health, ignoring";
	}
	else
	{
		this->max_health = floor(max_health);
	}
}

bool Player::ApplyDamage(double damage)
{
	double actual_damage = fmax(1, damage - this->GetArmourLevel());
	return this->ApplyRawDamage(actual_damage);
}

bool Player::ApplyRawDamage(double damage)
{
	this->current_health = fmax(0, this->current_health - damage);
	if (this->current_health == 0 && alive)
	{
		alive = false;
		return true;
	}
	else {
		current_invincibility_frames = total_invincibility_frames;
		return false;
	}
}

bool Player::IsAlive()
{
	return alive;
}

bool Player::IsInInvincibilityFrames()
{
	return current_invincibility_frames > 0;
}

double Player::xp_needed_for_level(double level)
{
	if (level == 2)
	{
		return 100;
	}
	return floor(pow(100 * (level - 1), 1.1));
}

void Player::LevelUp()
{
	this->level += 1;
	double health_increase_factor = 0.03;
	if (this->level >= 80)
	{
		health_increase_factor = 0.02;
	}
	double health_increase = std::max(5.0, this->GetMaxHealth() * health_increase_factor);
	this->SetMaxHealth(this->GetMaxHealth() + health_increase);
	this->SetHealth(std::min(this->GetMaxHealth(), this->GetHealth() + health_increase));
	BOOST_LOG_TRIVIAL(info) << "LEVEL UP! Level: " << this->GetLevel() << ", Max Health: " << this->GetMaxHealth();
}

double Player::SellingPriceFor(std::shared_ptr<Item> item)
{
	return round(item->GetValue() * this->mercantile_ability);
}

double Player::BuyingPriceFor(std::shared_ptr<Item> item)
{
	return round(item->GetValue() / this->mercantile_ability);
}

std::vector<std::shared_ptr<Item>> Player::GetItems()
{
	return this->items;
}

std::vector<std::shared_ptr<Item>> Player::GetItems(ItemType item_type)
{
	std::vector<std::shared_ptr<Item>> result;
	for (std::vector<std::shared_ptr<Item>>::iterator it = items.begin(); it < items.end(); it++)
	{
		if ((*it)->GetItemType() == item_type)
		{
			result.push_back(*it);
		}
	}
	return result;
}

void Player::SetItems(std::vector<std::shared_ptr<Item>> items)
{
	this->items = items;
}

void Player::AddItem(std::shared_ptr<Item> item)
{
	this->items.push_back(item);
}

void Player::RemoveItem(std::shared_ptr<Item> item)
{
	items.erase(std::remove_if(items.begin(), items.end(), [item](std::shared_ptr<Item> i) { return i == item; }), items.end());
}

bool Player::HasQuestItem(QuestItem quest_item_id)
{
	for (std::vector<std::shared_ptr<Item>>::iterator it = items.begin(); it < items.end(); it++)
	{
		if ((*it)->GetQuestItemId() == quest_item_id)
		{
			return true;
		}
	}
	return false;
}

void Player::RemoveQuestItem(QuestItem quest_item_id)
{
	items.erase(std::remove_if(items.begin(), items.end(),
		[quest_item_id](std::shared_ptr<Item> &item) { return item->GetQuestItemId() == quest_item_id; }), items.end());
}

int Player::GetNumberOfHealthPotions()
{
	int result = 0;

	for (std::vector<std::shared_ptr<Item>>::iterator it = items.begin(); it < items.end(); it++)
	{
		if ((*it)->GetItemType() == item_type_potion)
		{
			PotionItem* potion = (PotionItem*)(*it).get();
			if (potion->GetPotionType() == potion_type_healing)
			{
				++result;
			}
		}
	}

	return result;
}

void Player::EquipItemInSlot(EquipSlot slot, std::shared_ptr<Item> item)
{
	switch (slot)
	{
	case equip_slot_armour:
		if (item->GetItemType() == item_type_armour)
		{
			AddItem(GetEquippedArmour());
			SetEquippedArmour(std::static_pointer_cast<ArmourItem>(item));
			RemoveItem(item);
		}
		else
		{
			BOOST_LOG_TRIVIAL(warning) << "Attempted to equip '" << item->GetName() << "' as armour but it is not armour.";
		}
		break;
	case equip_slot_primary_weapon:
		if (item->GetItemType() == item_type_weapon)
		{
			AddItem(GetPrimaryEquippedWeapon());
			SetPrimaryEquippedWeapon(std::static_pointer_cast<WeaponItem>(item));
			RemoveItem(item);
		}
		else
		{
			BOOST_LOG_TRIVIAL(warning) << "Attempted to equip '" << item->GetName() << "' as primary weapon but it is not a weapon.";
		}
		break;

	case equip_slot_secondary_weapon:
		if (item->GetItemType() == item_type_weapon)
		{
			AddItem(GetSecondaryEquippedWeapon());
			SetSecondaryEquippedWeapon(std::static_pointer_cast<WeaponItem>(item));
			RemoveItem(item);
		}
		else
		{
			BOOST_LOG_TRIVIAL(warning) << "Attempted to equip '" << item->GetName() << "' as secondary weapon but it is not a weapon.";
		}
		break;
	default:
		BOOST_LOG_TRIVIAL(warning) << "Player unable to equip item to slot type '" << slot << "'";
	}
}


std::shared_ptr<ArmourItem> Player::GetEquippedArmour()
{
	return this->equipped_armour;
}

void Player::SetEquippedArmour(std::shared_ptr<ArmourItem> armour)
{
	this->equipped_armour = armour;
}

int Player::GetArmourLevel()
{
	return this->equipped_armour->GetArmourLevel();
}

std::shared_ptr<WeaponItem> Player::GetPrimaryEquippedWeapon()
{
	return this->equipped_primary_weapon;
}

void Player::SetPrimaryEquippedWeapon(std::shared_ptr<WeaponItem> weapon)
{
	this->equipped_primary_weapon = weapon;
}

std::shared_ptr<WeaponItem> Player::GetSecondaryEquippedWeapon()
{
	return this->equipped_secondary_weapon;
}

void Player::SetSecondaryEquippedWeapon(std::shared_ptr<WeaponItem> weapon)
{
	this->equipped_secondary_weapon = weapon;
}

SpriteAnimation* Player::GetPrimaryWeaponAnimation()
{
	return this->primary_weapon_animation;
}

int Player::GetPrimaryCurrentWeaponAnimationFrame()
{
	return this->primary_weapon_animation_current_frame;
}

SpriteAnimation* Player::GetSecondaryWeaponAnimation()
{
	return this->secondary_weapon_animation;
}

int Player::GetSecondaryCurrentWeaponAnimationFrame()
{
	return this->secondary_weapon_animation_current_frame;
}

