#include "TownMenu.h"
#include "common.cpp"


TownMenu::TownMenu()
{
}

TownMenu::TownMenu(std::string aTitle, SDL_Texture* aTitleTexture)
{
	title = aTitle;
	string_texture_menu_title = aTitleTexture;
}


TownMenu::~TownMenu()
{
}

void TownMenu::AddOption(TownMenuOption option)
{
	options.push_back(option);
}

std::vector<TownMenuOption>* TownMenu::GetOptions()
{
	return &options;
}

SDL_Texture* TownMenu::GetTitleTexture()
{
	return string_texture_menu_title;
}
