#include "DungeonMap.h"
#include "common.cpp"
#include <math.h>
#include <vector>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;

DungeonMap::DungeonMap()
{
	// Map file names to map types
	DungeonMapFileNames[forest1] = kAssetDir + "\\maps\\Forest.tmx";
	DungeonMapFileNames[forest2] = kAssetDir + "\\maps\\Forest2.tmx";
	DungeonMapFileNames[forest3] = kAssetDir + "\\maps\\Forest3.tmx";
	DungeonMapFileNames[graveyard] = kAssetDir + "\\maps\\Graveyard.tmx";
	DungeonMapFileNames[caves] = kAssetDir + "\\maps\\Cave1.tmx";

	// Set enemy generators possible to randomly appear in each map type
	enemy_generators_for_map_type[forest1] = { yurt, tree, stump };
	enemy_generators_for_map_type[forest2] = { yurt, tree, stump };
	enemy_generators_for_map_type[forest3] = { yurt, tree, stump };
	enemy_generators_for_map_type[graveyard] = { bonepile, grave };
	enemy_generators_for_map_type[caves] = { bonepile, hole, rock_mound_small, rock_mound };

	clutter_groups_for_map_type[forest1] = { clutter_group_basic_wares };
	clutter_groups_for_map_type[forest2] = { clutter_group_basic_wares };
	clutter_groups_for_map_type[forest3] = { clutter_group_basic_wares };
	clutter_groups_for_map_type[graveyard] = { clutter_group_funerary };
	clutter_groups_for_map_type[caves] = { clutter_group_basic_wares };
}


DungeonMap::~DungeonMap()
{
	BOOST_LOG_TRIVIAL(info) << "Deleting dungeon map";
	if (tmx_map != nullptr)
	{
		BOOST_LOG_TRIVIAL(debug) << "About to delete tmx_map for DungeonMap";
		delete(tmx_map);
	}
}

void DungeonMap::LoadTmxMap(DungeonMapType map_type, Quest& quest)
{
	BOOST_LOG_TRIVIAL(debug) << "Loading map of map type '" <<  map_type << "' for quest '" << quest.GetTitle() << "'";
	this->map_type = map_type;

	// Default fog is for outdoors, closer fog indoors.
	if (map_type == caves)
	{
		fog_fade_radius = 400;
		fog_clear_radius = 50;
	}

	tmx_map = new TmxMap(DungeonMapFileNames[map_type]);
	w = tmx_map->GetWidth();
	h = tmx_map->GetHeight();
	tileset_columns = tmx_map->GetTilesetColumnCount();
	tile_size = tmx_map->GetTileSize();

	std::pair<int, int> starting_coords = *(tmx_map->PickRandomStartPoint());
	start_x = starting_coords.first;
	start_y = starting_coords.second;

	if (tmx_map->HasExitTo(quest.GetDestination()))
	{
		std::pair<int, int> exit_coords = *(tmx_map->GetExitPointForDestination(quest.GetDestination()));
		exit_x = exit_coords.first;
		exit_y = exit_coords.second;
		exit_destination = quest.GetDestination();
	}
	else
	{
		std::pair<int, int> exit_coords = *(tmx_map->PickRandomExitPoint());
		exit_x = exit_coords.first;
		exit_y = exit_coords.second;
	}

	// Create enemy generators for possible locations
	std::vector<std::pair<int, int>*> enemy_generator_locations = tmx_map->GetEnemySpawnPoints();
	for (std::vector<std::pair<int, int>*>::iterator it = enemy_generator_locations.begin(); it < enemy_generator_locations.end(); it++)
	{
		EnemyGeneratorType type = enemy_generators_for_map_type[map_type].at(rand() % enemy_generators_for_map_type[map_type].size());
		BOOST_LOG_TRIVIAL(debug) << "Adding enemy generator of type " << type << " to map at (" << (*it)->first << "," << (*it)->second << ")";
		enemy_generators.push_back(new EnemyGenerator(type, (*it)->first, (*it)->second));
	}

	// Add clutter at clutter points
	std::vector<std::pair<int, int>*> clutter_locations = tmx_map->GetClutterPoints();
	for (std::vector<std::pair<int, int>*>::iterator it = clutter_locations.begin(); it < clutter_locations.end(); it++)
	{ 
		if (RandomDoubleZeroToOne() < CHANCE_CLUTTER_SPOT_POPULATED)
		{
			std::vector<ClutterGroup> possible_clutter_types = clutter_groups_for_map_type[this->GetDungeonMapType()];
			ClutterGroup chosen_clutter_group = possible_clutter_types.at(rand() % possible_clutter_types.size());
			// TODO: Create clutter groups for clutter items that belong together, and how they should be laid out
			switch (chosen_clutter_group)
			{
				case (clutter_group_basic_wares) :
				{
					// 25% chance of a medium crate
					if (rand() % 4 == 0)
					{
						auto clutter = std::make_shared<ClutterItem>(medium_crate);
						clutter->SetX((*it)->first);
						clutter->SetY((*it)->second);
						clutter_items.push_back(clutter);
					}
					else // Barrels might appear if no medium crate
					{
						int num_of_barrels = (rand() % 3);
						for (int i = 0; i < num_of_barrels; i++)
						{
							auto clutter = std::make_shared<ClutterItem>(barrel);
							std::pair<int, int> clutter_position = ResultingCoordsFromAttemptedMove((*it)->first, (*it)->second,
								(*it)->first + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
								(*it)->second + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
								clutter->GetWidth(), clutter->GetHeight());
							clutter->SetX(clutter_position.first);
							clutter->SetY(clutter_position.second);
							clutter_items.push_back(clutter);
						}
					}
					int num_of_small_crates = (rand() % 6);
					for (int i = 0; i < num_of_small_crates; i++)
					{
						auto clutter = std::make_shared<ClutterItem>(small_crate);
						std::pair<int, int> clutter_position = ResultingCoordsFromAttemptedMove((*it)->first, (*it)->second,
							(*it)->first + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							(*it)->second + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							clutter->GetWidth(), clutter->GetHeight());
						clutter->SetX(clutter_position.first);
						clutter->SetY(clutter_position.second);
						clutter_items.push_back(clutter);
					}
					int num_of_vases = (rand() % 6);
					for (int i = 0; i < num_of_vases; i++)
					{
						ClutterType vase_type = big_vase;
						if (rand() % 2 == 0)
						{
							vase_type = small_vase;
						}
						auto clutter = std::make_shared<ClutterItem>(vase_type);
						std::pair<int, int> clutter_position = ResultingCoordsFromAttemptedMove((*it)->first, (*it)->second,
							(*it)->first + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							(*it)->second + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							clutter->GetWidth(), clutter->GetHeight());
						clutter->SetX(clutter_position.first);
						clutter->SetY(clutter_position.second);
						clutter_items.push_back(clutter);
					}
					break;
				}
				case (clutter_group_funerary) :
				{
					int num_of_vases = (rand() % 6) + 1;
					for (int i = 0; i < num_of_vases; i++)
					{
						ClutterType vase_type = small_funerary_vase;
						if (rand() % 3 == 0)
						{
							vase_type = big_funerary_vase;
						}
						auto clutter = std::make_shared<ClutterItem>(vase_type);
						std::pair<int, int> clutter_position = ResultingCoordsFromAttemptedMove((*it)->first, (*it)->second,
							(*it)->first + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							(*it)->second + (rand() % (CLUTTER_SPREAD_RADIUS * 2) - CLUTTER_SPREAD_RADIUS),
							clutter->GetWidth(), clutter->GetHeight());
						clutter->SetX(clutter_position.first);
						clutter->SetY(clutter_position.second);
						clutter_items.push_back(clutter);
					}
					// 33% chance of a coffin
					if (rand() % 3 == 0)
					{
						auto clutter = std::make_shared<ClutterItem>(coffin);
						clutter->SetX((*it)->first);
						clutter->SetY((*it)->second);
						clutter_items.push_back(clutter);
					}
					break;
				}
				default :
					BOOST_LOG_TRIVIAL(warning) << "Defaulted when picking clutter type '" << chosen_clutter_group << "' - not adding clutter.";
			}
		}
	}

	BOOST_LOG_TRIVIAL(debug) << "Finished LoadTmxMap";
}

void DungeonMap::SetTmxMap(TmxMap* tmx_map)
{
	this->map_type = forest1;

	this->tmx_map = tmx_map;
	w = tmx_map->GetWidth();
	h = tmx_map->GetHeight();
	tileset_columns = tmx_map->GetTilesetColumnCount();
	tile_size = tmx_map->GetTileSize();

	std::pair<int, int> starting_coords = *(tmx_map->PickRandomStartPoint());
	start_x = starting_coords.first;
	start_y = starting_coords.second;

	std::pair<int, int> exit_coords = *(tmx_map->PickRandomExitPoint());
	exit_x = exit_coords.first;
	exit_y = exit_coords.second;

	// Create enemy generators for possible locations
	std::vector<std::pair<int, int>*> enemy_generator_locations = tmx_map->GetEnemySpawnPoints();
	for (std::vector<std::pair<int, int>*>::iterator it = enemy_generator_locations.begin(); it < enemy_generator_locations.end(); it++)
	{
		EnemyGeneratorType type = enemy_generators_for_map_type[map_type].at(rand() % enemy_generators_for_map_type[map_type].size());
		BOOST_LOG_TRIVIAL(debug) << "Adding enemy generator of type " << type << " to map at (" << (*it)->first << "," << (*it)->second << ")";
		enemy_generators.push_back(new EnemyGenerator(type, (*it)->first, (*it)->second));
	}
}

void DungeonMap::LoadTextures(SDL_Renderer *ren)
{
	std::string wall_texture_filename = tmx_map->GetTilemapFilename();
	if (wall_texture_filename.empty())
	{
		BOOST_LOG_TRIVIAL(error) << "Tile map does not have a filename for its tileset";
		throw "Tile map does not have a filename for its tileset";
	}

	texture_walls = LoadTextureFromPng(ren, kAssetDir + "\\" + wall_texture_filename);

	if (texture_walls == nullptr)
	{
		BOOST_LOG_TRIVIAL(error) << "Failed to load " << wall_texture_filename;
		throw "Failed to load " + wall_texture_filename;
	}
}

void DungeonMap::Render(SDL_Renderer *ren, int centre_pixel_x, int centre_pixel_y)
{
	int render_width = 0;
	int render_height = 0;
	SDL_GetRendererOutputSize(ren, &render_width, &render_height);
	int full_tiles_to_render_x = render_width / tile_size;
	int full_tiles_to_render_y = render_height / tile_size;
	// The nominal centre point, if tile 0,0 was at pixel 0,0
	int nominal_centre_x = (render_width / 2);
	int nominal_centre_y = (render_height / 2);
	// The leftmost full tile to render
	int min_tile_x = (int)fmax(0, (centre_pixel_x / tile_size) - (full_tiles_to_render_x / 2));
	// The rightmost full tile to render
	int max_tile_x = (int)fmin(w, (centre_pixel_x / tile_size) + (full_tiles_to_render_x / 2) + 1);
	// The topmost full tile to render
	int min_tile_y = (int)fmax(0, (centre_pixel_y / tile_size) - (full_tiles_to_render_y / 2));
	// The bottommost full tile to render
	int max_tile_y = (int)fmin(h, (centre_pixel_y / tile_size) + (full_tiles_to_render_y / 2) + 1);

	for (int i = min_tile_x; i < max_tile_x; i++)
	{
		for (int j = min_tile_y; j < max_tile_y; j++)
		{
			SDL_Rect rect_to_draw;
			int tile_to_draw = tmx_map->GetVisibleTiles()[(j * w) + i];
			rect_to_draw.x = (tile_to_draw % tileset_columns) * tile_size;
			rect_to_draw.y = (tile_to_draw / tileset_columns) * tile_size;
			rect_to_draw.w = tile_size;
			rect_to_draw.h = tile_size;

			SDL_Rect dest_rect = SDL_Rect{ i * MAP_TILE_SIZE + (nominal_centre_x - centre_pixel_x), j * MAP_TILE_SIZE + (nominal_centre_y - centre_pixel_y), MAP_TILE_SIZE, MAP_TILE_SIZE };
			SDL_RenderCopy(ren, texture_walls, &rect_to_draw, &dest_rect);
		}
	}
}

void DungeonMap::RenderFogOfWar(SDL_Renderer *ren, SDL_Texture* tex, int centre_pixel_x, int centre_pixel_y)
{
	int render_width = 0;
	int render_height = 0;
	SDL_GetRendererOutputSize(ren, &render_width, &render_height);
	int full_tiles_to_render_x = render_width / tile_size;
	int full_tiles_to_render_y = render_height / tile_size;
	// The nominal centre point, if tile 0,0 was at pixel 0,0
	int nominal_centre_x = (render_width / 2);
	int nominal_centre_y = (render_height / 2);
	// The leftmost full tile to render
	int min_tile_x = (int)fmax(0, (centre_pixel_x / tile_size) - (full_tiles_to_render_x / 2));
	// The rightmost full tile to render
	int max_tile_x = (int)fmin(w, (centre_pixel_x / tile_size) + (full_tiles_to_render_x / 2) + 1);
	// The topmost full tile to render
	int min_tile_y = (int)fmax(0, (centre_pixel_y / tile_size) - (full_tiles_to_render_y / 2));
	// The bottommost full tile to render
	int max_tile_y = (int)fmin(h, (centre_pixel_y / tile_size) + (full_tiles_to_render_y / 2) + 1);

	SDL_SetTextureColorMod(tex, 0, 0, 0);
	
	for (int i = min_tile_x; i < max_tile_x; i++)
	{
		for (int j = min_tile_y; j < max_tile_y; j++)
		{
			SDL_Rect rect_to_draw;
			int opacity = tmx_map->GetFogTiles()[(j * w) + i];
			SDL_SetTextureAlphaMod(tex, opacity);
			rect_to_draw.x = 0;
			rect_to_draw.y = 0;
			rect_to_draw.w = tile_size;
			rect_to_draw.h = tile_size;

			SDL_Rect dest_rect = SDL_Rect{ i * MAP_TILE_SIZE + (nominal_centre_x - centre_pixel_x), j * MAP_TILE_SIZE + (nominal_centre_y - centre_pixel_y), MAP_TILE_SIZE, MAP_TILE_SIZE };
			SDL_RenderCopy(ren, tex, &rect_to_draw, &dest_rect);
		}
	}

	SDL_SetTextureAlphaMod(tex, 255);
	SDL_SetTextureColorMod(tex, 255,255,255);
}

int DungeonMap::GetStartX()
{
	return start_x;
}

int DungeonMap::GetStartY()
{
	return start_y;
}

int DungeonMap::GetExitX()
{
	return exit_x;
}

int DungeonMap::GetExitY()
{
	return exit_y;
}

int DungeonMap::GetTileSize()
{
	return tile_size;
}

DungeonMapType DungeonMap::GetDungeonMapType()
{
	return map_type;
}

std::pair<double, double> DungeonMap::ResultingCoordsFromAttemptedMove(double current_x, double current_y, double desired_x, double desired_y, int w, int h, bool floating)
{
	TileType target_tile = tmx_map->GetSolidWallTileTypeAt(desired_x, desired_y);

	// Calculate the offset inside the target_tile that the entity wants to be at
	double int_part, fract_part;
	fract_part = modf(desired_x, &int_part);
	double target_tile_x_offset = ((int)int_part % tmx_map->GetTileSize()) + fract_part;
	fract_part = modf(desired_y, &int_part);
	double target_tile_y_offset = ((int)int_part % tmx_map->GetTileSize()) + fract_part;
	int target_tile_left = (int)(desired_x - target_tile_x_offset);
	int target_tile_top = (int)(desired_y - target_tile_y_offset);
	// If the object is floating and the target is a low tile they may move to desired coords
	if (floating && target_tile >= low_edge_top && target_tile <= low_jutting_corner_top_right)
	{
		return std::pair<double, double>(desired_x, desired_y);
	}
	switch (target_tile)
	{
	    case empty:
			return std::pair<double, double>(desired_x, desired_y);
		case edge_top:
		case low_edge_top:
			// If approaching from below; the object just needs to be clamped to the empty bottom of the tile
			if (current_y >= target_tile_top + (tmx_map->GetTileSize() / 2))
			{
				return std::pair<double, double>(desired_x, fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
			}
			else
			{
				// If approaching from right, clamp at right side
				if (current_x > target_tile_left + tmx_map->GetTileSize() + (w / 2))
				{
					return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() + (w / 2)), desired_y);
				}
				// If approaching from left, clamp at left side
				if (current_x < target_tile_left - (w / 2))
				{
					return std::pair<double, double>(fmin(desired_x, target_tile_left - (w / 2)), desired_y);
				}
				// Else the point where the object intersects with the bottom of the tile needs to be determined
				// TODO: Just halting the object is a cheap hack
				return std::pair<double, double>(current_x, current_y);
			}
		case solid:
		case low_solid:
			return std::pair<double, double>(current_x, current_y);
		case edge_right:
		case low_edge_right:
			// If approaching from the left; the object just needs to be clamped to the empty LHS of the tile
			if (current_x <= target_tile_left + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(fmin(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)), desired_y);
			}
			else
			{
				// Else the point where the object intersects with the RHS of the tile needs to be determined
				// TODO: Just halting the object is a cheap hack
				return std::pair<double, double>(current_x, current_y);
			}
		case edge_left:
		case low_edge_left:
			// If approaching from the right; the object just needs to be clamped to the empty RHS of the tile
			if (current_x >= target_tile_left + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)), desired_y);
			}
			else
			{
				// TODO: Just halting the object is a cheap hack
				return std::pair<double, double>(current_x, current_y);
			}
		case edge_bottom:
		case low_edge_bottom:
			// TODO: Handle walking at the tile from the sides. Currently character may jump slightly to a space above the tile
			// If approaching from the top; go no lower than the vertical midpoint of the tile
			if (current_y <= target_tile_top + (tmx_map->GetTileSize() / 2) - (h / 2))
			{
				return std::pair<double, double>(desired_x, fmin(desired_y, target_tile_top + (tmx_map->GetTileSize() / 2) - (h / 2)));
			}
			else
			{
				// If approaching from the left
				if (current_x < target_tile_left)
				{
					return std::pair<double, double>(target_tile_left - (w / 2), desired_y);
				}

				// If approaching from the right
				if (current_x > target_tile_left + tmx_map->GetTileSize())
				{
					return std::pair<double, double>(target_tile_left + tmx_map->GetTileSize() + (w / 2), desired_y);
				}

				// If approaching from below; go no higher than the bottom of the tile
				return std::pair<double, double>(desired_x, fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
			}

		case corner_bottom_left:
		case low_corner_bottom_left:
			return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)),
				                                 fmin(desired_y, target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2)));
		case corner_bottom_right:
		case low_corner_bottom_right:
			return std::pair<double, double>(fmin(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)),
				                                 fmin(desired_y, target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2)));
		case corner_top_left:
		case low_corner_top_left:
			if (current_y <= target_tile_top - (h / 2))
			{
				// If moving from left and above of the tile
				if (current_x < target_tile_left - (w / 2))
				{
					double angle_from_current_to_tile = atan2(current_y - target_tile_top, current_x - target_tile_left);
					double angle_from_current_to_desired = atan2(current_y - desired_y, current_x - desired_x);
					// If desired Y would collide with top of tile...
					if (angle_from_current_to_desired < angle_from_current_to_tile)
					{
						return std::pair<double, double>(desired_x, target_tile_top - (h / 2));
					}
					else
					{
						// If desired Y would collide with left of tile...
						return std::pair<double, double>(target_tile_left - (w / 2), desired_y);
					}
				}
				// If moving from right and above of the tile
				if (current_x > target_tile_left + tmx_map->GetTileSize() + (w / 2))
				{
					double angle_from_current_to_tile = atan2(current_y - target_tile_top, current_x - target_tile_left + tmx_map->GetWidth());
					double angle_from_current_to_desired = atan2(current_y - desired_y, current_x - desired_x);
					// If desired Y would collide with top of tile...
					if (angle_from_current_to_desired < angle_from_current_to_tile)
					{
						return std::pair<double, double>(desired_x, target_tile_top - (h / 2));
					}
					else
					{
						// If desired Y would collide with right of tile...
						return std::pair<double, double>(target_tile_left - (w / 2), desired_y);
					}
				}
				// If above but not to either side, just clamp to top
				return std::pair<double, double>(desired_x, target_tile_top - (h / 2));
			}
			// If to the left but neither above nor below, just clamp to left
			if (current_x <= target_tile_left && current_y > target_tile_top - (h / 2) && current_y < target_tile_top + tmx_map->GetHeight() - (h / 2))
			{
				return std::pair<double, double>(target_tile_left - (w / 2), desired_y);
			}
			// If below...
			if (current_y >= target_tile_top + tmx_map->GetHeight() - (h / 2))
			{
				// If below and to the left
				if (current_x < target_tile_left)
				{
					double angle_from_current_to_tile = atan2(current_y - target_tile_top + tmx_map->GetHeight(), current_x - target_tile_left);
					double angle_from_current_to_desired = atan2(current_y - desired_y, current_x - desired_x);
					// If angle is less, clamp to left hand side
					return std::pair<double, double>(target_tile_left - (w / 2), desired_y);
					// Else clamp to the bottom 
					return std::pair<double, double>(desired_x, fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
				}
				// If below and to the right
				if (current_x >= target_tile_left + tmx_map->GetTileSize())
				{
					// TODO: Might hit against right or bottom side, for now just clamp inside lower right quarter
					return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)),
						fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
				}
				// If below and neither left nor right
				if (desired_x <= target_tile_left + +tmx_map->GetTileSize() / 2)
				{
					// Clamp to bottom
					return std::pair<double, double>(desired_x, fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
				}
				else
				{
					return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)),
						fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
				}
			}
			return std::pair<double, double>(fmax(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)),
				                                 fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
		case corner_top_right:
		case low_corner_top_right:
			return std::pair<double, double>(fmin(desired_x, target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)),
				                                 fmax(desired_y, target_tile_top + tmx_map->GetTileSize() / 2));
		case jutting_corner_bottom_left:
		case low_jutting_corner_bottom_left:
			// Can move to desired pixel unless it's in the bottom left quarter
			if (desired_x > (target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2)) || desired_y < target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2))
			{
				return std::pair<double, double>(desired_x,desired_y);
			}
			// If originating from top half of tile, limit vertically 
			if (current_y <= target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2))
			{
				return std::pair<double, double>(desired_x, target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2));
			}
			else
			{
				return std::pair<double, double>(target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2), desired_y);
			}
		case jutting_corner_bottom_right:
		case low_jutting_corner_bottom_right:
			// Can move to the desired pixel unless it's in the bottom right corner
			if (desired_x < (target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)) || desired_y < target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2))
			{
				return std::pair<double, double>(desired_x, desired_y);
			}
			// If originating from top half of tile, limit vertically 
			if (current_y <= target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2))
			{
				return std::pair<double, double>(desired_x, target_tile_top + tmx_map->GetTileSize() / 2 - (h / 2));
			}
			else
			{
				return std::pair<double, double>(target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2), desired_y);
			}
		case jutting_corner_top_left:
		case low_jutting_corner_top_left:
			// Can move to the desired pixel unless it's in the top left quarter
			if (desired_x > (target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)) || desired_y > target_tile_top + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(desired_x, desired_y);
			}
			// If originating from bottom half of tile, limit vertically
			if (current_y >= target_tile_top + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(desired_x, target_tile_top + tmx_map->GetTileSize() / 2);
			}
			else
			{
				return std::pair<double, double>(target_tile_left + tmx_map->GetTileSize() / 2 + (w / 2), desired_y);
			}
		case jutting_corner_top_right:
		case low_jutting_corner_top_right:
			// Can move to the desired pixel unless it's in the top right quarter
			if (desired_x < (target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2)) || desired_y > target_tile_top + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(desired_x, desired_y);
			}
			// If originating from bottom half of tile, limit vertically
			if (current_y >= target_tile_top + tmx_map->GetTileSize() / 2)
			{
				return std::pair<double, double>(desired_x, target_tile_top + tmx_map->GetTileSize() / 2);
			}
			else
			{
				return std::pair<double, double>(target_tile_left + tmx_map->GetTileSize() / 2 - (w / 2), desired_y);
			}
		default:
			BOOST_LOG_TRIVIAL(error) << "Defaulted when switching on TileType. TileType was '" << target_tile << "'. Returning current co-ords.";
			return std::pair<double, double>(current_x, current_y);
	}
}

std::pair<double, double> DungeonMap::ResultingCoordsFromAttemptedMove(double current_x, double current_y, double desired_x, double desired_y, int w, int h)
{
	return this->ResultingCoordsFromAttemptedMove(current_x, current_y, desired_x, desired_y, w, h, false);
}

std::pair<double, double> DungeonMap::EmptySpaceNearTo(double desired_x, double desired_y, double max_distance)
{
	BOOST_LOG_TRIVIAL(debug) << "About to find an empty space on the map near to (" << desired_x << "," << desired_y << ") within max distance " << max_distance;
	enum SearchDirection { right, up_right, up, up_left, left, down_left, down, down_right };
	// Search for a candidate space starting to the right
	int search_distance = 0; // Will increase as soon as the search loop starts
	SearchDirection direction = right;
	double candidate_x = desired_x + search_distance;
	double candidate_y = desired_y;
	while (true)
	{
		switch (direction)
		{
		case(right) :
			BOOST_LOG_TRIVIAL(trace) << "Starting a new orbit round desired location, so increasing distance by " << tmx_map->GetTileSize();
			search_distance += tmx_map->GetTileSize();
			direction = up_right;
			candidate_x = desired_x + search_distance;
			candidate_y = desired_y - search_distance;
			break;
		case(up_right) :
			direction = up;
			candidate_x = desired_x;
			candidate_y = desired_y - search_distance;
			break;
		case(up) :
			direction = up_left;
			candidate_x = desired_x - search_distance;
			candidate_y = desired_y - search_distance;
			break;
		case(up_left) :
			direction = left;
			candidate_x = desired_x - search_distance;
			candidate_y = desired_y;
			break;
		case(left) :
			direction = down_left;
			candidate_x = desired_x - search_distance;
			candidate_y = desired_y + search_distance;
			break;
		case(down_left) :
			direction = down;
			candidate_x = desired_x;
			candidate_y = desired_y + search_distance;
			break;
		case(down) :
			direction = down_right;
			candidate_x = desired_x + search_distance;
			candidate_y = desired_y + search_distance;
			break;
		case(down_right) :
			direction = right;
			candidate_x = desired_x + search_distance;
			candidate_y = desired_y;
			break;
		}

		if (search_distance > max_distance) break;

		if (candidate_x > 0 && candidate_x < tmx_map->GetWidth() * tmx_map->GetTileSize()
			&& candidate_y > 0 && candidate_y < tmx_map->GetHeight() * tmx_map->GetTileSize())
		{
			TileType target_tile = tmx_map->GetSolidWallTileTypeAt((int)candidate_x, (int)candidate_y);
			if (target_tile == TileType::solid) continue;
			std::pair<double, double> adjusted_position = ResultingCoordsFromAttemptedMove(desired_x, desired_y, candidate_x, candidate_y, 8, 8);
			if (distance_between(adjusted_position.first, adjusted_position.second, desired_x, desired_y) < 16)
			{
				BOOST_LOG_TRIVIAL(debug) << "DungeonMap::EmptySpaceNearTo Candidate spot (" << adjusted_position.first << "," << adjusted_position.second << ") is too close to the origin object at (" << desired_x << "," << desired_y << ")";
				continue;
			}
			else
			{
				// adjusted_position is a good candidate. Now need to check if it is too close to anything in the map
				for (std::vector<EnemyGenerator*>::iterator it = enemy_generators.begin(); it < enemy_generators.end(); it++)
				{
					if (distance_between(adjusted_position.first, adjusted_position.second, (*it)->GetX(), (*it)->GetY()) < 16)
					{
						BOOST_LOG_TRIVIAL(trace) << "Candidate spot (" << adjusted_position.first << "," << adjusted_position.second << ") is too close to an enemy_generator at (" << desired_x << "," << desired_y << ")";
						continue;
					}
				}
				BOOST_LOG_TRIVIAL(debug) << "Returning free space found at (" << adjusted_position.first << "," << adjusted_position.second << ")";
				return adjusted_position;
			}
		}
		else
		{
			BOOST_LOG_TRIVIAL(trace) << "Candidate spot (" << candidate_x << "," << candidate_y << ") is out of bounds of the map.";
		}
	}
	
	BOOST_LOG_TRIVIAL(warning) << "Could not identify an empty space near to " << desired_x << "," << desired_y << " within distance " << max_distance << ". Returning the original location!";
	return std::pair<double, double>(desired_x, desired_y);
}

void DungeonMap::OpenExitBackToVillageNear(double x, double y)
{
	BOOST_LOG_TRIVIAL(debug) << "Opening an exit back to the village near" << x << "," << y;
	std::pair<double, double> location = EmptySpaceNearTo(x, y, 400);
	exit_x = (int)location.first;
	exit_y = (int)location.second;
	BOOST_LOG_TRIVIAL(debug) << "New exit is at (" << exit_x << "," << exit_y << ")";
	exit_destination = village_maptype;
}

std::vector<EnemyGenerator*> DungeonMap::GetEnemyGenerators()
{
	return enemy_generators;
}

void DungeonMap::RemoveDestroyedEnemyGenerators()
{
	enemy_generators.erase(std::remove_if(enemy_generators.begin(), enemy_generators.end(),
		[](EnemyGenerator* &generator) { return !generator->IsAlive(); }), enemy_generators.end());
}

std::vector<Enemy*>& DungeonMap::GetEnemies()
{
	return enemies;
}

void DungeonMap::AddEnemy(Enemy* enemy)
{
	if ((int)enemies.size() < kMaxEnemiesPerDungeon)
	{
		BOOST_LOG_TRIVIAL(debug) << "Spawning new enemy of type '" << enemy->GetType() << "'";
		enemies.push_back(enemy);
	}
	else
	{
		BOOST_LOG_TRIVIAL(debug) << "Not spawning new enemy - limit of " << kMaxEnemiesPerDungeon << " reached!";
	}
}

void DungeonMap::ClearEnemies()
{
	enemies.clear();
}

void DungeonMap::RemoveDeadEnemies()
{
	enemies.erase(std::remove_if(enemies.begin(), enemies.end(),
		[](Enemy* &enemy) { return !enemy->IsAlive(); }), enemies.end());
}

std::vector<Furniture*> DungeonMap::GetFurniture()
{
	return furniture;
}

void DungeonMap::AddFurniture(Furniture* furniture)
{
	this->furniture.push_back(furniture);
}

void DungeonMap::ClearFurniture()
{
	furniture.clear();
}

std::vector<std::shared_ptr<ClutterItem>> DungeonMap::GetClutter()
{
	return clutter_items;
}

void DungeonMap::RemoveDestroyedClutter()
{
	clutter_items.erase(std::remove_if(clutter_items.begin(), clutter_items.end(),
		[](std::shared_ptr<ClutterItem> &clutter_item) { return !clutter_item->IsAlive(); }), clutter_items.end());
}

std::vector<std::shared_ptr<DroppedItem>> DungeonMap::GetDroppedItems()
{
	return dropped_items;
}

void DungeonMap::AddDroppedItem(std::shared_ptr<DroppedItem> item)
{
	this->dropped_items.push_back(item);
}

void DungeonMap::ClearDroppedItems()
{
	this->dropped_items.clear();
}

void DungeonMap::RemoveCollectedDroppedItems()
{
	dropped_items.erase(std::remove_if(dropped_items.begin(), dropped_items.end(),
		[](std::shared_ptr<DroppedItem> &dropped_item) { return dropped_item->GetIsCollected(); }), dropped_items.end());
}

std::vector<Npc*> DungeonMap::GetNpcs()
{
	return npcs;
}

void DungeonMap::AddNpc(Npc* npc)
{
	this->npcs.push_back(npc);
}

void DungeonMap::ClearNpcs()
{
	npcs.clear();
}

int DungeonMap::GetWidthInPixels()
{
	return w * tile_size;
}

int DungeonMap::GetHeightInPixels()
{
	return h * tile_size;
}

DungeonMapType DungeonMap::GetExitDestinationMap()
{
	return exit_destination;
}

void DungeonMap::UpdateFogOfWar(Player& player)
{
	int dropoff = fog_fade_radius / 128;
	for (int i = 0; i < tmx_map->GetWidth(); i++)
	{
		for (int j = 0; j < tmx_map->GetHeight(); j++)
		{
			double distance = distance_between(player.GetX(), player.GetY(), i * tile_size + (tile_size / 2), j * tile_size + (tile_size / 2));
			if (distance < fog_clear_radius)
			{
				tmx_map->SetFogTileOpacity(i, j, 0);
			}
			else
			{
				if (distance < fog_fade_radius && CanSee(player.GetX(), player.GetY(), i * tile_size + (tile_size / 2), j * tile_size + (tile_size / 2)))
				{
					tmx_map->SetFogTileOpacity(i, j, std::min((unsigned)(distance / dropoff), tmx_map->GetFogTiles()[j * tmx_map->GetWidth() + i]));;
				}
			}
		}
	}
}

bool DungeonMap::CanSee(int fromX, int fromY, int targetX, int targetY)
{
	int x0, y0, x1, y1 = 0;
	bool steep = abs(targetY - fromY) > abs(targetX - fromX);
	if (steep)
	{
		x0 = fromY;
		y0 = fromX;

		x1 = targetY;
		y1 = targetX;
	}
	else
	{
		x0 = fromX;
		y0 = fromY;

		x1 = targetX;
		y1 = targetY;
	}

	if (x0 > x1)
	{
		std::swap(x0, x1);
		std::swap(y0, y1);
	}

	int deltaX = x1 - x0;
	int deltaY = abs(y1 - y0);
	int error = 0;
	int deltaError = deltaY;
	int xStep, yStep = 0;
	int y = y0;
	int x = x0;
	int tmpX, tmpY = 0;

	if (y0 < y1)
	{
		yStep = 1;
	}
	else
	{
		yStep = -1;
	}
	if (x0 < x1)
	{
		xStep = 1;
	}
	else
	{
		xStep = -1;
	}

	while (x != x1)
	{
		x += xStep;
		error += deltaError;

		// If the error exceeds the X delta then move along one on Y axis
		if (2 * error > deltaX)
		{
			y += yStep;
			error -= deltaX;
		}

		if (steep)
		{
			tmpX = y;
			tmpY = x;
		}
		else
		{
			tmpX = x;
			tmpY = y;
		}
		// TODO: Handle edge tiles
		if (tmx_map->GetSolidWallTileTypeAt(tmpX, tmpY) == solid)
		{
			return false;
		}
	}
	// If we got to the end without hitting false, we can see!
	return true;
}