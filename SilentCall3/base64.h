#pragma once
#include <string>

// https://github.com/ReneNyffenegger/cpp-base64/blob/master/base64.h

std::string base64_encode(unsigned char const*, unsigned int len);
std::string base64_decode(std::string const& s);