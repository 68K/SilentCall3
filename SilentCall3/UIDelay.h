#pragma once

/*! \file UIDelay.h
* A class representing a delay on the UI with some cooldown
*/
class UIDelay
{
public:
	UIDelay(int aDuration);
	~UIDelay();

	void Cooldown();

	void Activate();

	bool Ready();

	int GetTicksRemaining();
	int GetDuration();

private:
	int duration = 1;
	int ticks = 0;
};

