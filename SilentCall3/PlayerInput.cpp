#include "PlayerInput.h"
#include <SDL.h>
#include <iostream>

/*! \var The threshold the player needs to push past on a controller to register input */
static const int kAxisDeadzone = 8000;
SDL_GameController *ctrl;

PlayerInput::PlayerInput()
{

}


PlayerInput::~PlayerInput()
{
}

void PlayerInput::InitialiseInputDevices()
{
	BOOST_LOG_TRIVIAL(info) << SDL_NumJoysticks() << " joysticks were found.";

	for (int i = 0; i < SDL_NumJoysticks(); ++i)
	{
		if (SDL_JoystickName(SDL_JoystickOpen(i)))
		{
			BOOST_LOG_TRIVIAL(info) << "Joystick " << i << " is " << SDL_JoystickName(SDL_JoystickOpen(i));
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << "Joystick " << i << " does not return a name";
		}
		if (SDL_IsGameController(i))
		{
			BOOST_LOG_TRIVIAL(info) << "Joystick " << i << " is supported by the game controller interface";
		}
	}
	ctrl = SDL_GameControllerOpen(0);
}

void PlayerInput::UpdatePlayerInput()
{
	if(last_start_pressed && !this->StartPressed())
	{
		last_start_pressed = false;
	}
	if(last_up_pressed && !this->UpPressed())
	{
		last_up_pressed = false;
	}
	if(last_down_pressed && !this->DownPressed())
	{
		last_down_pressed = false;
	}
	if (last_confirm_pressed && !this->ConfirmPressed())
	{
		last_confirm_pressed = false;
	}
}

bool PlayerInput::ExitPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_ESCAPE] != 0);
}

bool PlayerInput::LeftPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_A] != 0) || (SDL_GameControllerGetAxis(ctrl, SDL_CONTROLLER_AXIS_LEFTX) < -kAxisDeadzone);
}

bool PlayerInput::RightPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_D] != 0) || (SDL_GameControllerGetAxis(ctrl, SDL_CONTROLLER_AXIS_LEFTX) > kAxisDeadzone);
}

bool PlayerInput::UpPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if ((state[SDL_SCANCODE_W] != 0) || (SDL_GameControllerGetAxis(ctrl, SDL_CONTROLLER_AXIS_LEFTY) < -kAxisDeadzone))
	{
		last_up_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::UpTapped()
{

	if (!last_up_pressed && this->UpPressed())
	{
		last_up_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::DownPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if ((state[SDL_SCANCODE_S] != 0) || (SDL_GameControllerGetAxis(ctrl, SDL_CONTROLLER_AXIS_LEFTY) > kAxisDeadzone))
	{
		last_down_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::DownTapped()
{
	if (!last_down_pressed && this->DownPressed())
	{
		last_down_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::ConfirmPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if ((state[SDL_SCANCODE_RETURN] != 0) || (state[SDL_SCANCODE_SPACE] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_A) == SDL_PRESSED))
	{
		last_confirm_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::ConfirmTapped()
{
	if (!last_confirm_pressed && this->ConfirmPressed())
	{
		last_confirm_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::StartPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if ((state[SDL_SCANCODE_RETURN] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_START) == SDL_PRESSED))
	{
		last_start_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::StartTapped()
{
	if (!last_start_pressed && this->StartPressed())
	{
		last_start_pressed = true;
		return true;
	}
	return false;
}

bool PlayerInput::CancelPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_DELETE] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_B) == SDL_PRESSED);
}

bool PlayerInput::PrimaryAttackPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_RETURN] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_X) == SDL_PRESSED);
}

bool PlayerInput::SecondaryAttackPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_RSHIFT] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_Y) == SDL_PRESSED);
}

bool PlayerInput::HealthPotionPressed()
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return (state[SDL_SCANCODE_1] != 0) || (SDL_GameControllerGetButton(ctrl, SDL_CONTROLLER_BUTTON_LEFTSHOULDER) == SDL_PRESSED);
}