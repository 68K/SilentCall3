#include "EnemyGenerator.h"



EnemyGenerator::EnemyGenerator()
{
}

EnemyGenerator::EnemyGenerator(EnemyGeneratorType type, int x, int y)
	: Actor()
{
	this->type = type;
	this->SetX(x);
	this->SetY(y);
	switch (type)
	{
	case(yurt) :
		this->SetWidth(64);
		this->SetHeight(64);
		break;
	case(tree):
		this->SetWidth(64);
		this->SetHeight(64);
		break;
	case(stump):
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	case(bonepile) :
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	case(grave) :
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	case(rock_mound) :
		this->SetWidth(64);
		this->SetHeight(64);
		break;
	case(rock_mound_small) :
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	case(hole) :
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	default:
		BOOST_LOG_TRIVIAL(warning) << "Defaulted on enemy generator size for type '" << type << "' - defaulting to 32x32";
		this->SetWidth(32);
		this->SetHeight(32);
		break;
	}
}

EnemyGenerator::~EnemyGenerator()
{
}

EnemyGeneratorType EnemyGenerator::GetEnemyGeneratorType()
{
	return this->type;
}

double EnemyGenerator::GetSpawnRadius()
{
	return this->spawn_radius;
}

void EnemyGenerator::Update()
{
	frames_since_last_spawn++;
	this->ActorUpdate();
}

bool EnemyGenerator::GetIfReadyToGenerateEnemy()
{
	return this->IsAlive() && (frames_since_last_spawn >= frames_between_spawns);
}

EnemyType EnemyGenerator::GenerateEnemy()
{
	frames_since_last_spawn = 0;
	return goblin;
}