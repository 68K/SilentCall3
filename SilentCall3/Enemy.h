#include "src/Actor.h"
#include "common.cpp"
#include "SpriteAnimation.h"

#pragma once
/*! \file Enemy.h
* A base class representing everything common for enemies in the game */
class Enemy: public Actor
{
public:
	Enemy();
	~Enemy();
	Enemy(EnemyType type, int x, int y);

	EnemyType GetType();
	void SetType(EnemyType type);
	int GetBestiaryId();
	void SetBestiaryId(int id);

	double GetMovementSpeed();
	void SetMovementSpeed(double speed);

	bool GetIfMoving();
	void SetIfMoving(bool moving);
	EnemyMovementState GetMovementState();
	void SetMovementState(EnemyMovementState state);
	std::pair<double, double> GetPointOfInterest();
	void SetPointOfInterest(double x, double y);
	double GetAlertRadius();
	void SetAlertRadius(double radius);
	void Alarm();
	bool StillAlarmed();
	bool CanBeAlarmed();
	void SetAlarmDuration(int duration);

	double GetMeleeDamage();
	void SetMeleeDamage(double damage);
	double GetProjectileDamage();
	void SetProjectileDamage(double damage);

	bool IsStunned();
	void Stun(int duration);

	int GetLevel();
	void SetLevel(int level);

	int GetGold();
	void SetGold(int gold);
	int GetXP();
	void SetXP(int xp);
	DropTableType GetDropTableType();
	void SetDropTableType(DropTableType type);

	// Step forward sprite animation etc
	void Update();

private:
	EnemyType type;
	int bestiary_id = 0;
	double movement_speed = 0.8;
	bool moving = false;
	EnemyMovementState movement_state = idle;
	std::pair<double, double> point_of_interest;
	double alert_radius = 200;

	double melee_damage = 1;
	double projectile_damage = 1;

	bool is_stunned = false;
	int stun_duration = 0;

	int alarm_duration_frames = 0;
	int alarm_current_frames = 0;

	int level = 1;

	int gold = 1;
	int xp = 1;
	DropTableType drop_table_type = only_gold;
};

