#include "Building.h"
#include "SpriteAnimation.h"


Building::Building()
{
}

Building::Building(SDL_Rect location, SpriteAnimation* sprite_anim)
{
	tile_location_rect = location;
	sprite_animation = sprite_anim;
}


Building::~Building()
{
}

SDL_Rect Building::GetTileLocationRect()
{
	return tile_location_rect;
}

void Building::SetTileLocationRect(SDL_Rect location)
{
	tile_location_rect = location;
}

SpriteAnimation* Building::GetSpriteAnimation()
{
	return sprite_animation;
}

int Building::GetCurrentAnimationFrameIndex()
{
	return current_animation_frame;
}

void Building::StepForwardAnimation()
{
	current_animation_frame++;
	if (current_animation_frame >= GetSpriteAnimation()->GetAnimationLength())
	{
		current_animation_frame = 0;
	}
}
