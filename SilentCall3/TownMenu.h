#pragma once
#include <string>
#include <vector>
#include <SDL.h>
#include "TownMenuOption.h"

class TownMenu
{
public:
	TownMenu();
	TownMenu(std::string aTitle, SDL_Texture* aTitleTexture);
	~TownMenu();

	void AddOption(TownMenuOption option);

	std::vector<TownMenuOption>* GetOptions();

	SDL_Texture* GetTitleTexture();

private:
	std::string title = "DEFAULT";
	std::vector<TownMenuOption> options;
	SDL_Texture* string_texture_menu_title;
};

