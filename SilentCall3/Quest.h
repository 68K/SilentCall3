#pragma once
#include <string>
#include "common.cpp"
#include "Enemy.h"

/*! \file Quest.h
* A class representing a quest */
class Quest
{
public:
	Quest();
	Quest(std::string title, std::string description, DungeonMapType map, int base_level, QuestCompletionCriteriaType completion_criteria_type,
		  int target_x, int target_y, QuestItem quest_item_id, DungeonMapType target_map, Enemy* target_enemy,
		  QuestLineType quest_line_type, int quest_line_id);
	~Quest();

    std::string GetTitle();
	std::string GetDescription();
	QuestState GetState();
	void SetState(QuestState state);

	int GetBaseLevel();

	DungeonMapType GetMapType();
	DungeonMapType GetDestination();

	QuestCompletionCriteriaType GetQuestCompletionCriteriaType();

	QuestLineType GetQuestLineType();
	int GetQuestLineId();

	int GetTargetX();
	int GetTargetY();

	QuestItem GetQuestItemId();

	/*! \brief Has the player got in range of the target location?
	*
	* @return true if x,y is less than threshold away from target_x,target_y
	* @return true if target_x < 0 (indicates this quest only cares about map; not location)
	* @return false is x,y is greater than threshold away from target_x,target_y
	*/
	bool IsNearCompletionLocation(int x, int y, int threshold);

private:
	std::string title = "DEFAULT TITLE";
	std::string description = "DEFAULT DESCRIPTION";
	DungeonMapType map;
	QuestState state = quest_ready;

	int base_level = 1;

	// TODO: Make subclasses for quest types
	QuestCompletionCriteriaType completion_criteria = defeat_an_enemy;
	DungeonMapType target_map;
	int target_x = -1;
	int target_y = -1;
	Enemy* target_enemy = nullptr;
	QuestItem quest_item_id = quest_item_none;

	// Questline data
	QuestLineType quest_line_type;
	int quest_line_id;
};

