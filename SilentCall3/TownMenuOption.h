#pragma once
#include <string>
#include <SDL.h>
#include "common.cpp"

class TownMenuOption
{
public:
	TownMenuOption();
	TownMenuOption(TownMenuOptionId id, std::string aTitle, SDL_Texture* aTitleTexture, bool isEnabled);
	~TownMenuOption();

	void Enable();
	void Disable();

	SDL_Texture* GetTitleTexture();

	TownMenuOptionId GetTownMenuOptionId();

private:
	TownMenuOptionId id;
	std::string title = "DEFAULT";
	SDL_Texture* title_texture;
	bool enable = false;
};

