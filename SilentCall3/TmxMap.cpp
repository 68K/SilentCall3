#include "TmxMap.h"
#include "common.cpp"
#include <string>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include "base64.h"

TmxMap::TmxMap()
{
}

TmxMap::TmxMap(std::string filename)
{
	using boost::property_tree::ptree;
	ptree pt;

	using namespace boost::algorithm;

	try
	{
		read_xml(filename, pt);
		width = pt.get<int>("map.<xmlattr>.width");
		height = pt.get<int>("map.<xmlattr>.height");
		solid_walls = new unsigned[width * height];
		visible_tiles = new unsigned[width * height];
		fog_tiles = new unsigned[width * height];
		std::fill_n(fog_tiles, width * height, 255);
		BOOST_LOG_TRIVIAL(debug) << "Width of map is " << width;
		int gid = 1; //TODO: To support multiple tilesets, the real gid(s) need to be determined for each tile layer
		tile_size = pt.get<int>("map.tileset.<xmlattr>.tilewidth");
		tileset_columns = pt.get<int>("map.tileset.<xmlattr>.columns");
		tileset_image_source = pt.get<std::string>("map.tileset.image.<xmlattr>.source");
		BOOST_LOG_TRIVIAL(trace) << "Map tileset image filename (direct from map file) is " << tileset_image_source;

		std::vector<std::string> path_split;
		split(path_split, tileset_image_source, is_any_of("/"));
		tileset_image_source = path_split.back();
		BOOST_LOG_TRIVIAL(debug) << "Map tileset image filename (with path stripped) is " << tileset_image_source;

		// Process layers
		for (const ptree::value_type &v : pt.get_child("map"))
		{
			if (v.first == "layer")
			{
				std::string layer_name = v.second.get_child("<xmlattr>.name").data();
				BOOST_LOG_TRIVIAL(debug) << "Reading map layer with name: " << layer_name;
				if (layer_name == "Solid walls")
				{
					std::string solid_wall_base64_string = v.second.get_child("data").data();
					ReadInBase64TileData(solid_wall_base64_string, solid_walls, width, height);
				}
				if (layer_name == "Visible tiles")
				{
					std::string visible_walls_base64_string = v.second.get_child("data").data();
					ReadInBase64TileData(visible_walls_base64_string, visible_tiles, width, height);
				}
				BOOST_LOG_TRIVIAL(debug) << "Finished reading map layer with name: " << layer_name;
			}

			// Objects
			if (v.first == "objectgroup")
			{
				std::string object_group_name = v.second.get_child("<xmlattr>.name").data();
				BOOST_LOG_TRIVIAL(debug) << "Reading object layer with name: " << object_group_name;
				if (object_group_name == "Objects")
				{
					BOOST_FOREACH(const ptree::value_type &w, v.second)
					{
						if (w.first == "object")
						{
							if(w.second.get_child("<xmlattr>.type").data() == "PlayerSpawn")
							{
								int player_spawn_x = (int)(w.second.get<double>("<xmlattr>.x") + (tile_size / 2));
								int player_spawn_y = (int)(w.second.get<double>("<xmlattr>.y") + (tile_size / 2));
								BOOST_LOG_TRIVIAL(debug) << "Map has player spawn point at x=" << player_spawn_x << ", y=" << player_spawn_y;
								player_spawn_points.push_back(new std::pair<int, int>(player_spawn_x, player_spawn_y));
							}
							if (w.second.get_child("<xmlattr>.type").data() == "PlayerExit")
							{
								int player_spawn_x = (int)(w.second.get<int>("<xmlattr>.x") + (tile_size / 2));
								int player_spawn_y = (int)(w.second.get<int>("<xmlattr>.y") + (tile_size / 2));
								BOOST_LOG_TRIVIAL(debug) << "Map has player exit point at x=" << player_spawn_x << ", y=" << player_spawn_y;
								player_exit_points.push_back(new std::pair<int, int>(player_spawn_x, player_spawn_y));
								boost::optional< const ptree& > optional_properties = w.second.get_child_optional("properties");
								if (optional_properties)
								{
									BOOST_LOG_TRIVIAL(debug) << "Exit number " << player_exit_points.size() << " has properties to read.";
									BOOST_FOREACH(const ptree::value_type &x, w.second.get_child("properties"))
									{
										if (x.second.get_child("<xmlattr>.name").data() == "LeadsToArea")
										{
											int leads_to = x.second.get<int>("<xmlattr>.value");
											BOOST_LOG_TRIVIAL(debug) << "Exit number " << player_exit_points.size() << " leads to map " << player_exit_points.size() - 1;
											player_exit_destinations[leads_to] = player_exit_points.size() - 1;
										}
									}
								}
							}
							if (w.second.get_child("<xmlattr>.type").data() == "EnemySpawn")
							{
								int enemy_spawn_x = (int)(w.second.get<double>("<xmlattr>.x") + (tile_size / 2));
								int enemy_spawn_y = (int)(w.second.get<double>("<xmlattr>.y") + (tile_size / 2));
								BOOST_LOG_TRIVIAL(trace) << "Map has player spawn point at x=" << enemy_spawn_x << ", y=" << enemy_spawn_y;
								enemy_spawn_points.push_back(new std::pair<int, int>(enemy_spawn_x, enemy_spawn_y));
							}
							if (w.second.get_child("<xmlattr>.type").data() == "clutter")
							{
								int clutter_x = (int)(w.second.get<double>("<xmlattr>.x") + (tile_size / 2));
								int clutter_y = (int)(w.second.get<double>("<xmlattr>.y") + (tile_size / 2));
								clutter_points.push_back(new std::pair<int, int>(clutter_x, clutter_y));
							}
						}
					}
				}
			}
		}
	}
	catch (std::exception const&  ex)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Something went wrong loading map: " << ex.what();
	}
}

TmxMap::~TmxMap()
{
	if (visible_tiles) free(visible_tiles);
	if (solid_walls) free(solid_walls);
}

std::string TmxMap::GetTilemapFilename()
{
	if (tileset_image_source.empty())
	{
		BOOST_LOG_TRIVIAL(warning) << "Map tileset filename was accessed when it had no value";
	}
	return tileset_image_source;
}

int TmxMap::GetWidth()
{
	return width;
}

int TmxMap::GetHeight()
{
	return height;
}

int TmxMap::GetTilesetColumnCount()
{
	return tileset_columns;
}

int TmxMap::GetTileSize()
{
	return tile_size;
}

unsigned * TmxMap::GetSolidWalls()
{
	return solid_walls;
}

unsigned * TmxMap::GetVisibleTiles()
{
	return visible_tiles;
}

unsigned* TmxMap::GetFogTiles()
{
	return fog_tiles;
}

void TmxMap::SetFogTileOpacity(int x, int y, unsigned opacity)
{
	fog_tiles[y * width + x] = opacity;
}

std::pair<int, int>* TmxMap::PickRandomStartPoint()
{
	if (player_spawn_points.size() == 0)
	{
		BOOST_LOG_TRIVIAL(error) << "TmxMap::PickRandomStartPoint was called when no spawn points are loaded. Returning 0,0";
		return new std::pair<int, int>(0, 0);
	}
	else
	{
		int randomly_selected_index = rand() % player_spawn_points.size();
		return new std::pair<int, int>(player_spawn_points.at(randomly_selected_index)->first, player_spawn_points.at(randomly_selected_index)->second);
	}
}

std::pair<int, int>* TmxMap::PickRandomExitPoint()
{
	if (player_exit_points.size() == 0)
	{
		BOOST_LOG_TRIVIAL(error) << "TmxMap::PickRandomExitPoint was called when no spawn points are loaded. Returning 0,0";
		return new std::pair<int, int>(0, 0);
	}
	else
	{
		int randomly_selected_index = rand() % player_exit_points.size();
		return new std::pair<int, int>(player_exit_points.at(randomly_selected_index)->first, player_exit_points.at(randomly_selected_index)->second);
	}
}

bool TmxMap::HasExitTo(DungeonMapType destination)
{
	return player_exit_destinations.count(destination) == 1;
}

std::pair<int, int>* TmxMap::GetExitPointForDestination(DungeonMapType destination)
{
	if (!(this->HasExitTo(destination)))
	{
		BOOST_LOG_TRIVIAL(error) << "TmxMap::GetExitPointForDestination was asked for exit to destination '" << destination
			                     << "' but this map does not lead there! Returning some random exit for a laugh...";
		return this->PickRandomExitPoint();
	}
	return new std::pair<int, int>(player_exit_points.at(player_exit_destinations[destination])->first, player_exit_points.at(player_exit_destinations[destination])->second);
}

std::vector<std::pair<int, int>*> TmxMap::GetEnemySpawnPoints()
{
	return enemy_spawn_points;
}

std::vector<std::pair<int, int>*> TmxMap::GetClutterPoints()
{
	return clutter_points;
}

TileType TmxMap::GetSolidWallTileTypeAt(int x, int y)
{
	return static_cast<TileType>(solid_walls[(y/tile_size * width) + (x / tile_size)]);
}

void TmxMap::ReadInBase64TileData(std::string raw_data, unsigned* destination, int width, int height)
{
	using boost::algorithm::trim;

	trim(raw_data);
	const std::string decoded_string = base64_decode(raw_data);
	unsigned *out = 0;
	BOOST_LOG_TRIVIAL(trace) << "Size of decoded map tile data is: " << decoded_string.size();
	std::string test = "1234";
	BOOST_LOG_TRIVIAL(trace) << "Size of test string is: " << test.size();
	out = (unsigned *)malloc(decoded_string.size());

	memcpy(out, decoded_string.c_str(), decoded_string.size());

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			unsigned tile = out[y * width + x];
			destination[y * width + x] = tile - 1;
			//BOOST_LOG_TRIVIAL(trace) << "Map tile " << x << "," << y << " is " << destination[y * width + x];
		}
	}
	free(out);
}