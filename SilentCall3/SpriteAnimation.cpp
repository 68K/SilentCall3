#include "SpriteAnimation.h"



SpriteAnimation::SpriteAnimation()
{
}

SpriteAnimation::SpriteAnimation(SDL_Texture* sheet, int w, int h, int duration, std::vector<AnimationFrame>& aFrames)
{
	spritesheet = sheet;
	sprite_width = w;
	sprite_height = h;
	frame_duration = duration;
	frames = aFrames;
}


SpriteAnimation::~SpriteAnimation()
{
}

SDL_Texture* SpriteAnimation::GetSpriteSheet()
{
	return spritesheet;
}

SDL_Rect SpriteAnimation::GetAnimationFrame(int step)
{
	SDL_Rect frame;
	frame.w = sprite_width;
	frame.h = sprite_height;

	if(step > GetAnimationLength())
	{ 
		frame.x = frames[0].x;
		frame.y = frames[0].y;
	}
	else
	{
		frame.x = frames[(int)(step / frame_duration)].x;
		frame.y = frames[(int)(step / frame_duration)].y;
	}
	return frame;
}

int SpriteAnimation::GetAnimationLength()
{
	return (int)frames.size() * frame_duration;
}
