#pragma once
#include <SDL.h>
#include <memory>
#include <string>
#include "common.cpp"
#include "TmxMap.h"
#include "EnemyGenerator.h"
#include "src/ClutterItem.h"
#include "src/DroppedItem.h"
#include "src/Furniture.h"
#include "src/Npc.h"
#include "player.h"
#include "Quest.h"

class DungeonMap
{
public:
	DungeonMap();
	~DungeonMap();

	void LoadTmxMap(DungeonMapType map_type, Quest& quest);

	void SetTmxMap(TmxMap* tmx_map);

	void LoadTextures(SDL_Renderer *ren);

	void Render(SDL_Renderer *ren, int centre_pixel_x, int centre_pixel_y);
	void RenderFogOfWar(SDL_Renderer *ren, SDL_Texture* tex, int centre_pixel_x, int centre_pixel_y);

	int GetStartX();

	int GetStartY();

	int GetExitX();

	int GetExitY();

	int GetTileSize();

	DungeonMapType GetDungeonMapType();

	/*! \brief Determine where an object will end up if it attempts to move to a new x,y; given
	*          the walls in this map
	*
	* @param current_x X ordinate that the object is currently at
	* @param current_y Y ordinate that the object is currently at 
	* @param desired_x X ordinate that the object would move to, if this map had no obstacles
	* @param desired_y Y ordinate that the object would move to, if this map had no obstacles
	* @param w           object width
	* @param h           object height
	* @return            pair of doubles (x,y) where the object should actually move to
	*/
	std::pair<double, double> ResultingCoordsFromAttemptedMove(double current_x, double current_y, double desired_x, double desired_y, int w, int h, bool floating);

	/*! \brief ResultingCoordsFromAttemptedMove that defaults to floating being false
	*/
	std::pair<double, double> ResultingCoordsFromAttemptedMove(double current_x, double current_y, double desired_x, double desired_y, int w, int h);

	/*! \brief Find an empty spot on the map near to a desired location
	*
	* TODO: Account for the width and height of the item that needs placed
	*
	* @param desired_x    X ordinate of the location you want an empty space near to
	* @param desired_y    Y ordinate of the location you want an empty space near to
	* @param max_distance The max distance to search before giving back
	* @return new pair of doubles (x,y) of an empty spot on the map
	*/
	std::pair<double, double> EmptySpaceNearTo(double desired_x, double desired_y, double max_distance);

	/*! \brief Set the exit near to x,y and have it lead back to the village.
	*          Used when a quest is complete.
	*/
	void OpenExitBackToVillageNear(double x, double y);

	std::vector<EnemyGenerator*> GetEnemyGenerators();
	void RemoveDestroyedEnemyGenerators();

	std::vector<Enemy*>& GetEnemies();
	void AddEnemy(Enemy* enemy);
	void ClearEnemies();
	void RemoveDeadEnemies();

	std::vector<Furniture*> GetFurniture();
	void AddFurniture(Furniture* furniture);
	void ClearFurniture();

	std::vector<std::shared_ptr<ClutterItem>> GetClutter();
	void RemoveDestroyedClutter();

	std::vector<std::shared_ptr<DroppedItem>> GetDroppedItems();
	void AddDroppedItem(std::shared_ptr<DroppedItem> item);
	void ClearDroppedItems();
	void RemoveCollectedDroppedItems();

	std::vector<Npc*> GetNpcs();
	void AddNpc(Npc* npc);
	void ClearNpcs();

	int GetWidthInPixels();
	int GetHeightInPixels();

	DungeonMapType GetExitDestinationMap();

	void UpdateFogOfWar(Player& player);

	bool CanSee(int fromX, int fromY, int targetX, int targetY);

private:

	std::map<DungeonMapType, std::string> DungeonMapFileNames;

	TmxMap* tmx_map = nullptr;

	SDL_Texture* texture_walls;

	DungeonMapType map_type;

	std::map<DungeonMapType, std::vector<EnemyGeneratorType>> enemy_generators_for_map_type;
	std::map<DungeonMapType, std::vector<ClutterGroup>> clutter_groups_for_map_type;

	int w = 0;
	int h = 0;
	int tileset_columns = 0;
	int tile_size = 0;

	int start_x = 0;
	int start_y = 0;
	int exit_x = 0;
	int exit_y = 0;
	DungeonMapType exit_destination = village_maptype;

	std::vector<EnemyGenerator*> enemy_generators;

	std::vector<Enemy*> enemies;
	int kMaxEnemiesPerDungeon = 500;

	std::vector<Furniture*> furniture;

	std::vector<std::shared_ptr<ClutterItem>> clutter_items;

	std::vector<std::shared_ptr<DroppedItem>> dropped_items;

	std::vector<Npc*> npcs;

	int fog_fade_radius = 500; // less for internal maps
	int fog_clear_radius = 250; // less for internal maps
};