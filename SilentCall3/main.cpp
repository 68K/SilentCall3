#include <iostream>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <time.h>
#include "common.cpp"
#include "src/Game.h"
#include "shlobj.h"

namespace logging = boost::log;
namespace src = boost::log::sources;

#define CLOCKS_PER_MS (CLOCKS_PER_SEC / 1000)
#define MILLISECONDS_PER_UPDATE (1000 / 60)

Game* game;

void InitialiseLogging()
{
	logging::add_common_attributes();

	//Register "Severity" as a formatter token
	boost::log::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");

	//Define log file
	logging::add_file_log(
		boost::log::keywords::file_name = kLogFilename,
		boost::log::keywords::rotation_size = 10 * 1024 * 1024,
		boost::log::keywords::format = "%Severity%: [%TimeStamp%]: %Message%",
		boost::log::keywords::auto_flush = true
		);

	logging::core::get()->set_filter
	(
		logging::trivial::severity >= logging::trivial::trace
	);

	BOOST_LOG_TRIVIAL(info) << "Initialised Logging";
}

int main(int, char**) {

	InitialiseLogging();

	//Initialise SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) != 0) {
		BOOST_LOG_TRIVIAL(fatal) << "SDL_Init Error: " << SDL_GetError();
		return 1;
	}

	//Initialise TTF library
	if (TTF_Init() == -1)
	{
		BOOST_LOG_TRIVIAL(fatal) << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError();
		SDL_Quit();
		return 1;
	}

	//Initialise PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		BOOST_LOG_TRIVIAL(fatal) << "SDL_image could not initialize! SDL_image Error: %s", IMG_GetError();
		SDL_Quit();
		return 1;
	}

	game = new Game();

	std::string config_file_path = ".";
	// TODO: Need to support wide characters here
	char szPath[MAX_PATH];
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
	{
		std::string path = "";
		path += szPath;
		config_file_path = path + "\\SilentCall3";
	}
	game->SetConfigPath(config_file_path);
	game->LoadConfig();
	
	std::string save_file_path = ".";
	// TODO: Need to support wide characters here
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 0, szPath)))
	{
		std::string path = "";
		path += szPath;
		save_file_path = path + "\\SilentCall3";
	}
	game->SetSavePath(save_file_path);

	game->SetDisplay();
	if (!game->LoadAssets())
	{
		BOOST_LOG_TRIVIAL(fatal) << "Failed to load game assets, quiting";
		SDL_Quit();
		return 1;
	}
	game->DefineSprites();
	game->InitialiseInput();
	game->Initialise();

	//Keep running until quitting the game is flagged
	clock_t last_update_time = NULL;
	BOOST_LOG_TRIVIAL(debug) << "Starting main game loop";
	while (!game->Quit()) {
		
		if (last_update_time)
		{
			clock_t time_since_last_update = clock() - last_update_time;
			if (time_since_last_update / CLOCKS_PER_MS >= MILLISECONDS_PER_UPDATE)
			{
				game->Update();
				last_update_time = clock();
			}
		}
		else
		{
			game->Update();
			last_update_time = clock();
		}
		game->Render();
	}
	BOOST_LOG_TRIVIAL(debug) << "Ended main game loop";

	// Shut down
	delete(game);
	BOOST_LOG_TRIVIAL(debug) << "Main program has deleted game.";
	TTF_Quit();
	BOOST_LOG_TRIVIAL(debug) << "Main program has quit TTF.";
	IMG_Quit();
	BOOST_LOG_TRIVIAL(debug) << "Main program has quit IMG.";
	SDL_Quit();
	BOOST_LOG_TRIVIAL(debug) << "Main program has quit SDL.";
	return 0;
}