#include "stdafx.h"
#include <fakeit.hpp>
#include "CppUnitTest.h"
#include "../SilentCall3/src/Bestiary.h"
#include "../SilentCall3/DungeonMap.h"
#include "../SilentCall3/Player.cpp"
#include "../SilentCall3/src/items/PotionItem.cpp"
#include <SDL.h>

using namespace fakeit;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SilentCall3UnitTests
{		
	TEST_CLASS(TestBestiary)
	{
	public:
		
		TEST_METHOD(TestMethodFillEntries)
		{
			Bestiary bestiary;
			bestiary.FillEntries();
			Assert::IsNotNull(bestiary.GenerateEnemy(yurt, 1, 1, 1));
		}

	};

	TEST_CLASS(TestDungeonMap)
	{
	public:
		TEST_METHOD(TestResultingCoordsFromAttemptedMove)
		{
			TmxMap* tmxmap = new TmxMap("../../test/collision_test.tmx");
			dungeon_map = new DungeonMap();
			dungeon_map->SetTmxMap(tmxmap);

			// EMPTY SPACE
			// From above
			// Attempting to reach centre of tile
			AssertWallCollision(640, 80, 640, 112, 640, 112);

			// TOP EDGE @ 64,96
			// From below...
			// ...attempting to cross above halfway
			AssertWallCollision(80, 128, 80, 96, 80, 112);
			// ...attempting to land below halfway
			AssertWallCollision(80, 128, 80, 120, 80, 120);
			// Colliding into top half from right
			AssertWallCollision(110, 100, 80, 100, 100, 100);
			// Colliding into top half from left
			AssertWallCollision(50, 100, 80, 100, 60, 100);

			// BOTTOM EDGE @ 448,96
			// From above...
			// ...landing inside top half
			AssertWallCollision(460, 80, 460, 106, 460, 106);
			// ...attempting to cross above halfway, bumping into wall
			AssertWallCollision(460, 80, 460, 110, 460, 108);
			// ...attempting to cross below halfway
			AssertWallCollision(460, 80, 460, 120, 460, 108);
			// Colliding into bottom half from right
			AssertWallCollision(490, 120, 464, 120, 484, 120);
			// Colliding into bottom half from left
			AssertWallCollision(432, 120, 464, 120, 444, 120);

			delete(dungeon_map);
		}
	private:
		DungeonMap* dungeon_map;
		int entity_size = 8;

		void AssertWallCollision(double current_x, double current_y, double desired_x, double desired_y, double expected_result_x, double expected_result_y)
		{
			std::pair<double, double> result = dungeon_map->ResultingCoordsFromAttemptedMove(current_x, current_y, desired_x, desired_y, entity_size, entity_size);
			std::wstring failure_message = L"Moving from " + std::to_wstring(current_x) + L"," + std::to_wstring(current_y)
				                           + L" to " + std::to_wstring(desired_x) + L"," + std::to_wstring(desired_y)
										   + L" with expected result " + std::to_wstring(expected_result_x) + L"," + std::to_wstring(expected_result_y);
			Assert::AreEqual(expected_result_x, result.first, failure_message.c_str());
			Assert::AreEqual(expected_result_y, result.second, failure_message.c_str());
		}
	};
}